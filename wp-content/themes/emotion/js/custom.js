/**
	* @package Emotion
	* @subpackage Emotion HTML
	* @since Emotion HTML 1.0
	* 
	* Template Scripts
	* Created by dan_fisher

	Custom JS
	
	1. Flexslider Fade
	2. Flexslider Slide
	3. Main Navigation
	4. Tabs (Tabbed Content)
	5. Accordion (Toggle)
	6. Isotope
	7. Responsive Video
	8. Prettyphoto
	-- Misc
	
**/

jQuery(document).ready(function() {

	
	/* ----------------------------------------------------------- */
	/*  1. Flexslider Fade
	/* ----------------------------------------------------------- */

	jQuery('.flexslider.fade').flexslider({
		animation: "fade",
		start: function(slider){
			jQuery('#slider').removeClass('loading');
		}
	});



	/* ----------------------------------------------------------- */
	/*  2. Flexslider Slide
	/* ----------------------------------------------------------- */
	jQuery('.flexslider.slide').flexslider({
		animation: "slide",
		smoothHeight: true,
		start: function(slider){
			jQuery('#slider').removeClass('loading');
		}
	});



	/* ----------------------------------------------------------- */
	/*  3. Main Navigation
	/* ----------------------------------------------------------- */

	jQuery('ul.sf-menu').superfish({
		autoArrows	: false,
		delay			: 800,
		disableHI	: true,
		animation	: {opacity:'show', height:'show'},
		speed			: 'fast'
	});

	/* Mobile Menu */
	jQuery('nav.primary .sf-menu').mobileMenu({
		defaultText: 'Navigate to...',
		className: 'select-menu'
	});



	/* ----------------------------------------------------------- */
	/*  4. Tabs (Tabbed Content)
	/* ----------------------------------------------------------- */

	jQuery(".tabs").each(function(){

		jQuery(this).find(".tab").hide();
		jQuery(this).find(".tab-menu li:first a").addClass("active").show();
		jQuery(this).find(".tab:first").show();

	});

	jQuery(".tabs").each(function(){

		jQuery(this).find(".tab-menu a").click(function() {

			jQuery(this).parent().parent().find("a").removeClass("active");
			jQuery(this).addClass("active");
			jQuery(this).parent().parent().parent().parent().find(".tab").hide();
			var activeTab = jQuery(this).attr("href");
			jQuery(activeTab).fadeIn();
			return false;

		});

	});

	

	/* ----------------------------------------------------------- */
	/*  5. Accordion (Toggle)
	/* ----------------------------------------------------------- */

	(function() {
		var $container = jQuery('.acc-body'),
			$acc_head   = jQuery('.acc-head');

		$container.hide();

		$acc_head.each(function() {
			if(jQuery(this).hasClass('active')) {
				jQuery(this).next().show();
			}
		});
		
		$acc_head.on('click', function(e) {
			if( jQuery(this).next().is(':hidden') ) {
				$acc_head.removeClass('active').next().slideUp(300);
				jQuery(this).toggleClass('active').next().slideDown(300);
			}
			e.preventDefault();
		});

	})();

	/* ----------------------------------------------------------- */
	/*  6. Isotope
	/* ----------------------------------------------------------- */

    (function() {

        // modified Isotope methods for gutters in masonry
        jQuery.Isotope.prototype._getMasonryGutterColumns = function() {
            var gutter = this.options.masonry && this.options.masonry.gutterWidth || 0;
                containerWidth = this.element.width();
          
            this.masonry.columnWidth = this.options.masonry && this.options.masonry.columnWidth ||
                        // or use the size of the first item
                        this.$filteredAtoms.outerWidth(true) ||
                        // if there's no items, use size of container
                        containerWidth;

            this.masonry.columnWidth += gutter;

            this.masonry.cols = Math.floor( ( containerWidth + gutter ) / this.masonry.columnWidth );
            this.masonry.cols = Math.max( this.masonry.cols, 1 );
        };

        jQuery.Isotope.prototype._masonryReset = function() {
            // layout-specific props
            this.masonry = {};
            // FIXME shouldn't have to call this again
            this._getMasonryGutterColumns();
            var i = this.masonry.cols;
            this.masonry.colYs = [];
            while (i--) {
                this.masonry.colYs.push( 0 );
            }
        };

        jQuery.Isotope.prototype._masonryResizeChanged = function() {
            var prevSegments = this.masonry.cols;
            // update cols/rows
            this._getMasonryGutterColumns();
            // return if updated cols/rows is not equal to previous
            return ( this.masonry.cols !== prevSegments );
        };


        // Set Gutter width
        var gutterSize;

        function getWindowWidth() {
            if( jQuery(window).width() < 480 ) {
                gutterSize = 10;
            } else if( jQuery(window).width() < 768 ) {
                gutterSize = 10;
            } else if( jQuery(window).width() < 980 ) {
                gutterSize = 20;
            } else {
                gutterSize = 20;
            }
        }


        // Portfolio settings
        var $container          = jQuery('.project-feed');
        var $filter             = jQuery('.project-feed-filter');

        jQuery(window).smartresize(function(){
            getWindowWidth();
            $container.isotope({
						filter              : '*',
						resizable           : true,
						// set columnWidth to a percentage of container width
						masonry: {
						gutterWidth     : gutterSize
               }
            });
        });

        $container.imagesLoaded( function(){
            jQuery(window).smartresize();
        });

        // Filter items when filter link is clicked
        $filter.find('a').click(function() {
            var selector = jQuery(this).attr('data-filter');
            $filter.find('a').removeClass('current');
            jQuery(this).addClass('current');
            $container.isotope({ 
                filter             : selector,
                animationOptions   : {
                animationDuration  : 750,
                easing             : 'linear',
                queue              : false
                }
            });
            return false;
        });
       
	})();


	/* ----------------------------------------------------------- */
	/*  7. Responsive Video
	/* ----------------------------------------------------------- */

	jQuery(".entry__video iframe, .page iframe").each(function(){
		jQuery(this).wrap("<figure class='video-holder'/>");
	});

	jQuery(".video-holder").fitVids();


	/* ----------------------------------------------------------- */
	/*  8. Prettyphoto
	/* ----------------------------------------------------------- */

	// store the viewport width in a variable
	var viewportWidth = jQuery('body').innerWidth();
	
	jQuery("a[rel^='prettyPhoto']").prettyPhoto({
		overlay_gallery: false,
		theme: 'pp_default',
		social_tools: false,
    changepicturecallback: function(){
        // 1024px is presumed here to be the widest mobile device. Adjust at will.
        if (viewportWidth < 1025) {
            jQuery(".pp_pic_holder.pp_default").css("top",window.pageYOffset+"px");
        }
    }
	});


	/* ----------------------------------------------------------- */
	/*  Misc
	/* ----------------------------------------------------------- */

	//Inset Border For Images
	jQuery(".featured-thumb > a").each(function(){
		jQuery(this).append("<i class='border'></i>");
	});

	//Even class for Old browsers
	jQuery(".ad-holder li:nth-child(even)").addClass("even");

	//Nomargin class for Old browsers
	jQuery(".post-loop li:nth-child(4n+1)").addClass("nomargin");

	//Contact Form 7 Tip Fix
	jQuery(".wpcf7-form-control-wrap").each(function(){
		jQuery(this).hover(function(){
			jQuery(this).find(".wpcf7-not-valid-tip").fadeOut();
		});
	});

});