jQuery(function() {
	jQuery('.folder .title').click(function(e) {
		var $f = jQuery(this).parent();
		$f.toggleClass('open');
		$f.find('> .contents').slideToggle('300');
		e.preventDefault();
	});
	jQuery('.post-loop li:not(:has(img))').prepend('<img src="' + templateDir + '/images/220x210.png">');
});