<?php get_header(); ?>
	
<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container clearfix">

		<!-- Content -->
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
	
			<?php get_template_part('loop'); ?>
			
			<!-- Pagination -->
			<?php emotion_pagination(); ?>
			<!-- /Pagination -->

		</div>
		<!--/Content -->

		<!-- Sidebar -->
		<aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php get_sidebar(); ?>
		</aside>
		<!-- /Sidebar -->
	
	</div>
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>