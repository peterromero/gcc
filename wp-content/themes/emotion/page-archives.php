<?php
/**
 * Template Name: Archives
 */
?>

<?php get_header(); ?>
<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
    <div class="container clearfix">	
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>                
            <div id="post-<?php the_ID(); ?>" <?php post_class('post-holder'); ?>>                    
                <div class="post-content">
                    <?php the_content(''); ?>
                    <?php wp_link_pages(array('before' => '<p><strong>'.__('Pages:', 'emotion').'</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>                            
                    
                    <h3><?php _e('Last 30 Posts', 'emotion') ?></h3>
                    <div class="list list-style__check">
                        <ul>
                        <?php $archive_30 = get_posts('numberposts=30&suppress_filters=0');
                        foreach($archive_30 as $k => $post) :
                        // Unset not translated posts
                        if ( function_exists( 'wpml_get_language_information' ) ) {
                        global $sitepress;

                            $check = wpml_get_language_information( $post->ID );
                        $language_code = substr( $check['locale'], 0, 2 );
                            if ( $language_code != $sitepress->get_current_language() )
                        unset( $archive_30[$k] );

                            // Post ID is different in a second language Solution
                            if ( function_exists( 'icl_object_id' ) ) 
                        $post = get_post( icl_object_id( $post->ID, 'post', true ) );
                        } ?>

                        <li><a href="<?php the_permalink(); ?>"><?php the_title();?></a></li>
                        <?php endforeach; ?>
                        </ul>
                    </div>

                    <div class="hr"></div>

                    <h3 class="archive_h"><?php _e('Archives by Month:', 'emotion') ?></h3>
                    <div class="list list-style__check">
                        <ul>
                            <?php wp_get_archives('type=monthly'); ?>
                        </ul>
                    </div>

                    <div class="hr"></div>

                    <h3 class="archive_h"><?php _e('Archives by Subject:', 'emotion') ?></h3>
                    <div class="list list-style__check">
                        <ul>
                            <?php wp_list_categories( 'title_li=' ); ?>
                        </ul>
                    </div><!-- .archive_lists -->
                </div>
            </div>
		<?php endwhile; endif; ?>			
		</div><!--#content-->	

		<!-- Sidebar -->
        <aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
            <?php if(function_exists('generated_dynamic_sidebar')) { 
                generated_dynamic_sidebar();
            } else {
                get_sidebar();
            }?>
        </aside>
        <!-- /Sidebar -->
	</div><!--.container-->
</div>
<!-- END CONTENT WRAPPER --> 
<?php get_footer(); ?>