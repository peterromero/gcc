<?php get_header(); ?>

<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container">
		
		
		<!-- Loop -->
		<?php if( have_posts() ) : while ( have_posts() ) : the_post(); ?>
		<div class="project-details-holder clearfix">
   		<!-- Project Thumbnail -->
   		<div class="grid_8">
   			<?php
				$embed = get_post_meta(get_the_ID(), 'emotion_video_embed', true);
				$type = get_post_meta(get_the_ID(), 'emotion_portfolio_type', true);

				//Check for video format
				$vimeo = strpos($embed, "vimeo");
				$youtube = strpos($embed, "youtube");

				if ($embed != '' && $type == "Video") {

					echo '<div class="video-holder">';
					//Display video
					if($vimeo !== false){

						//Get ID from video url
						$video_id = str_replace( 'http://vimeo.com/', '', $embed );
						$video_id = str_replace( 'http://www.vimeo.com/', '', $video_id );

						//Display Vimeo video
						echo '<div class="video-holder"><iframe src="http://player.vimeo.com/video/'.$video_id.'?title=0&amp;byline=0&amp;portrait=0" width="620" height="354" frameborder="0"></iframe></div>';

					} elseif($youtube !== false){

						//Get ID from video url
						$video_id = str_replace( 'https://youtube.com/watch?v=', '', $embed );
						$video_id = str_replace( 'https://www.youtube.com/watch?v=', '', $video_id );
						$video_id = str_replace( '&feature=channel', '', $video_id );

						echo '<div class="video-holder"><iframe title="YouTube video player" class="youtube-player" type="text/html" width="620" height="354" src="http://www.youtube.com/embed/'.$video_id.'" frameborder="0"></iframe></div>';

					} else {

					   echo _e("You entered a video URL that isn't compatible with theme functionality.", "emotion");

					}

					echo '</div>';

				} elseif($type == "Gallery") { ?>

				<?php
				$args = array(
					'post_type' => 'attachment',
					'numberposts' => '-1',
					'post_status' => null,
					'post_parent' => $post->ID,
					'orderby' => 'menu_order',
					'order' => 'ASC',
					'exclude' => get_post_thumbnail_id()
				);
				$attachments = get_posts($args); ?>
				<!-- begin post image -->
				<div class="flexslider slide project-thumbnail">
					<ul class="slides">
						<?php foreach($attachments as $attachment): ?>
						<?php $attachment_image = wp_get_attachment_image_src($attachment->ID, 'full'); ?>
						<?php $full_image = wp_get_attachment_image_src($attachment->ID, 'full'); ?>
						<?php $attachment_data = wp_get_attachment_metadata($attachment->ID); ?>
						<li>
							<a href="<?php echo $full_image[0]; ?>" rel="prettyPhoto['gallery']"><img src="<?php echo $attachment_image[0]; ?>" alt="<?php echo $attachment->post_title; ?>" /></a>
						</li>
						<?php endforeach; ?>
					</ul>
				</div>
				<!-- end post image -->
					
				<?php } else {
					// get the featured image for the post
					if( has_post_thumbnail() ) {
						$thumb = get_post_thumbnail_id();
						$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL

						echo '<figure class="project-img full-width">';
							echo '<img src="'. $img_url .'" alt="'. get_the_title() .'" />';
						echo '</figure>';
					}
				}

				?>
				
				<!--BEGIN .oldernewer-->
				<nav class="post-nav clearfix">
					
					<?php if( get_previous_post() ) : ?>
					<div class="older fleft"><?php previous_post_link('&larr; %link') ?></div>
					<?php endif; ?>
					
					<?php if( get_next_post() ) : ?>
					<div class="newer fright"><?php next_post_link('%link &rarr;') ?></div>
					<?php endif; ?>

				<!--END .oldernewer -->
				</nav>
   		</div>
   		
   		<!-- Project Description -->
   		<div class="grid_4">
   			<div class="project-details">
   				<?php the_content(); ?>
   			</div>
   		</div>
   	</div>
   	<?php endwhile; endif; ?>
   	<!-- Loop / End -->

		
		<!-- Related Projects -->
		<div class="clearfix">
   		<div class="grid_12">
   			<h2><?php echo of_get_option('portfolio_related_title'); ?></h2>
   		</div>
   	</div>

   	<div class="clearfix">
   		<div class="related-projects">
		   	<?php
				//Get array of terms
				$terms = get_the_terms( $post->ID , 'portfolio_category');

				if(!empty($terms)) {

				//Pluck out the IDs to get an array of IDS
				$term_ids = array_values(wp_list_pluck($terms,'term_id'));

				//Query posts with tax_query. Choose in 'IN' if want to query posts with any of the terms
				//Chose 'AND' if you want to query for posts with all terms
				$second_query = new WP_Query( array(
				   'post_type' => 'portfolio',
				   'tax_query' => array(
				                 array(
				                     'taxonomy' => 'portfolio_category',
				                     'field' => 'id',
				                     'terms' => $term_ids,
				                     'operator'=> 'IN' //Or 'AND' or 'NOT IN'
				                  )),
				   'posts_per_page' => 4,
				   'ignore_sticky_posts' => 1,
				   'orderby' => 'rand',
				   'post__not_in'=>array($post->ID),
				   'suppress_filters' => 0
				) );

				//Loop through posts and display...
				if($second_query->have_posts()) {
				while ($second_query->have_posts() ) : $second_query->the_post();

				//Video vars
				$embed = get_post_meta(get_the_ID(), 'emotion_video_embed', true);
				$type = get_post_meta(get_the_ID(), 'emotion_portfolio_type', true);

				$thumb = get_post_thumbnail_id();
				$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
				$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), 'folio-small');

				$excerpt = get_the_excerpt(); ?>
				<div class="grid_3 project-item">
					<?php if($embed != '' && $type == "Video") { ?>
						<?php if(has_post_thumbnail()): ?>
						<figure class="project-img">
							<img src="<?php echo $image[0] ?>" alt="" />
							<div class="overlay"></div>
							<div class="mask">
								<a class="icon-image" 
									href="<?php echo $embed; ?>"
									rel="prettyphoto[gallery]"
									title="<?php the_title_attribute(); ?>">
									<i class="icon-film"></i>
								</a>
							</div>
						</figure>
						<?php endif; ?>
					<?php } else { ?>
						<?php if(has_post_thumbnail()): ?>
						<figure class="project-img">
							<img src="<?php echo $image[0] ?>" alt="" />
							<div class="overlay"></div>
							<div class="mask">
								<a class="icon-image" 
									href="<?php echo $img_url ?>"
									rel="prettyphoto[gallery]"
									title="<?php the_title_attribute(); ?>">
									<i class="icon-search"></i>
								</a>
							</div>
						</figure>
						<?php endif; ?>
					<?php } ?>
					<div class="project-desc">
						<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
						<?php echo emotion_string_limit_words($excerpt,9); ?>
					</div>
				 </div>
				<?php endwhile; wp_reset_query();

				}
				} ?>

			</div>
   	</div>
   	<!-- Related Projects / End -->

	</div>
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>