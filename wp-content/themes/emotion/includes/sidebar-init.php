<?php
// If Dynamic Sidebar Exists
if (function_exists('register_sidebar'))
{   
    // Define Sidebar
    register_sidebar(array(
        'name'          => __('Sidebar', 'emotion'),
        'id'            => 'sidebar',
        'description'   => __( 'Sidebar located at the right side of blog page by default.', 'emotion'),
        'before_widget' => '<div id="%1$s" class="widget widget__sidebar %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // Define Dynamic Sidebar
    register_sidebar(array(
        'name'          => __('Home Sidebar', 'emotion'),
        'id'            => 'sidecode',
        'description'   => __( 'Widgets in this area will be shown using a shortcode [shortbar].', 'emotion'),
        'before_widget' => '<div id="%1$s" class="widget widget__sidebar">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>',
    ));
    // Define Footer Widget 1
    register_sidebar(array(
        'name' => __('Footer Widget 1', 'emotion'),
        'id' => 'footer1',
        'description'   => __( 'First Footer Widget Area', 'emotion'),
        'before_widget' => '<div id="%1$s" class="widget widget__footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
    // Define Footer Widget 2
    register_sidebar(array(
        'name' => __('Footer Widget 2', 'emotion'),
        'id' => 'footer2',
        'description'   => __( 'Second Footer Widget Area', 'emotion'),
        'before_widget' => '<div id="%1$s" class="widget widget__footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));
    // Define Footer Widget 3
    register_sidebar(array(
        'name' => __('Footer Widget 3', 'emotion'),
        'id' => 'footer3',
        'description'   => __( 'Third Footer Widget Area', 'emotion'),
        'before_widget' => '<div id="%1$s" class="widget widget__footer %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="widget-title">',
        'after_title' => '</h3>'
    ));

}
?>