<?php

/*
 * ========================================================================
 * Custom Post Types
 * ========================================================================
 */

// Slides
function create_post_type_slides() {
    register_post_type( 'slides',
        array( 
        'label' => __('Slides', 'emotion'), 
        'public' => true, 
        'show_ui' => true,
        'show_in_nav_menus' => false,
        'menu_icon' => 'dashicons-format-image',
        'rewrite' => array(
            'slug' => 'slides',
            'with_front' => FALSE,
        ),
        'supports' => array(
                'title',
                'thumbnail',
                'page-attributes')
            ) 
        );
}


// Portfolio
function create_post_type_portfolio() {
    register_post_type( 'portfolio',
    array( 
    'label' => __('Portfolio', 'emotion'), 
    'singular_label' => __('Porfolio Item', 'emotion'),
    '_builtin' => false,
    'public' => true, 
    'show_ui' => true,
    'show_in_nav_menus' => true,
    'hierarchical' => true,
    'capability_type' => 'page',
    'menu_icon' => 'dashicons-format-gallery',
    'rewrite' => array(
        'slug' => 'portfolio-view',
        'with_front' => FALSE,
    ),
    'supports' => array(
            'title',
            'editor',
            'thumbnail',
            'excerpt',
            'custom-fields',
            'comments')
        ) 
    );
    register_taxonomy('portfolio_category', 'portfolio', array('hierarchical' => true, 'label' => 'Portfolio Categories', 'singular_name' => 'Category', "rewrite" => true, "query_var" => true));
}

// Our Team
function create_post_type_team() {
    register_post_type( 'team',
        array( 
        'label' => __('Team', 'emotion'), 
        'singular_label' => __('Person', 'emotion'),
        '_builtin' => false,
        'exclude_from_search' => true, // Exclude from Search Results
        'capability_type' => 'page',
        'public' => true, 
        'show_ui' => true,
        'show_in_nav_menus' => true,
        'menu_icon' => 'dashicons-groups',
        'menu_position' => 5,
        'rewrite' => array(
            'slug' => 'team',
            'with_front' => FALSE,
        ),
        'supports' => array(
                'title',
                'editor',
                'excerpt',
                'thumbnail',
                'page-attributes')
            ) 
        );
}

?>