<?php

// Load Custom Theme Scripts using Enqueue
function emotion_scripts()
{
    if (!is_admin()) {
        // Modernizr with version Number at the end
        wp_register_script('modernizr', get_template_directory_uri() . '/js/modernizr.custom.17475.js', array('jquery'), '2.6.2', true);
        wp_enqueue_script('modernizr');

         // Easing Plugin
        wp_register_script('easing', get_template_directory_uri() . '/js/jquery.easing.min.js', array('jquery'), '1.3', true);
        wp_enqueue_script('easing');

        // Prettyphoto
        wp_register_script('prettyPhoto', get_template_directory_uri() . '/js/jquery.prettyPhoto.js', array('jquery'), '3.1.4', true);
        wp_enqueue_script('prettyPhoto');

        // Mobile Select Menu
        wp_register_script('mobilemenu', get_template_directory_uri() . '/js/jquery.mobilemenu.js', array('jquery'), '1.1', true);
        wp_enqueue_script('mobilemenu');

        // Superfish Menu
        wp_register_script('superfish', get_template_directory_uri() . '/js/jquery.superfish.js', array('jquery'), '1.7.2', true);
        wp_enqueue_script('superfish');

        // Flickr Feed
        wp_register_script('flickr', get_template_directory_uri() . '/js/jflickrfeed.js', array('jquery'), '1.0', true);
        wp_enqueue_script('flickr', true);

        // Elastislide
        wp_register_script('elastislide', get_template_directory_uri() . '/js/jquery.elastislide.js', array('jquery'), '1.1.0', true);
        wp_enqueue_script('elastislide');

        // Fitvids
        wp_register_script('fitvids', get_template_directory_uri() . '/js/jquery.fitvids.js', array('jquery'), '1.0', true);
        wp_enqueue_script('fitvids');

        // Flexslider
        wp_register_script('flexslider', get_template_directory_uri() . '/js/jquery.flexslider.js', array('jquery'), '2.1', true);
        wp_enqueue_script('flexslider');

        // Isotope Gallery
        wp_register_script('isotope', get_template_directory_uri() . '/js/jquery.isotope.min.js', array('jquery'), '1.5.21', true);
        wp_enqueue_script('isotope');
        wp_register_script('imagesloaded', get_template_directory_uri() . '/js/jquery.imagesloaded.min.js', array('jquery'), '1.0', true);
        wp_enqueue_script('imagesloaded');

        // Script for inits
        wp_register_script('emotionscripts', get_template_directory_uri() . '/js/custom.js', array('jquery'), '1.0', true);
        wp_enqueue_script('emotionscripts'); 
        wp_register_script('gccscripts', get_template_directory_uri() . '/js/gcc.js', array('jquery'), '1.0', true);
        wp_enqueue_script('gccscripts');
    }
}

// Loading Conditional Scripts
function conditional_scripts()
{

    // Conditional for Contact page
    if (is_page_template('page-contacts.php')) {
        // Google Map
        wp_register_script('gmap_api', 'http://maps.google.com/maps/api/js?sensor=true', array('jquery'), '1.0');
        wp_enqueue_script('gmap_api');
        wp_register_script('gmap', get_template_directory_uri() . '/js/jquery.ui.map.js', array('jquery'), '3.0');
        wp_enqueue_script('gmap');
    }

}

// Loading Admin Scripts
function emotion_admin_js($hook) {
    if ($hook == 'post.php' || $hook == 'post-new.php') {
        wp_register_script('emotion-admin', get_template_directory_uri() . '/js/jquery.custom.admin.js', 'jquery', '1.0');
        wp_enqueue_script('emotion-admin');
    }
}
add_action('admin_enqueue_scripts','emotion_admin_js',10,1);
?>