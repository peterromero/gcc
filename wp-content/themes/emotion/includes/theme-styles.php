<?php

// Theme Stylesheets using Enqueue
function emotion_styles()
{
	// Normalize default styles
	wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css', array(), '2.0.1', 'all');
	wp_enqueue_style('normalize');

	// Skeleton grid system
	wp_register_style('skeleton', get_template_directory_uri() . '/css/skeleton.css', array(), '1.2', 'all');
	wp_enqueue_style('skeleton');

	//Base Template Styles
	wp_register_style('base', get_template_directory_uri() . '/css/base.css', array(), '1.0', 'all');
	wp_enqueue_style('base');

	// Template Styles
	wp_register_style('emotion', get_stylesheet_directory_uri() . '/style.css', array(), '1.0', 'all');
	wp_enqueue_style('emotion');

	// Superfish Menu
	wp_register_style('superfish', get_template_directory_uri() . '/css/superfish.css', array(), '1.4.8', 'all');
	wp_enqueue_style('superfish');

	// PrettyPhoto
	wp_register_style('prettyPhoto', get_template_directory_uri() . '/css/prettyPhoto.css', array(), '1.0', 'all');
	wp_enqueue_style('prettyPhoto');

	// FontAwesome (icon fonts)
	wp_register_style('fontawesome', get_template_directory_uri() . '/css/font-awesome.min.css', array(), '3.1.1', 'all');
	wp_enqueue_style('fontawesome');

	// Flexslider
	wp_register_style('flexslider', get_template_directory_uri() . '/css/flexslider.css', array(), '2.1', 'all');
	wp_enqueue_style('flexslider');

	// Layout and Media queries 
	wp_register_style('layout', get_template_directory_uri() . '/css/media-queries.css', array(), '1.0', 'all');
	wp_enqueue_style('layout');

	// Tweaks for Greeley Children's Chorale
	wp_register_style('gcc', get_template_directory_uri() . '/css/gcc_tweaks.css', array(), '1.0', 'all');
	wp_enqueue_style('gcc');
}
?>