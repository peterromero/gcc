frameworkShortcodeAtts={
	attributes:[
			{
				label:"Button Text",
				id:"text",
				help:"Enter the text for button."
			},
			{
				label:"Button Link",
				id:"link",
				help:"Enter the link for button. (e.g. http://yoursite.com/)"
			},
			{
				label:"Size",
				id:"size",
				controlType:"select-control",
				selectValues:['normal', 'large'],
				defaultValue: 'normal', 
				defaultText: 'normal',
				help:"Choose button size."
			},
			{
				label:"Style",
				id:"style",
				controlType:"select-control",
				selectValues:['default', 'style2', 'style3'],
				defaultValue: 'normal', 
				defaultText: 'normal',
				help:"Choose button style."
			},
			{
				label:"Target",
				id:"target",
				controlType:"select-control",
				selectValues:['_blank', '_self', '_parent', '_top'],
				defaultValue: '_self', 
				defaultText: '_self',
				help:"The target attribute specifies a window or a frame where the linked document is loaded."
			}
	],
	defaultContent:"",
	shortcode:"button"
};
