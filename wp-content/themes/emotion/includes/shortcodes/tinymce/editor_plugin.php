<?php 
header("Content-Type:text/javascript");

//Setup URL to WordPress
$absolute_path = __FILE__;
$path_to_wp = explode( 'wp-content', $absolute_path );
$wp_url = $path_to_wp[0];

//Access WordPress
require_once( $wp_url.'/wp-load.php' );

//URL to TinyMCE plugin folder
$plugin_url = get_template_directory_uri().'/includes/shortcodes/tinymce/';
?>
(function(){
	
	var icon_url = '<?php echo $plugin_url; ?>' + 'images/icon_shortcodes.png';

	tinymce.create(
		"tinymce.plugins.MyThemeShortcodes",
		{
			init: function(d,e) {
					
					d.addCommand( "myThemeOpenDialog",function(a,c){
						
						// Grab the selected text from the content editor.
						selectedText = '';
					
						if ( d.selection.getContent().length > 0 ) {
					
							selectedText = d.selection.getContent();
							
						} // End IF Statement
						
						myThemeSelectedShortcodeType = c.identifier;
						myThemeSelectedShortcodeTitle = c.title;
						
						jQuery.get(e+"/dialog.php",function(b){
							
							jQuery('#shortcode-options').addClass( 'shortcode-' + myThemeSelectedShortcodeType );
							
							// Skip the popup on certain shortcodes.
							
							switch ( myThemeSelectedShortcodeType ) {
							
								
						// Recent Posts
								
								case 'recent_posts':
								
								var a = '[recent_posts num="5" type="post"]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

						// Posts
								
								case 'posts':
								
								var a = '[posts num="4" order="latest" type="post"]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

						// Team
								
								case 'team':
								
								var a = '[team num="4" item_class="grid_3"]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Alert
								
								case 'alert':
								
								var a = '[alert]'+selectedText+'[/alert]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Warning
								
								case 'warning':
								
								var a = '[warning]'+selectedText+'[/warning]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Info
								
								case 'info':
								
								var a = '[info]'+selectedText+'[/info]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Success
								
								case 'success':
								
								var a = '[success]'+selectedText+'[/success]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
								
						// Tags
								
								case 'tags':
								
								var a = '[tags color="color1" count="0"]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

						// Link
								
								case 'link':
								
								var a = '[link]'+selectedText+'[/link]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Dropcap
								
								case 'dropcap':
								
								var a = '[dropcap style="style1" size="normal" rounded="no"]'+selectedText+'[/dropcap]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Progress bar
								
								case 'bar':
								
								var a = '[bar progress="10" title="Title"]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
						
								
						// Horizontal Rule
								
								case 'hr':
								
								var a = '[hr]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// Horizontal Rule dashed
								
								case 'hr_dashed':
								
								var a = '[hr_dashed]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                	// spacer
								
								case 'spacer':
								
								var a = '[spacer]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

						// Spacer Small
								
								case 'spacer_small':
								
								var a = '[spacer_small]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// grid_1
								
								case 'grid_1':
								
								var a = '[grid_1]'+selectedText+'[/grid_1]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
						// grid_2
								
								case 'grid_2':
								
								var a = '[grid_2]'+selectedText+'[/grid_2]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_3
								
								case 'grid_3':
								
								var a = '[grid_3]'+selectedText+'[/grid_3]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_4
								
								case 'grid_4':
								
								var a = '[grid_4]'+selectedText+'[/grid_4]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_5
								
								case 'grid_5':
								
								var a = '[grid_5]'+selectedText+'[/grid_5]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_6
								
								case 'grid_6':
								
								var a = '[grid_6]'+selectedText+'[/grid_6]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_7
								
								case 'grid_7':
								
								var a = '[grid_7]'+selectedText+'[/grid_7]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_8
								
								case 'grid_8':
								
								var a = '[grid_8]'+selectedText+'[/grid_8]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_9
								
								case 'grid_9':
								
								var a = '[grid_9]'+selectedText+'[/grid_9]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_10
								
								case 'grid_10':
								
								var a = '[grid_10]'+selectedText+'[/grid_10]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_11
								
								case 'grid_11':
								
								var a = '[grid_11]'+selectedText+'[/grid_11]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // grid_12
								
								case 'grid_12':
								
								var a = '[grid_12]'+selectedText+'[/grid_12]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
               // blockquote
								
								case 'blockquote':
								
								var a = '[blockquote align="left" boxed="no"]'+selectedText+'[/blockquote]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                
                // clear
								
								case 'clear':
								
								var a = '[clear]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Tabs
								
								case 'tabs':
								
								var a = '[tabs tab1="Title #1" tab2="Title #2" tab3="Title #3"] [tab1] Tab 1 content... [/tab1] [tab2] Tab 2 content... [/tab2] [tab3] Tab 3 content... [/tab3] [/tabs]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Tabs Vertical
								
								case 'tabs_ver':
								
								var a = '[tabs_ver tab1="Title #1" tab2="Title #2" tab3="Title #3"] [tab1] Tab 1 content... [/tab1] [tab2] Tab 2 content... [/tab2] [tab3] Tab 3 content... [/tab3] [/tabs_ver]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Tabs Alt
								
								case 'tabs_alt':
								
								var a = '[tabs_alt tab1="Title #1" tab2="Title #2" tab3="Title #3"] [tab1] Tab 1 content... [/tab1] [tab2] Tab 2 content... [/tab2] [tab3] Tab 3 content... [/tab3] [/tabs_alt]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Carousel
								
								case 'carousel':
								
								var a = '[carousel] [item link="#"] Put image here [/item] [/carousel]';

								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
                			
               // Box
								
								case 'box':
								
								var a = '[box]'+selectedText+'[/box]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Title
								
								case 'title':
								
								var a = '[title]'+selectedText+'[/title]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
					
					// Title Bordered
								
								case 'title_bordered':
								
								var a = '[title_bordered]'+selectedText+'[/title_bordered]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
					
					// Pricing Tables
								
								case 'pricing_tables':
								
								var a = '[pricing_tables cols="4"] [pricing_col name="free" price="$ 0" field_1="per month" field_2="8 GB storage" field_3="Enhanced security" field_4="Real time operation" field_5="Unlimited users" link_txt="sign up" link_url="#" active="no"] [/pricing_tables]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Unstyled
								
								case 'unstyled':
								
								var a = '[unstyled]'+selectedText+'[/unstyled]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Check List
								
								case 'checklist':
								
								var a = '[checklist]'+selectedText+'[/checklist]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Arrow1 List
								
								case 'list_arrow1':
								
								var a = '[list_arrow1]'+selectedText+'[/list_arrow1]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Arrow2 List
								
								case 'list_arrow2':
								
								var a = '[list_arrow2]'+selectedText+'[/list_arrow2]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Arrow3 List
								
								case 'list_arrow3':
								
								var a = '[list_arrow3]'+selectedText+'[/list_arrow3]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Arrow4 List
								
								case 'list_arrow4':
								
								var a = '[list_arrow4]'+selectedText+'[/list_arrow4]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					// Star List
								
								case 'list_star':
								
								var a = '[list_star]'+selectedText+'[/list_star]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
								
					// Accordion
								
								case 'accordion':
								
								var a = '[accordion] [panel title="Title goes here"] Your Text [/panel] [panel title="Title goes here"] Your Text [/panel] [panel title="Title goes here"] Your Text [/panel] [/accordion]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;

					 // Shortbar
								
								case 'shortbar':
								
								var a = '[shortbar]';
								
								tinyMCE.activeEditor.execCommand("mceInsertContent", false, a);
								
								break;
															
								default:
								
								jQuery("#dialog").remove();
								jQuery("body").append(b);
								jQuery("#dialog").hide();
								var f=jQuery(window).width();
								b=jQuery(window).height();
								f=720<f?720:f;
								f-=80;
								b-=84;
							
							tb_show("Insert "+ myThemeSelectedShortcodeTitle +" Shortcode", "#TB_inline?width="+f+"&height="+b+"&inlineId=dialog");jQuery("#shortcode-options h3:first").text(""+c.title+" Shortcode Settings");
							
								break;
							
							} // End SWITCH Statement
						
						}
												 
					)
					 
					} 
				);

				},
					
				createControl:function(d,e){
				
						if(d=="shortcodes_button"){
						
							d=e.createMenuButton("shortcodes_button",{
								title:"Insert Shortcode",
								image:icon_url,
								icons:false
								});
								
								var a=this;d.onRenderMenu.add(function(c,b){
                	c=b.addMenu({title:"Dynamic"});
										a.addWithDialog(c,"Recent posts","recent_posts");
										a.addWithDialog(c,"Posts","posts");
										a.addWithDialog(c,"Team","team");
										a.addWithDialog(c,"Shortbar","shortbar");
										a.addWithDialog(c,"Tags","tags");
                  c=b.addMenu({title:"Columns"});
										a.addWithDialog(c,"grid_1 (60px)","grid_1");
										a.addWithDialog(c,"grid_2 (140px)","grid_2");
										a.addWithDialog(c,"grid_3 (220px)","grid_3");
										a.addWithDialog(c,"grid_4 (300px)","grid_4");
										a.addWithDialog(c,"grid_5 (380px)","grid_5");
										a.addWithDialog(c,"grid_6 (460px)","grid_6");
										a.addWithDialog(c,"grid_7 (540px)","grid_7");
										a.addWithDialog(c,"grid_8 (620px)","grid_8");
										a.addWithDialog(c,"grid_9 (700px)","grid_9");
										a.addWithDialog(c,"grid_10 (780px)","grid_10");
										a.addWithDialog(c,"grid_11 (860px)","grid_11");
										a.addWithDialog(c,"grid_12 (940px)","grid_12");
										a.addWithDialog(c,"Clear","clear");
                  c=b.addMenu({title:"Typography"});
                  				a.addWithDialog(c,"Link","link");
										a.addWithDialog(c,"Button","button");
										a.addWithDialog(c,"Dropcap","dropcap");
                    				a.addWithDialog(c,"Blockquote","blockquote");
                    				a.addWithDialog(c,"Title","title");
                    				a.addWithDialog(c,"Title - Bordered","title_bordered");
                    				a.addWithDialog(c,"Box","box");
										a.addWithDialog(c,"Divider","hr");
										a.addWithDialog(c,"Divider - dashed","hr_dashed");
                 					a.addWithDialog(c,"Spacer","spacer");
                 					a.addWithDialog(c,"Spacer - small","spacer_small");
						c=b.addMenu({title:"Alert Boxes"});
										a.addWithDialog(c,"Alert","alert");
										a.addWithDialog(c,"Warning","warning");
										a.addWithDialog(c,"Info","info");
										a.addWithDialog(c,"Success","success");
						c=b.addMenu({title:"List styles"});
										a.addWithDialog(c,"Unstyled","unstyled");
										a.addWithDialog(c,"Check list","checklist");
										a.addWithDialog(c,"Arrow1 list","list_arrow1");
										a.addWithDialog(c,"Arrow2 list","list_arrow2");
										a.addWithDialog(c,"Arrow3 list","list_arrow3");
										a.addWithDialog(c,"Arrow4 list","list_arrow4");
										a.addWithDialog(c,"Star list","list_star");
						c=b.addMenu({title:"Elements"});
										a.addWithDialog(c,"Icon Box","icobox");
										a.addWithDialog(c,"Tabs","tabs");
										a.addWithDialog(c,"Tabs Alt","tabs_alt");
										a.addWithDialog(c,"Tabs Vertical","tabs_ver");
										a.addWithDialog(c,"Carousel","carousel");
										a.addWithDialog(c,"Pricing Tables","pricing_tables");
										a.addWithDialog(c,"Accordion","accordion");
										a.addWithDialog(c,"Progress Bar","bar");

							});
							
							return d
						
						} // End IF Statement
						
						return null
					},
		
				addImmediate:function(d,e,a){d.add({title:e,onclick:function(){tinyMCE.activeEditor.execCommand("mceInsertContent",false,a)}})},
				
				addWithDialog:function(d,e,a){d.add({title:e,onclick:function(){tinyMCE.activeEditor.execCommand("myThemeOpenDialog",false,{title:e,identifier:a})}})},
		
				getInfo:function(){ return{longname:"Shortcode Generator",author:"VisualShortcodes.com",authorurl:"http://visualshortcodes.com",infourl:"http://visualshortcodes.com/shortcode-ninja",version:"1.0"} }
			}
		);
		
		tinymce.PluginManager.add("MyThemeShortcodes",tinymce.plugins.MyThemeShortcodes)
	}
)();
