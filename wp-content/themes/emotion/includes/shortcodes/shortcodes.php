<?php
// Grid Columns
function grid_column($atts, $content=null, $shortcodename ="")
{	
	extract(shortcode_atts(array(
		'class' => ''
	), $atts));
	
	//remove wrong nested <p>
	$content = remove_invalid_tags($content, array('p'));

	// add divs to the content
	$return = '<div class="'.$shortcodename.' '.$class.'">';
	$return .= do_shortcode($content);
	$return .= '</div>';

	return $return;
}
add_shortcode('grid_1', 'grid_column');
add_shortcode('grid_2', 'grid_column');
add_shortcode('grid_3', 'grid_column');
add_shortcode('grid_4', 'grid_column');
add_shortcode('grid_5', 'grid_column');
add_shortcode('grid_6', 'grid_column');
add_shortcode('grid_7', 'grid_column');
add_shortcode('grid_8', 'grid_column');
add_shortcode('grid_9', 'grid_column');
add_shortcode('grid_10', 'grid_column');
add_shortcode('grid_11', 'grid_column');
add_shortcode('grid_12', 'grid_column');



// Carousel Wrapper
function emotion_carousel_shortcode($atts, $content = null) {

	//Create unique ID for this carousel
	$unique_id = rand();

	$output = '<ul id="carousel-'. $unique_id .'" class="elastislide-list">';
				$output .= do_shortcode($content);
	$output .= '</ul><!-- /es-carousel-wrapper -->';

	$output .= '<script>
	jQuery(document).ready(function() {
		jQuery("#carousel-'. $unique_id .'").elastislide({
			minItems    : 2
		});
	});';
	$output .= '</script>';

	return $output;
}
add_shortcode('carousel', 'emotion_carousel_shortcode');


// Carousel Item
function emotion_carousel_item_shortcode($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'link' => ''
	), $atts));

	$output = '<li>';
		$output .= '<a href="'. $link .'">';
			$output .= do_shortcode($content);
		$output .= '</a>';
	$output .= '</li>';

	return $output;
}
add_shortcode('item', 'emotion_carousel_item_shortcode');
	

// Horizontal Rule
function hr_shortcode($atts, $content = null) {

    $output = '<div class="hr"></div>';

    return $output;
}
add_shortcode('hr', 'hr_shortcode');


// Horizontal Rule Dotted
function hr_dashed_shortcode($atts, $content = null) {

    $output = '<div class="hr hr-dashed"></div>';

    return $output;
}
add_shortcode('hr_dashed', 'hr_dashed_shortcode');	


// Box
function emotion_box_shortcode($atts, $content = null) {

	$output = '<div class="info-box">';
	$output .= do_shortcode($content);
	$output .= '</div>';

	return $output;
}

add_shortcode('box', 'emotion_box_shortcode');


// Spacer
function emotion_spacer_shortcode($atts, $content = null) {

    $output = '<div class="spacer"></div>';

    return $output;

}

add_shortcode('spacer', 'emotion_spacer_shortcode');



// Spacer Small
function emotion_spacer_small_shortcode($atts, $content = null) {

    $output = '<div class="spacer spacer__small"></div>';

    return $output;

}

add_shortcode('spacer_small', 'emotion_spacer_small_shortcode');
	
	
//Recent Posts
function shortcode_recent_posts($atts, $content = null) {
		
		extract(shortcode_atts(array(
				'type' => 'post',											 
				'num' => '5'
		), $atts));

		$output = '<div class="latest-posts-holder">';

		global $post;
		global $emotion_string_limit_words;
		
		$args = array(
			'post_type' => $type,
			'numberposts' => $num,
			'orderby' => 'post_date',
			'order' => 'DESC'
		);

		$latest = get_posts($args);
		
		foreach($latest as $post) {
				setup_postdata($post);
				$excerpt = get_the_excerpt();

				$categories = get_the_category();
				$separator = ' / ';
				$display = '';

				if($categories){
					foreach($categories as $category) {
						$display .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", "emotion" ), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
					}
				}

				$output .= '<article class="post clearfix">';


				if ( has_post_thumbnail($post->ID) ){
					$output .= '<figure class="featured-thumb"><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
					$output .= get_the_post_thumbnail($post->ID);
					$output .= '</a></figure>';
				}
				$output .= '<header class="post-header">';
					$output .= '<h3><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
							$output .= get_the_title($post->ID);
					$output .= '</a></h3>';
					$output .= '<p class="post-meta">';
						$output .= '<span class="post-meta-cats"><i class="icon-tag"></i>';
							$output .= trim($display, $separator);
						$output .= '</span>';
						$output .= '<span class="post-meta-author">';
							$output .= '<a href="'.get_author_posts_url(get_the_author_meta( 'ID' )).'"><i class="icon-user"></i>';
								$output .= get_the_author();
							$output .= '</a>';
						$output .= '</span>';
						$output .= '<span class="post-meta-comments">';
							$output .= '<a href="'.get_comments_link($post->ID).'"><i class="icon-comment"></i>';
								$output .= get_comments_number($post->ID);
							$output .= '</a>';
						$output .= '</span>';
					$output .= '</p>';
				$output .= '</header>';
				

				$output .= '<div class="post-excerpt">';
					$output .= '<p>';
						$output .= emotion_string_limit_words($excerpt,25);
					$output .= '</p>';
					$output .= '<a href="'.get_permalink($post->ID).'" class="btn" title="'.get_the_title($post->ID).'">';
						$output .= 'Read More';
					$output .= '</a>';
				$output .= '</div>';
				
			$output .= '</article>';
				
		}
				
		$output .= '</div>';
		return $output;
		
}

add_shortcode('recent_posts', 'shortcode_recent_posts');



//Posts
function shortcode_posts($atts, $content = null) {
		
		extract(shortcode_atts(array(
				'type' => 'post',
				'order' => 'post_date',
				'cat' => '',											 
				'num' => '4',
				'offset' => '0'
		), $atts));

		$output = '<ul class="post-loop unstyled clearfix">';

		global $post;
		global $emotion_string_limit_words;
		
		$args = array(
			'post_type' => $type,
			'numberposts' => $num,
			'orderby' => $order,
			'category_name' => $cat,
			'offset' => $offset,
			'order' => 'DESC'
		);

		$latest = get_posts($args);
		
		foreach($latest as $post) {
				setup_postdata($post);

				$categories = get_the_category();
				$separator = ' / ';
				$display = '';

				if($categories){
					foreach($categories as $category) {
						$display .= '<a href="'.get_category_link( $category->term_id ).'" title="' . esc_attr( sprintf( __( "View all posts in %s", "emotion"), $category->name ) ) . '">'.$category->cat_name.'</a>'.$separator;
					}
				}

				$output .= '<li>';

				if ( has_post_thumbnail($post->ID) ){
					$output .= get_the_post_thumbnail($post->ID, 'medium');
				}
				$output .= '<div class="post-caption">';
					$output .= '<span class="post-cats">';
						$output .= trim($display, $separator);
					$output .= '</span>';
					$output .= '<h4><a href="'.get_permalink($post->ID).'" title="'.get_the_title($post->ID).'">';
							$output .= get_the_title($post->ID);
					$output .= '</a></h4>';
				$output .= '</div>';
				
			$output .= '</li>';
				
		}
				
		$output .= '</ul>';
		return $output;
		
}

add_shortcode('posts', 'shortcode_posts');


//Team
function team_shortcode($atts, $content = null) {

		extract(shortcode_atts(array(
				'num' => '4',
				'item_class' => 'grid_3',
				'offset' => ''
		), $atts));

		$team = get_posts('post_type=team&orderby=menu_order&order=ASC&offset='.$offset.'&numberposts='.$num);

		$output = '<div class="team-holder clearfix">';
		
		global $post;

		foreach($team as $post){
				setup_postdata($post);
				$position =  get_post_meta(get_the_ID(), 'emotion_team_role', true);
				$twitter =  get_post_meta(get_the_ID(), 'emotion_team_twitter', true);
				$facebook =  get_post_meta(get_the_ID(), 'emotion_team_facebook', true);
				$googleplus =  get_post_meta(get_the_ID(), 'emotion_team_gplus', true);
				$dribbble =  get_post_meta(get_the_ID(), 'emotion_team_dribbble', true);
				$linkedin =  get_post_meta(get_the_ID(), 'emotion_team_linkedin', true);
				$excerpt = get_the_excerpt();

				$output .= '<div class="item-team '.$item_class.'">';
					if ( has_post_thumbnail($post->ID) ){
						$output .= '<figure class="team-img"><a href="'.get_permalink($post->ID).'">';
							$output .= get_the_post_thumbnail($post->ID, 'medium');
						$output .= '</a></figure>';
					}
					$output .= '<hgroup>';
						$output .= '<h4><a href="'.get_permalink($post->ID).'">';
							$output .= get_the_title($post->ID);
						$output .= '</a></h4>';
						$output .= '<h5>';
							$output .= $position;
						$output .= '</h5>';
					$output .= '</hgroup>';
					
					$output .= '<div class="team-excerpt">';
						$output .= $excerpt;
					$output .= '</div>';

					$output .= '<footer class="team-footer">';
						$output .= '<ul class="social-links unstyled">';
						if($twitter!="") {
							$output .= '<li class="ico-twitter"><a href="'.$twitter.'">Twitter</a></li>';
						}
						if($facebook!="") {
							$output .= '<li class="ico-facebook"><a href="'.$facebook.'">Facebook</a></li>';
						}
						if($googleplus!="") {
							$output .= '<li class="ico-googleplus"><a href="'.$googleplus.'">Google+</a></li>';
						}
						if($dribbble!="") {
							$output .= '<li class="ico-dribbble"><a href="'.$dribbble.'">Dribbble</a></li>';
						}
						if($linkedin!="") {
							$output .= '<li class="ico-linkedin"><a href="'.$linkedin.'">Linkedin</a></li>';
						}
						$output .= '</ul>';
					$output .= '</footer>';
						
				$output .= '</div>';

		}

		$output .= '</div>';

		return $output;

}

add_shortcode('team', 'team_shortcode');

	
	
//Tag Cloud
function shortcode_tags($atts, $content = null) {

	extract(shortcode_atts(array(
		'color' => 'color1',
		'count' => '0'
   ), $atts));

	$output = '<div class="tagcloud clearfix tag-'.$color.'">';

	$tags = wp_tag_cloud('smallest=8&largest=8&format=array&number='.$count);
	foreach($tags as $tag){
			$output .= $tag.' ';
	}
	$output .= '</div>';
	return $output;

}

add_shortcode('tags', 'shortcode_tags');



/**
 *
 * HTML Shortcodes
 *
 */

// Blockquote
function blockquote_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'align' => 'left',
			'boxed' => 'no'
	 ), $atts));

	if($boxed == 'yes') {
		$boxed = 'styled';
	}

	$output = '<blockquote class="'.$align.' '.$boxed.'">';
	$output .= do_shortcode($content);
	$output .= '</blockquote><!-- blockquote (end) -->';

	return $output;
}
add_shortcode('blockquote', 'blockquote_shortcode');



// Testimonials
function testimonials_shortcode($atts, $content = null) {

	$output = '<ul class="testimonials-list">';
	$output .= do_shortcode($content);
	$output .= '</ul>';

	return $output;
}
add_shortcode('testimonials', 'testimonials_shortcode');


// Testimonial
function testimonial_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'name' => '',
			'link' => ''
	 ), $atts));

	$output = '<li>';
		$output .= '<blockquote>';
		$output .= do_shortcode($content);
		$output .= '</blockquote>';

		$output .= '<span class="testi-info">';
			$output .= '<a href="'.$link.'">';
				$output .= $name;
			$output .= '</a>';
		$output .= '</span>';

	$output .= '</li>';

	return $output;
}
add_shortcode('testi', 'testimonial_shortcode');


// Clear
function shortcode_clear() {
	return '<div class="clear"></div>';
}

add_shortcode('clear', 'shortcode_clear');


// Tabs
function tabs_shortcode($atts, $content = null) {

	$output = '<div class="tabs">';
	$output .= '<div class="tab-menu">';
	$output .= '<ul>';

		//Create unique ID for this tab set
		$id = rand();

	//Build tab menu
	$numTabs = count($atts);

	for($i = 1; $i <= $numTabs; $i++){
	  $output .= '<li><a href="#tab-'.$id.'-'.$i.'">'.$atts['tab'.$i].'<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>';
	}

	$output .= '</ul>';
	$output .= '<div class="clear"></div>';
	$output .= '</div><!-- .tab-menu (end) -->';
	$output .= '<div class="tab-wrapper">';

	//Build content of tabs
	$i = 1;
	$tabContent = do_shortcode($content);
	$find = array();
	$replace = array();
	foreach($atts as $key => $value){
	  $find[] = '['.$key.']';
	  $find[] = '[/'.$key.']';
	  $replace[] = '<div id="tab-'.$id.'-'.$i.'" class="tab">';
	  $replace[] = '</div><!-- .tab (end) -->';
			$i++;
	}

	$tabContent = str_replace($find, $replace, $tabContent);

	$output .= $tabContent;

	$output .= '</div><!-- .tab-wrapper (end) -->';
	$output .= '</div><!-- .tabs (end) -->';

	return $output;

}

add_shortcode('tabs', 'tabs_shortcode');


// Tabs Vertical
function tabs_vertical_shortcode($atts, $content = null) {

	$output = '<div class="tabs tabs__vertical">';
	$output .= '<div class="tab-menu grid_2 alpha clearfix">';
	$output .= '<ul>';

		//Create unique ID for this tab set
		$id = rand();

	//Build tab menu
	$numTabs = count($atts);

	for($i = 1; $i <= $numTabs; $i++){
	  $output .= '<li><a href="#tab-'.$id.'-'.$i.'">'.$atts['tab'.$i].'<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>';
	}

	$output .= '</ul>';
	$output .= '<div class="clear"></div>';
	$output .= '</div><!-- .tab-menu (end) -->';
	$output .= '<div class="tab-wrapper grid_4 alpha omega">';

	//Build content of tabs
	$i = 1;
	$tabContent = do_shortcode($content);
	$find = array();
	$replace = array();
	foreach($atts as $key => $value){
	  $find[] = '['.$key.']';
	  $find[] = '[/'.$key.']';
	  $replace[] = '<div id="tab-'.$id.'-'.$i.'" class="tab">';
	  $replace[] = '</div><!-- .tab (end) -->';
			$i++;
	}

	$tabContent = str_replace($find, $replace, $tabContent);

	$output .= $tabContent;

	$output .= '</div><!-- .tab-wrapper (end) -->';
	$output .= '</div><!-- .tabs (end) -->';

	return $output;

}

add_shortcode('tabs_ver', 'tabs_vertical_shortcode');


// Tabs Alt
function tabs_alt_shortcode($atts, $content = null) {
	
	$output = '<div class="tabs tabs__alt">';
	$output .= '<div class="tab-menu">';
	$output .= '<ul>';

		//Create unique ID for this tab set
		$id = rand();

	//Build tab menu
	$numTabs = count($atts);

	for($i = 1; $i <= $numTabs; $i++){
	  $output .= '<li><a href="#tab-'.$id.'-'.$i.'">'.$atts['tab'.$i].'<i class="l-tab-shad"></i><i class="r-tab-shad"></i></a></li>';
	}

	$output .= '</ul>';
	$output .= '<div class="clear"></div>';
	$output .= '</div><!-- .tab-menu (end) -->';
	$output .= '<div class="tab-wrapper">';

	//Build content of tabs
	$i = 1;
	$tabContent = do_shortcode($content);
	$find = array();
	$replace = array();
	foreach($atts as $key => $value){
	  $find[] = '['.$key.']';
	  $find[] = '[/'.$key.']';
	  $replace[] = '<div id="tab-'.$id.'-'.$i.'" class="tab">';
	  $replace[] = '</div><!-- .tab (end) -->';
			$i++;
	}

	$tabContent = str_replace($find, $replace, $tabContent);

	$output .= $tabContent;

	$output .= '</div><!-- .tab-wrapper (end) -->';
	$output .= '</div><!-- .tabs (end) -->';

	return $output;

}

add_shortcode('tabs_alt', 'tabs_alt_shortcode');


// Title
function title_shortcode($atts, $content = null) {

	$output = '<h4 class="alt-title">';
	$output .= do_shortcode($content);
	$output .= '</h4>';

	return $output;
}
add_shortcode('title', 'title_shortcode');

// Title Bordered
function title_bordered_shortcode($atts, $content = null) {

	$output = '<h2 class="bordered">';
	$output .= do_shortcode($content);
	$output .= '</h2>';

	return $output;
}
add_shortcode('title_bordered', 'title_bordered_shortcode');



// Pricing Tables
function pricing_tables_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'cols' => '4'
	 ), $atts));

	if($cols=='4') {
		$cols = 'four-cols';
	} else{
		$cols = 'three-cols';
	}

	$output = '<div class="pricing-tables clearfix '.$cols.'">';
	$output .= do_shortcode($content);
	$output .= '</div>';

	return $output;
}
add_shortcode('pricing_tables', 'pricing_tables_shortcode');

// Pricing Column
function pricing_col_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'name' => '',
			'price' => '$ 0',
			'field_1' => '',
			'field_2' => '',
			'field_3' => '',
			'field_4' => '',
			'field_5' => '',
			'field_6' => '',
			'field_7' => '',
			'field_8' => '',
			'field_9' => '',
			'field_10' => '',
			'field_11' => '',
			'field_12' => '',
			'link_txt' => 'Sign Up',
			'link_url' => '#',
			'active' => ''
	), $atts));

	if($active == "yes") {
		$active = "active";
	}

	$output = '<div class="pricing-column">';
		$output .= '<div class="single-pricing-table '.$active.'">';
			$output .= '<header class="pr-head">';
				$output .= '<h4>'.$name.'</h4>';
				$output .= '<h3 class="price">'.$price.'</h3>';
			$output .= '</header>';

			$output .= '<div class="pr-features">';
				$output .= '<ul>';
					if($field_1) {
						$output .= '<li>'.$field_1.'</li>';
					}
					if($field_2) {
						$output .= '<li>'.$field_2.'</li>';
					}
					if($field_3) {
						$output .= '<li>'.$field_3.'</li>';
					}
					if($field_4) {
						$output .= '<li>'.$field_4.'</li>';
					}
					if($field_5) {
						$output .= '<li>'.$field_5.'</li>';
					}
					if($field_6) {
						$output .= '<li>'.$field_6.'</li>';
					}
					if($field_7) {
						$output .= '<li>'.$field_7.'</li>';
					}
					if($field_8) {
						$output .= '<li>'.$field_8.'</li>';
					}
					if($field_9) {
						$output .= '<li>'.$field_9.'</li>';
					}
					if($field_10) {
						$output .= '<li>'.$field_10.'</li>';
					}
					if($field_11) {
						$output .= '<li>'.$field_11.'</li>';
					}
					if($field_12) {
						$output .= '<li>'.$field_12.'</li>';
					}
				$output .= '</ul>';
			$output .= '</div>';

			$output .= '<footer class="pr-foot">';
				$output .= '<a href="'.$link_url.'">';
					$output .= $link_txt;
				$output .= '</a>';
			$output .= '</footer>';

		$output .= '</div>';
	$output .= '</div>';

	return $output;

}
add_shortcode('pricing_col', 'pricing_col_shortcode');


// Button
function button_shortcode($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'style' => 'default',
		   'link' => '#',
		   'text' => 'Button',
			'size' => 'normal',
			'target' => '_self'
   ), $atts));
    
	$output =  '<a href="'.$link.'" title="'.$text.'" class="btn btn-'.$size.' btn-'.$style.'" target="'.$target.'">';
		$output .= $text;
	$output .= '</a>';

   return $output;

}
add_shortcode('button', 'button_shortcode');


// Icon Box
function icobox_shortcode($atts, $content = null) {
	extract(shortcode_atts(
		array(
			'icon' => 'icon-laptop',
			'title' => 'Title',
		   'text' => ''
   ), $atts));
    
	$output =  '<div class="ico-box">';
		$output .= '<div class="ico-holder">';
			$output .= '<i class="'.$icon.'"></i>';
		$output .= '</div>';
		$output .= '<div class="ico-box-content">';
			$output .= '<h4>';
				$output .= $title;
			$output .= '</h4>';
			$output .= $text;
		$output .= '</div>';
	$output .= '</div>';

   return $output;

}
add_shortcode('icobox', 'icobox_shortcode');


//Link
function link_shortcode($args, $content) {
	return '<span class="link"><span>'.do_shortcode($content).'<span> &nbsp; &rarr;</span>';
}
add_shortcode('link', 'link_shortcode');


/**
 * Alert boxes
 */

//alert
function shortcode_alert($args, $content) {
	return '<div class="alert alert-error">'.do_shortcode($content).'</div>';
}
add_shortcode('alert', 'shortcode_alert');

//warning
function shortcode_warning($args, $content) {
	return '<div class="alert alert-warning">'.do_shortcode($content).'</div>';
}
add_shortcode('warning', 'shortcode_warning');

//info
function shortcode_info($args, $content) {
	return '<div class="alert alert-info">'.do_shortcode($content).'</div>';
}
add_shortcode('info', 'shortcode_info');

//success
function shortcode_success($args, $content) {
	return '<div class="alert alert-success">'.do_shortcode($content).'</div>';
}
add_shortcode('success', 'shortcode_success');


/**
 * List styles
 */

// Unstyled List
function unstyled_shortcode($atts, $content = null) {
    $output = '<div class="unstyled">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('unstyled', 'unstyled_shortcode');

// Check List
function checklist_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__check">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('checklist', 'checklist_shortcode');

// Arrow1 List
function arrow1_list_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__arrow1">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('list_arrow1', 'arrow1_list_shortcode');

// Arrow2 List
function arrow2_list_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__arrow2">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('list_arrow2', 'arrow2_list_shortcode');

// Arrow1 List
function arrow3_list_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__arrow3">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('list_arrow3', 'arrow3_list_shortcode');

// Arrow4 List
function arrow4_list_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__arrow4">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('list_arrow4', 'arrow4_list_shortcode');

// Star List
function star_list_shortcode($atts, $content = null) {
    $output = '<div class="list list-style__star">';
    $output .= do_shortcode($content);
    $output .= '</div>';
    return $output;
}
add_shortcode('list_star', 'star_list_shortcode');


// Accordion
function accordion_shortcode($atts, $content = null) {

	$output = '<dl class="accordion-wrapper">';
	$output .= do_shortcode($content);
	$output .= '</dl>';

	return $output;
}
add_shortcode('accordion', 'accordion_shortcode');

// Accordion Item
function panel_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'title' => 'Title goes here'
	 ), $atts));

	$output = '<dt class="acc-head">';
		$output .= '<a href="#">';
			$output .= $title;
		$output .= '</a>';
	$output .= '</dt>';

	$output .= '<dd class="acc-body">';
		$output .= '<div class="content">';
			$output .= do_shortcode($content);
		$output .= '</div>';
	$output .= '</dd><!-- //.acc-body -->';

	return $output;

}
add_shortcode('panel', 'panel_shortcode');



// Dropcaps
function dropcap_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'style' => 'style1',
			'size' => 'normal',
			'rounded' => 'no'
	), $atts));

	if($rounded == "yes") {
		$rounded = "rounded";
	}

	$output = '<span class="dropcap dropcap-'.$style.' dropcap-'.$size.' dropcap-'.$rounded.'">';
	$output .= do_shortcode($content);
	$output .= '</span>';

	return $output;
}
add_shortcode('dropcap', 'dropcap_shortcode');


// Progress Bar
function bar_shortcode($atts, $content = null) {

	extract(shortcode_atts(
		array(
			'progress' => '10',
			'title' => 'Title'
	), $atts));

	$output = '<div class="progress-bar">';
		$output .= '<h4>';
			$output .= $title . ' (' . $progress . '%)';
		$output .= '</h4>';
		$output .= '<div class="progress-bar-holder">';
			$output .= '<div class="progress-bar-value" style="width:'.$progress.'%">';
				$output .= do_shortcode($content);
			$output .= '</div>';
		$output .= '</div>';
	$output .= '</div>';

	return $output;
}
add_shortcode('bar', 'bar_shortcode');


// Home Sidebar
function shortbar_sc( $atts ) {
	ob_start();
	dynamic_sidebar('sidecode');
	$html = ob_get_contents();
	ob_end_clean();
	return $html;
}
add_shortcode( 'shortbar', 'shortbar_sc' );

?>