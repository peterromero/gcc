<?php // Custom Styling
function custom_styles() {
 echo '<style>'; ?>

 <?php /* Body background */
 $background = of_get_option('bg_body');
 echo 'body { background-image:url('.$background['image']. '); background-repeat:'.$background['repeat'].'; background-position:'.$background['position'].';  background-attachment:'.$background['attachment'].'; background-color:'.$background['color'].'}';

 /* Body background */
 $background = of_get_option('bg_body');
 echo 'body { background-image:url('.$background['image']. '); background-repeat:'.$background['repeat'].'; background-position:'.$background['position'].';  background-attachment:'.$background['attachment'].'; background-color:'.$background['color'].'}';

 /* Top Header background */
 $bg_header_top = of_get_option('bg_header_top');
 echo '.header-top { background-image:url('.$bg_header_top['image']. '); background-repeat:'.$bg_header_top['repeat'].'; background-position:'.$bg_header_top['position'].';  background-attachment:'.$bg_header_top['attachment'].'; background-color:'.$bg_header_top['color'].'}';

 /* Footer widgets background */
 $bg_footer_widgets = of_get_option('bg_footer_widgets');
 echo '.footer-widgets { background-image:url('.$bg_footer_widgets['image']. '); background-repeat:'.$bg_footer_widgets['repeat'].'; background-position:'.$bg_footer_widgets['position'].';  background-attachment:'.$bg_footer_widgets['attachment'].'; background-color:'.$bg_footer_widgets['color'].'}'; 
 ?>

<?php if(of_get_option('bg_footer', '#040404')): ?>
#footer {
	background-color:<?php echo of_get_option('bg_footer', '#040404'); ?>;
}
<?php endif; ?>


<?php if(of_get_option('color_accent', '#eb593c')): ?>
/* Accent Color */
a,
.accordion-wrapper .acc-head a:hover,
.accordion-wrapper .acc-head.active a,
.list ul li:before,
.link a:hover,
.copyright a:hover,
.posts-list h4 a:hover,
.project-desc h3 a:hover,
.commentlist li .comment-reply a:hover,
.entry-title a:hover,
.post-nav a:hover,
.post-loop .post-caption h4 a:hover,
.post-loop .post-cats a:hover,
.post-header h3 a:hover,
.flex-caption h2 strong {
	color:<?php echo of_get_option('color_accent', '#eb593c'); ?>;
}
.tabs__vertical .tab-menu ul {
	border-right-color:<?php echo of_get_option('color_accent', '#eb593c'); ?>;
}
.tabs .tab-menu a.active,
.single-pricing-table.active .pr-foot a,
.single-pricing-table.active .pr-head h4,
.single-pricing-table.active:before,
.single-pricing-table .pr-foot a:hover,
.dropcap-style2,
.progress-bar-value,
.btn,
button,
input[type="submit"],
input[type="reset"],
input[type="button"],
.flickr-widget_thumb_holder:hover .hover,
.project-feed-filter a:hover,
.tabs__alt .tab-menu a.active:after,
.tabs__alt .tab-menu a.active,
.sf-menu > li.current-menu-item > a:before,
.sf-menu > li.current-menu-parent > a:before,
.sf-menu > li.current-menu-item > a,
.sf-menu > li.current-menu-parent > a,
.page-title-holder h1:after {
	background:<?php echo of_get_option('color_accent', '#eb593c'); ?>;
}
.tabs .tab-menu,
.header-top {
	border-bottom-color: <?php echo of_get_option('color_accent', '#eb593c'); ?>;
}
.featured-thumb .border,
.commentlist li.bypostauthor .avatar {
	border-color: <?php echo of_get_option('color_accent', '#eb593c'); ?>;
}
<?php endif; ?>


<?php if(of_get_option('color_primary', '#6e9b76')): ?>
/* Primary Color */
::-moz-selection {
	background: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
::selection {
	background: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
.elastislide-wrapper nav span:hover,
.single-pricing-table .pr-foot a,
.dropcap-style3,
.btn-style3:hover,
.btn-style2,
.btn:hover,
button:hover,
input[type="submit"]:hover,
input[type="reset"]:hover,
input[type="button"]:hover,
.pagination li a:hover,
.widget_tag_cloud a:hover,
.project-feed-filter .current,
.project-feed-filter .current:hover,
.flex-control-paging li a.flex-active,
.flex-control-paging li a:hover {
	background: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
.flex-next:hover,
.flex-prev:hover {
	background-color: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
h1, h3, h5,
.accordion-wrapper .acc-head a,
.footer-widgets a,
.posts-list .post-meta a,
.project-feed-filter a,
.commentlist li .comment-reply a,
.item-team hgroup h5,
.tabs__alt .tab-menu a:hover,
.post-meta span a,
.post-meta [class^="icon-"],
#logo .tagline,
.item-team hgroup h5,
.sf-menu li li a,
.sf-menu > li > a span {
	color: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
h2, h4, h6 {
	color: <?php echo of_get_option('color_accent', '#6e9b76'); ?>;
}
.project-desc {
	border-left-color: <?php echo of_get_option('color_primary', '#6e9b76'); ?>;
}
<?php endif; ?>


<?php if(of_get_option('color_secondary', '#bdd4c1')): ?>
/* Secondary Color */
.elastislide-wrapper nav span,
.dropcap-style1,
.btn-style2:hover,
.pagination li a,
.widget_tag_cloud a,
.project-feed-filter a,
.format-icon,
.post-loop li,
.tabs__alt .tab-menu li:before,
.header-main .hr-bottom:after,
.sf-menu li li a:hover,
.sf-menu li li.sfHover > a,
.sf-menu li li.current-menu-item > a,
.sf-menu li li.current-menu-parent > a {
	background: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
.footer-widgets a:hover,
.contact-widget address i,
.twitter_update_list li .icon-twitter,
.widget__footer.widget_archive ul li,
.widget__footer.widget_nav_menu ul li,
.widget__footer.widget_meta ul li,
.widget__footer.widget_pages ul li,
.widget__footer.widget_recent_comments ul li,
.widget__footer.widget_recent_entries ul li,
.widget__footer.widget_categories ul li,
.widget__footer h5,
.post-loop .post-cats a,
.post-loop .post-cats {
	color: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
.single-pricing-table {
	border-top-color: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
.item-team hgroup,
.header-main .hr-bottom,
.sf-menu > li > ul > li:first-child > a:hover:before,
.sf-menu > li > ul > li.sfHover:first-child > a:before,
.sf-menu > li > ul > li.current-menu-item > a:before,
.sf-menu > li > ul > li.current-menu-parent > a:before {
	border-bottom-color: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
blockquote {
	border-left-color: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
.flickr-widget_thumb_holder .inset-border {
	border-color: <?php echo of_get_option('color_secondary', '#bdd4c1'); ?>;
}
<?php endif; ?>


<?php if(of_get_option('social_anim', '#282828')): ?>
/* Tertiary Color */
.tabs .tab-menu a,
.posts-list h4 a,
.project-desc h3 a,
.commentlist li .comment-author .fn,
.entry-title a,
.post-meta span a:hover [class^="icon-"],
.post-meta span a:hover,
.project-details h3,
.post-header h3 a,
.page-title h1,
.ico-box h4,
.progress-bar h4,
#logo h1 a,
#logo h2 a,
.flex-caption h2,
.sf-menu > li.current-menu-item > a span,
.sf-menu > li.current-menu-parent > a span {
	color: <?php echo of_get_option('color_tertiary', '#282828'); ?>;
}
.accordion-wrapper .acc-head a:before,
.dropcap-style4,
.btn-style3,
.flex-control-paging li a {
	background: <?php echo of_get_option('color_tertiary', '#282828'); ?>;
}
.flex-direction-nav a {
	background-color: <?php echo of_get_option('color_tertiary', '#282828'); ?>;
}
<?php endif; ?>

<?php if(of_get_option('social_anim', '1')): ?>
.social-links li a {
	-webkit-transition: all 0.15s ease-out;
	-moz-transition: all 0.15s ease-out;
	transition: all 0.15s ease-out;
}
<?php endif; ?>

<?php echo htmlspecialchars_decode(of_get_option('custom_css')); ?>
<?php echo '</style>';
}?>