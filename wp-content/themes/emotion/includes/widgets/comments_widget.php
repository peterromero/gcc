<?php
add_action('widgets_init', 'comments_load_widgets');

function comments_load_widgets()
{
  register_widget('Comments_Widget');
}

class Comments_Widget extends WP_Widget {
  
  function Comments_Widget()
  {
    $widget_ops = array('classname' => 'comments-list', 'description' => 'Advanced Recent Comments');

    $control_ops = array('id_base' => 'comments-list');

    $this->WP_Widget('comments-list', 'Emotion: Comments', $widget_ops, $control_ops);
  }
  
  function widget($args, $instance)
  {
    extract($args);
    $title = apply_filters('widget_title', $instance['title']);
    $comments = $instance['comments'];

    echo $before_widget;

    if($title) {
      echo $before_title.$title.$after_title;
    }
    ?>

    

    <ul class="comments-list unstyled clearfix">
      <?php
      $number = $instance['comments'];
      global $wpdb;
      $recent_comments = "SELECT DISTINCT ID, post_title, post_password, comment_ID, comment_post_ID, comment_author, comment_author_email, comment_date_gmt, comment_approved, comment_type, comment_author_url, SUBSTRING(comment_content,1,110) AS com_excerpt FROM $wpdb->comments LEFT OUTER JOIN $wpdb->posts ON ($wpdb->comments.comment_post_ID = $wpdb->posts.ID) WHERE comment_approved = '1' AND comment_type = '' AND post_password = '' ORDER BY comment_date_gmt DESC LIMIT $number";
      $the_comments = $wpdb->get_results($recent_comments);
      foreach($the_comments as $comment) { ?>

      <?php if(defined('ICL_LANGUAGE_CODE')) { ?>

      <?php $comment_id = icl_object_id($comment->comment_ID, 'comment', false, ICL_LANGUAGE_CODE);
        if($comment_id){ ?>
        <li>
          <?php echo get_avatar($comment, '52'); ?>
          <div class="post-holder">
            <h5>
              <?php echo strip_tags($comment->comment_author); ?> <span>says:</span>
            </h5>
            <div class="meta">
              <a class="comment-text-side" href="<?php echo get_permalink($comment->ID); ?>#comment-<?php echo $comment->comment_ID; ?>" title="<?php echo strip_tags($comment->comment_author); ?> on <?php echo $comment->post_title; ?>"><?php echo emotion_string_limit_words(strip_tags($comment->com_excerpt), 9); ?></a>
            </div>
          </div>
        </li>
        <?php }//close if $comment_id ?>

      <?php } else { ?>
      <li>
          <?php echo get_avatar($comment, '52'); ?>
          <div class="post-holder">
            <h5>
              <?php echo strip_tags($comment->comment_author); ?> <span>says:</span>
            </h5>
            <div class="meta">
              <a class="comment-text-side" href="<?php echo get_permalink($comment->ID); ?>#comment-<?php echo $comment->comment_ID; ?>" title="<?php echo strip_tags($comment->comment_author); ?> on <?php echo $comment->post_title; ?>"><?php echo emotion_string_limit_words(strip_tags($comment->com_excerpt), 9); ?></a>
            </div>
          </div>
        </li>
      <?php } ?>

      
      <?php } ?>
    </ul>
    
    
    <?php
    echo $after_widget;
  }
  
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;

    $instance['title'] = strip_tags($new_instance['title']);
    $instance['comments'] = $new_instance['comments'];
    
    return $instance;
  }

  function form($instance)
  {
    $defaults = array(
      'title' => 'Recent Comments',
      'comments' => 3
    );
    $instance = wp_parse_args((array) $instance, $defaults); ?>
    
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo _e('Title:', 'emotion') ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
    </p>
    
    </p>
      <label for="<?php echo $this->get_field_id('comments'); ?>"><?php echo _e('Number of comments:', 'emotion') ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('comments'); ?>" name="<?php echo $this->get_field_name('comments'); ?>" value="<?php echo $instance['comments']; ?>" />
    </p>

  <?php
  }
}
?>