<?php
add_action('widgets_init', 'latest_load_widgets');

function latest_load_widgets()
{
  register_widget('Posts_Widget');
}

class Posts_Widget extends WP_Widget {
  
  function Posts_Widget()
  {
    $widget_ops = array('classname' => 'latest-posts', 'description' => 'Latest posts.');

    $control_ops = array('id_base' => 'latest-posts');

    $this->WP_Widget('latest-posts', 'Emotion: Latest Posts', $widget_ops, $control_ops);
  }
  
  function widget($args, $instance)
  {
    extract($args);
    $title = apply_filters('widget_title', $instance['title']);
    $postscount = $instance['postscount'];

    echo $before_widget;

    if($title) {
      echo $before_title.$title.$after_title;
    }
    ?>

    <ul class="posts-list unstyled clearfix">
      <?php
      $pp = new WP_Query("orderby=date&posts_per_page=".$postscount."&suppress_filters=0"); ?>
      <?php while ($pp->have_posts()) : $pp->the_post();
      $format = get_post_format();
      ?>
      <li>
        <?php if(has_post_thumbnail()) { ?>
        <!-- begin post image -->
        <figure class="featured-thumb">
          <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_post_thumbnail('small'); ?></a>
        </figure>
        <!-- end post image -->
        <?php } else { ?>

          <?php if($format=="link") { ?>
            <!-- link placeholder -->
            <figure class="featured-thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/empty-link__small.jpg" alt=""></a>
            </figure>
            <!-- link placeholder -->
          <?php } elseif($format=="gallery") { ?>
            <!-- gallery placeholder -->
            <figure class="featured-thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/empty-gallery__small.jpg" alt=""></a>
            </figure>
            <!-- gallery placeholder -->
          <?php } elseif($format=="video") { ?>
            <!-- video placeholder -->
            <figure class="featured-thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/empty-video__small.jpg" alt=""></a>
            </figure>
            <!-- video placeholder -->
          <?php } elseif($format=="quote") { ?>
            <!-- quote placeholder -->
            <figure class="featured-thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/empty-quote__small.jpg" alt=""></a>
            </figure>
            <!-- quote placeholder -->
          <?php } else { ?>
            <!-- standard placeholder -->
            <figure class="featured-thumb">
            <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><img src="<?php echo get_template_directory_uri() ?>/images/empty-standard__small.jpg" alt=""></a>
            </figure>
            <!-- standard placeholder -->
          <?php } ?>

        <?php } ?>
        <h4><a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>"><?php the_title(); ?></a></h4>
        <p class="post-meta"><?php the_time('F j, Y'); ?> <?php _e('by', 'emotion'); ?> <a href="<?php echo get_author_posts_url(get_the_author_meta( 'ID' )) ?>"><?php echo get_the_author(); ?></a></p>
      </li>
      <?php endwhile; ?>
    </ul>
    
    <?php
    echo $after_widget;
  }
  
  function update($new_instance, $old_instance)
  {
    $instance = $old_instance;

    $instance['title'] = strip_tags($new_instance['title']);
    $instance['postscount'] = $new_instance['postscount'];
    
    return $instance;
  }

  function form($instance)
  {
    $defaults = array(
      'title' => 'Latest Posts',
      'postscount' => 4
    );
    $instance = wp_parse_args((array) $instance, $defaults); ?>
    
    <p>
      <label for="<?php echo $this->get_field_id('title'); ?>"><?php echo _e('Title:', 'emotion') ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" value="<?php echo $instance['title']; ?>" />
    </p>
    
    </p>
      <label for="<?php echo $this->get_field_id('postscount'); ?>"><?php echo _e('Number of posts:', 'emotion') ?></label>
      <input class="widefat" id="<?php echo $this->get_field_id('postscount'); ?>" name="<?php echo $this->get_field_name('postscount'); ?>" value="<?php echo $instance['postscount']; ?>" />
    </p>

  <?php
  }
}
?>