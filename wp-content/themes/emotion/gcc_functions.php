<?php

function get_gcc_roster_data() {
  $json = file_get_contents("https://docs.google.com/spreadsheet/tq?key=0Ai3EcOtM24yCdEwxNWdxeXI0aEtNSjVSbzdPTzRqRkE&headers=0&range=A1%3AD221&gid=0");
  $json = substr($json, strpos($json, '{'), -2);
  $spreadsheet_contents = json_decode($json);

  // Arrays to store singer names.
  $peak_names = array();
  $summit_names = array();
  $valley_names = array();
  $all_names = array();

  // Column references in the Google doc.
  $first_name_column = 1;  // First name
  $last_name_column = 2;   // Last name
  $current_column = 0;     // The "X" that indicates whether a singer is currently an active member
  $ensemble_column = 3;    // Ensemble

  foreach ($spreadsheet_contents->table->rows as $row) {
    if ($row->c[$current_column]->v == 'X') {

      $all_names[] = array(
        'first' => $row->c[$first_name_column]->v,
        'last' => $row->c[$last_name_column]->v
      );

      switch ($row->c[$ensemble_column]->v) {
        case 'Peak Performers':
          $peak_names[] = array(
            'first' => $row->c[$first_name_column]->v,
            'last' => $row->c[$last_name_column]->v
          );
          break;
        case 'Summit Singers':
          $summit_names[] = array(
            'first' => $row->c[$first_name_column]->v,
            'last' => $row->c[$last_name_column]->v
          );
          break;
        case 'Valley Voices':
          $valley_names[] = array(
            'first' => $row->c[$first_name_column]->v,
            'last' => $row->c[$last_name_column]->v
          );
          break;
      }
    }
  }

  usort($peak_names, 'sort_name');
  usort($summit_names, 'sort_name');
  usort($valley_names, 'sort_name');
  usort($all_names, 'sort_name');

  $peak_count = count($peak_names);
  $summit_count = count($summit_names);
  $valley_count = count($valley_names);
  $all_count = count($all_names);

  foreach ($peak_names as &$singer) {
    $singer = implode(" ", $singer);
  }
  $peak_names = implode(", ", $peak_names);

  foreach ($summit_names as &$singer) {
    $singer = implode(" ", $singer);
  }
  $summit_names = implode(", ", $summit_names);

  foreach ($all_names as &$singer) {
    $singer = implode(" ", $singer);
  }
  $all_names = implode(", ", $all_names);

  foreach ($valley_names as &$singer) {
    $singer = implode(" ", $singer);
  }
  $valley_names = implode(", ", $valley_names);

  return array(
    'peak' => array(
      'count' => $peak_count,
      'list' => $peak_names
    ),
    'summit' => array(
      'count' => $summit_count,
      'list' => $summit_names
    ),
    'valley' => array(
      'count' => $valley_count,
      'list' => $valley_names
    ),
    'all' => array(
      'count' => $all_count,
      'list' => $all_names
    ),
  );
}

function sort_name($a, $b) {
  return strcasecmp($a['last'], $b['last']);
}

function get_gcc_repertoire_data() {
  // The highest row index to search. Increase if you add too many songs to the
  // spreadsheet.
  $max_search_row = 300;

  // Read in the Master sheet from the Repertoire workbook.
  $master_sheet = file_get_contents("https://docs.google.com/spreadsheet/tq?key=1iCFWQqWbjq9HGowbFQ7lxo0ZQ6aKsv3Q71vDGvuJUP8&headers=0&range=A1:I{$max_search_row}&gid=1");
  $master_sheet = json_decode(substr($master_sheet, strpos($master_sheet, '{'), -2));

  $concerts = [];

  // The zero-based indices of the columns which contain concert selections
  // (A = 0, etc.)
  $first_concert_column = 1;
  $last_concert_column = 6;

  // Grab the concert titles.
  for ($column = $first_concert_column; $column <= $last_concert_column; $column++) {
    $concerts[$column] = [
      'title' => $master_sheet->table->rows[0]->c[$column]->v,
      'songs' => []
    ];
  }

  // Lookup array for decoding single-letter ensemble codes to real names.
  $ensemble_names = [
    'A' => 'All',
    'P' => 'Peak Performers',
    'S' => 'Summit Singers',
    'V' => 'Valley Voices'
  ];

  // Grab the songs for each concert.
  foreach ($master_sheet->table->rows as $i => $row) {
    if ($i > 0) {
      for ($column = $first_concert_column; $column <= $last_concert_column; $column++) {
        if ($row->c[$column]->v) {
          $ensembles = str_split($row->c[$column]->v);
          foreach ($ensembles as $ensemble) {
            // To program a song in the spreadsheet that won't show up in this
            // list, just use an asterisk or some other single letter than A, P,
            // S, or V.
            if (!empty($ensemble_names[$ensemble])) {
              $concerts[$column]['songs'][$ensemble_names[$ensemble]][$row->c[0]->v] = [
                'title' => $row->c[7]->v
              ];
            }
          }
        }
      }
    }
  }

  // Sort concert tabs by numeric index. Concert columns must be named following
  // this pattern: "3 Winter's Eve (Dec. 9)". The 3 will be used to sort and
  // then stripped off.
  usort($concerts, function($a, $b) {
    if ($a['title'] < $b['title']) {
      return -1;
    }
    if ($a['title'] > $b['title']) {
      return 1;
    }
    return 0;
  });

  // Strip off the leading digits used for ordering concerts.
  foreach ($concerts as &$concert) {
    $concert['title'] = preg_replace('/\d+ (.*)/', '$1', $concert['title']);
  }
  // Unset this since we're going to be looping over $concerts again but without
  // a reference.
  unset($concert);

  // Load the sheet with the recording data.
  $recording_sheet = file_get_contents("https://docs.google.com/spreadsheet/tq?key=1iCFWQqWbjq9HGowbFQ7lxo0ZQ6aKsv3Q71vDGvuJUP8&headers=0&range=A1:J{$max_search_row}&gid=241911716");
  $recording_sheet = json_decode(substr($recording_sheet, strpos($recording_sheet, '{'), -2));

  // Process the recording data into a format that can more easily be searched.
  $table_data = $recording_sheet->table->rows;
  unset($table_data[0]);
  $recordings = [];
  foreach ($table_data as $row) {
    if ($row->c[4]) {
      $recordings[$row->c[0]->v][] = $row;
    }
  }

  // Save some memory.
  unset($recording_sheet);
  unset($table_data);

  // Create output of Zilla shortcodes for concert tabs.
  $output = '[tabs_new type="hor"]' . "\n";
  foreach ($concerts as $concert) {
    if (!empty($concert['songs'])) { // Don't output empty concerts.
      $output .= '[tab title="' . $concert['title'] . '"]' . "\n";

      foreach ($concert['songs'] as $ensemble => $song_list) {

        // Sort songs by title.
        uasort($song_list, function($a, $b) {
          if ($a['title'] < $b['title']) {
            return -1;
          }
          if ($a['title'] > $b['title']) {
            return 1;
          }
          return 0;
        });

        $output .= "<h3>$ensemble</h3>\n";
        $output .= '[list style="star"]' . "\n";
        $output .= '<ul>' . "\n";
        foreach ($song_list as $id => $song) {
          if (!empty($recordings[$id])) {
            if (count($recordings[$id]) > 1) {

              // Sort recordings within a single song by title.
              usort($recordings[$id], function($a, $b) {
                if ($a->c[9]->v < $b->c[9]->v) {
                  return -1;
                }
                if ($a->c[9]->v > $b->c[9]->v) {
                  return 1;
                }
                return 0;
              });

              $output .= "<li><strong>{$recordings[$id][0]->c[1]->v}</strong><ul>";
              foreach ($recordings[$id] as $recording) {
                $output .= "<li><a href='{$recording->c[4]->v}' target='_blank'>{$recording->c[9]->v}</a></li>";
              }
              $output .= "</ul></li>";
            }
            else {
              $output .= "<li><a href='{$recordings[$id][0]->c[4]->v}' target='_blank'>{$recordings[$id][0]->c[1]->v}: {$recordings[$id][0]->c[9]->v}</a></li>\n";
            }
          }
          else {
            $output .= "<li><em>{$song['title']} (no recording available)</em></li>";
          }
        }
        $output .= '</ul>' . "\n";
        $output .= '[/list]' . "\n";
      }
      $output .= '[/tab]' . "\n";
    }
  }
  $output .= '[/tabs_new]' . "\n";

  return $output;
}

?>
