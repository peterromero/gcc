<?php if ( ! dynamic_sidebar( 'Sidebar' )) : ?>

<div id="sidebar-search" class="widget">
	<?php echo '<h4>' . __('Search', 'emotion') . '</h4>'; ?>
	<?php get_search_form(); ?> <!-- outputs the default Wordpress search form-->
</div>

<?php endif; ?>