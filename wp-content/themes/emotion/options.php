<?php
/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 * By default it uses the theme name, in lowercase and without spaces, but this can be changed if needed.
 * If the identifier changes, it'll appear as if the options have been reset.
 */

function optionsframework_option_name() {

	// This gets the theme name from the stylesheet
	$themename = get_option( 'stylesheet' );
	$themename = preg_replace("/\W/", "_", strtolower($themename) );

	$optionsframework_settings = get_option( 'optionsframework' );
	$optionsframework_settings['id'] = $themename;
	update_option( 'optionsframework', $optionsframework_settings );
}

/**
 * Defines an array of options that will be used to generate the settings page and be saved in the database.
 * When creating the 'id' fields, make sure to use all lowercase and no spaces.
 *
 */

if(!function_exists('optionsframework_options')) {
	function optionsframework_options() {

		// Logo type
		$logo_type = array(
			"image_logo" => __("Image Logo", 'emotion'),
			"text_logo" => __("Text Logo", 'emotion')
		);

		// Background Defaults
		$background_defaults = array(
			'color' => '',
			'image' => '',
			'repeat' => 'repeat',
			'position' => 'top center',
			'attachment'=>'scroll' );

		// Body Background Defaults
		$bg_body_defaults = array(
			'color' => '#f3f1ee',
			'image' => get_template_directory_uri() . '/images/pattern.gif',
			'repeat' => 'repeat',
			'position' => 'top center',
			'attachment'=>'scroll' );

		// Header Top Background Defaults
		$bg_header_top_defaults = array(
			'color' => '#2e3438',
			'image' => get_template_directory_uri() . '/images/pattern-alt.gif',
			'repeat' => 'repeat',
			'position' => 'top center',
			'attachment'=>'scroll' );

		// Footer widgets Background Defaults
		$bg_footer_widgets_defaults = array(
			'color' => '#2e3438',
			'image' => get_template_directory_uri() . '/images/pattern-alt.gif',
			'repeat' => 'repeat',
			'position' => 'top center',
			'attachment'=>'scroll' );

		// Fonts
		$typography_mixed_fonts = array_merge( options_typography_get_os_fonts() , options_typography_get_google_fonts() );
		asort($typography_mixed_fonts);

		// Body Typography
		$typography_body = array(
			'size' => '12px',
			'face' => 'Arial, Helvetica, sans-serif',
			'style' => 'normal',
			'color' => '#5c5c5c'
		);
		// Typography Options
		$typography_opt = array(
			'faces' => $typography_mixed_fonts
		);

		// Headings Typography
		$typography_headings = array(
			'size' => false,
			'face' => 'Oswald',
			'style' => 'normal',
			'color' => false
		);
		// Headings Typography Options
		$typography_headings_opt = array(
			'sizes' => false,
			'faces' => $typography_mixed_fonts,
			'color' => false
		);

		// Menu Typography
		$typography_menu = array(
			'size' => '16px',
			'face' => 'Oswald',
			'style' => 'normal',
			'color' => '#282828'
		);
		// Typography Options
		$typography_menu_opt = array(
			'faces' => $typography_mixed_fonts
		);

		

		// Pull all the categories into an array
		$options_categories = array();
		$options_categories_obj = get_categories();
		foreach ($options_categories_obj as $category) {
			$options_categories[$category->cat_ID] = $category->cat_name;
		}
		
		// Pull all tags into an array
		$options_tags = array();
		$options_tags_obj = get_tags();
		foreach ( $options_tags_obj as $tag ) {
			$options_tags[$tag->term_id] = $tag->name;
		}


		// Pull all the pages into an array
		$options_pages = array();
		$options_pages_obj = get_pages('sort_column=post_parent,menu_order');
		$options_pages[''] = 'Select a page:';
		foreach ($options_pages_obj as $page) {
			$options_pages[$page->ID] = $page->post_title;
		}

		// If using image radio buttons, define a directory path
		$imagepath =  get_template_directory_uri() . '/admin/images/';

		$options = array();

		$options[] = array( "name" => "General",
						"type" => "heading");

		$options['logo_type'] = array( 
							"name" => __("What kind of logo?", 'emotion'),
							"desc" => __("If you select '<em>Image Logo</em>' you can put in the image url in the next option, and if you select '<em>Text Logo</em>' your Site Title will show instead.", 'emotion'),
							"id" => "logo_type",
							"std" => "image_logo",
							"type" => "radio",
							"options" => $logo_type);

		$options['logo_url'] = array( 
							"name" => __("Logo Image", 'emotion'),
							"desc" => __("Upload a custom logo for your Site.", 'emotion'),
							"id" => "logo_url",
							"std" => get_template_directory_uri() . "/images/logo.png",
							"type" => "upload");
							
		$options['favicon'] = array( 
							"name" => __("Favicon", 'emotion'),
							"desc" => __("Upload a custom favicon for your Site. <i><strong>Format</strong>: ico</i> <i><strong>Size</strong>: 16x16</i>", 'emotion'),
							"id" => "favicon",
							"std" => get_template_directory_uri() . "/images/favicon.ico",
							"type" => "upload");

		$options['favicon_iphone'] = array( 
							"name" => __("iPhone Favicon", 'emotion'),
							"desc" => __("Upload a custom iPhone favicon for your Site. <i><strong>Format</strong>: png</i> <i><strong>Size</strong>: 57x57</i> <i>For iPhone 2G/3G/3GS, 2011 iPod Touch and older Android devices </i>", 'emotion'),
							"id" => "favicon_iphone",
							"std" => get_template_directory_uri() . "/images/apple-touch-icon.png",
							"type" => "upload");

		$options['favicon_ipad'] = array( 
							"name" => __("iPad Favicon", 'emotion'),
							"desc" => __("Upload a custom iPad favicon for your Site. <i><strong>Format</strong>: png</i> <i><strong>Size</strong>: 72x72</i> <i>For 1st generation iPad, iPad 2 and iPad mini.</i>", 'emotion'),
							"id" => "favicon_ipad",
							"std" => get_template_directory_uri() . "/images/apple-touch-icon-72x72.png",
							"type" => "upload");

		$options['favicon_iphone_retina'] = array( 
							"name" => __("iPhone Retina Favicon", 'emotion'),
							"desc" => __("Upload a custom favicon for iPhone/Touch Retina based devices. <i><strong>Format</strong>: png</i> <i><strong>Size</strong>: 114x114</i> <i>For iPhone 4, 4S, 5 and 2012 iPod Touch.</i>", 'emotion'),
							"id" => "favicon_iphone_retina",
							"std" => get_template_directory_uri() . "/images/apple-touch-icon-114x114.png",
							"type" => "upload");

		$options['favicon_ipad_new'] = array( 
							"name" => __("iPad 3+ Favicon", 'emotion'),
							"desc" => __("Upload a custom favicon for iPad 3rd and 4th generation. <i><strong>Format</strong>: png</i> <i><strong>Size</strong>: 144x144</i> <i>For iPad 3rd and 4th generation.</i>", 'emotion'),
							"id" => "favicon_ipad_new",
							"std" => get_template_directory_uri() . "/images/apple-touch-icon-144x144.png",
							"type" => "upload");

		$options[] = array( 
							"name" => __("Show footer widgets?", 'emotion'),
							"desc" => __("Do you want to show footer widgets?", 'emotion'),
							"id" => "footer_widgets",
							"std" => "yes",
							"type" => "select",
							"class" => "mini",
							"options" => array('yes' => 'Yes', 'no' => 'No'));

		$options['copyright'] = array( 
							"name" => __("Copyright", 'emotion'),
							"desc" => __("Enter text used in the left side of the footer. HTML tags are allowed.", 'emotion'),
							"id" => "copyright",
							"std" => "&copy; 2013 Emotion Theme | All rights reserved",
							"type" => "textarea");
		
		$options[] = array( 
							"name" => __("Google Analytics Code", 'emotion'),
							"desc" => __("You can paste your Google Analytics or other tracking code in this box. This will be automatically added to the footer. <em>Paste your code without &lt;script&gt; tag</em>", 'emotion'),
							"id" => "ga_code",
							"std" => "",
							"type" => "textarea");

		$options[] = array( 
							"name" => __("Custom CSS", 'emotion'),
							"desc" => __("Want to add any custom CSS code? Put in here, and the rest is taken care of. This overrides any other stylesheets. eg: a {color:red}", 'emotion'),
							"id" => "custom_css",
							"std" => "",
							"type" => "textarea");




		$options[] = array( "name" => "Styling",
							"type" => "heading");


		$options['bg_body'] = array(
							'name' =>  __('Body Background', 'emotion'),
							'desc' => __('Change the body background.', 'emotion'),
							'id' => 'bg_body',
							'std' => $bg_body_defaults,
							'type' => 'background' );

		$options['bg_header_top'] = array(
							'name' =>  __('Top Header Background', 'emotion'),
							'desc' => __('Change the top header background.', 'emotion'),
							'id' => 'bg_header_top',
							'std' => $bg_header_top_defaults,
							'type' => 'background' );

		$options['bg_footer_widgets'] = array(
							'name' =>  __('Footer Widgets Background', 'emotion'),
							'desc' => __('Change the footer widgets background.', 'emotion'),
							'id' => 'bg_footer_widgets',
							'std' => $bg_footer_widgets_defaults,
							'type' => 'background' );

		$options['bg_footer'] = array(
							'name' => __('Footer Background Color', 'emotion'),
							'desc' => __('Change the footer background color.', 'emotion'),
							'id' => 'bg_footer',
							'std' => '#040404',
							'type' => 'color' );


		$options['color_accent'] = array(
							'name' => __('Accent Color', 'emotion'),
							'desc' => __('Theme accent color.', 'emotion'),
							'id' => 'color_accent',
							'std' => '#eb593c',
							'type' => 'color' );

		$options['color_primary'] = array(
							'name' => __('Primary Color', 'emotion'),
							'desc' => __('Theme primary color.', 'emotion'),
							'id' => 'color_primary',
							'std' => '#6e9b76',
							'type' => 'color' );

		$options['color_secondary'] = array(
							'name' => __('Secondary Color', 'emotion'),
							'desc' => __('Theme secondary color.', 'emotion'),
							'id' => 'color_secondary',
							'std' => '#bdd4c1',
							'type' => 'color' );

		$options['color_tertiary'] = array(
							'name' => __('Tertiary Color', 'emotion'),
							'desc' => __('Theme tertiary color.', 'emotion'),
							'id' => 'color_tertiary',
							'std' => '#282828',
							'type' => 'color' );


		$options['typography_body'] = array( 'name' => __('Body typography', 'emotion'),
							'desc' => __('Body typography.', 'emotion'),
							'id' => "typography_body",
							'std' => $typography_body,
							'type' => 'typography',
							'options' => $typography_opt 
							);

		$options['typography_menu'] = array( 'name' => __('Navigation typography', 'emotion'),
							'desc' => __('Customize typography for main navigation.', 'emotion'),
							'id' => "typography_menu",
							'std' => $typography_menu,
							'type' => 'typography',
							'options' => $typography_menu_opt 
							);

		$options['typography_heading'] = array( 'name' => __('Headings typography', 'emotion'),
							'desc' => __("Select a custom font to be used for your website's headings and titles. (* - <em>Google Web Font</em>)", 'emotion'),
							'id' => "typography_heading",
							'std' => $typography_headings,
							'type' => 'typography',
							'options' => $typography_headings_opt
							);



		$options[] = array( "name" => "Social",
							"type" => "heading");

		$options['social_anim'] = array(
							'name' => __('Animation for Social Links', 'emotion'),
							"desc" => __("The animation for Social Links is enabled by default. <em>Un-check this box to disable this functionality.</em>", 'emotion'),
							'id' => 'social_anim',
							'std' => '1',
							'type' => 'checkbox');

		$options['social_twitter'] = array(
							'name' => __('Twitter Link', 'emotion'),
							'desc' => __('Enter link to your Twitter account.', 'emotion'),
							'id' => 'social_twitter',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_facebook'] = array(
							'name' => __('Facebook Link', 'emotion'),
							'desc' => __('Enter link to your Facebook account.', 'emotion'),
							'id' => 'social_facebook',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_google'] = array(
							'name' => __('Google+ Link', 'emotion'),
							'desc' => __('Enter link to your Google+ account.', 'emotion'),
							'id' => 'social_google',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_dribbble'] = array(
							'name' => __('Dribbble Link', 'emotion'),
							'desc' => __('Enter link to your Dribbble account.', 'emotion'),
							'id' => 'social_dribbble',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_vimeo'] = array(
							'name' => __('Vimeo Link', 'emotion'),
							'desc' => __('Enter link to your Vimeo account.', 'emotion'),
							'id' => 'social_vimeo',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_youtube'] = array(
							'name' => __('YouTube Link', 'emotion'),
							'desc' => __('Enter link to your YouTube account.', 'emotion'),
							'id' => 'social_youtube',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_pinterest'] = array(
							'name' => __('Pinterest Link', 'emotion'),
							'desc' => __('Enter link to your Pinterest account.', 'emotion'),
							'id' => 'social_pinterest',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_instagram'] = array(
							'name' => __('Instagram Link', 'emotion'),
							'desc' => __('Enter link to your Instagram account.', 'emotion'),
							'id' => 'social_instagram',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_linkedin'] = array(
							'name' => __('LinkedIn Link', 'emotion'),
							'desc' => __('Enter link to your LinkedIn account.', 'emotion'),
							'id' => 'social_linkedin',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_behance'] = array(
							'name' => __('Behance Link', 'emotion'),
							'desc' => __('Enter link to your Behance account.', 'emotion'),
							'id' => 'social_behance',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_skype'] = array(
							'name' => __('Skype name', 'emotion'),
							'desc' => __('Enter your Skype name.', 'emotion'),
							'id' => 'social_skype',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_tumblr'] = array(
							'name' => __('Tumblr Link', 'emotion'),
							'desc' => __('Enter link to your Tumblr account.', 'emotion'),
							'id' => 'social_tumblr',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');

		$options['social_rss'] = array(
							'name' => __('RSS Link', 'emotion'),
							'desc' => __('Enter link to your rss feed.', 'emotion'),
							'id' => 'social_rss',
							'std' => '#',
							'class' => 'tiny',
							'type' => 'text');





		$options[] = array( 
							"name" => __("Slider", 'emotion'),
							"type" => "heading");

		$options['flex_effect'] = array( 
							"name" => __("Slider Effect", 'emotion'), 
							"desc" => __("Select your animation type.", 'emotion'),
							"id" => "flex_effect",
							"std" => 'fade',
							"type" => "select",
							"options" => array(
								'fade' => 'fade', 
								'slide' => 'slide'));

		$options['flex_startat'] = array( 
							"name" => __("Start at", 'emotion'), 
							"desc" => __("The slide that the slider should start on. Array notation (0 = first slide).", 'emotion'),
							"id" => "flex_startat",
							"std" => "0",
							"type" => "text",
							"class" => "tiny");

		$options['flex_autoplay'] = array( 
							"name" => __("Autoplay", 'emotion'), 
							"desc" => __("Auto play the slider or not.", 'emotion'),
							"id" => "flex_autoplay",
							"std" => "true",
							"type" => "radio",
							"options" => array(
								'false' => 'No',
								'true' => 'Yes'));

		$options['flex_slideshowspeed'] = array( 
							"name" => __("Slideshow Speed", 'emotion'), 
							"desc" => __("Set the speed of the slideshow cycling, in milliseconds", 'emotion'),
							"id" => "flex_slideshowspeed",
							"std" => "7000",
							"type" => "text",
							"class" => "tiny");

		$options['flex_controls'] = array( 
							"name" => __("Controls", 'emotion'),
							"desc" => __("Display slide controls (1,2,3 etc.)?", 'emotion'),
							"id" => "flex_controls",
							"std" => "true",
							"type" => "radio",
							"options" => array(
								'false' => 'No',
								'true' => 'Yes'));

		$options['flex_directionnav'] = array( 
							"name" => __("Previous/next", 'emotion'),
							"desc" => __("Create navigation for previous/next navigation?", 'emotion'),
							"id" => "flex_directionnav",
							"std" => "true",
							"type" => "radio",
							"options" => array(
								'false' => 'No',
								'true' => 'Yes'));





		$options[] = array( 
							"name" => __("Blog & Portfolio", 'emotion'),
							"type" => "heading");

		$options[] = array( 
							"name" => __("Blog Title", 'emotion'),
							"desc" => __("Enter Your Blog Title used on Blog page.", 'emotion'),
							"id" => "blog_text",
							"std" => "Blog",
							"type" => "text");
		
		$options['blog_sidebar'] = array( 
							"name" => __("Sidebar position", 'emotion'),
							"desc" => __("Choose sidebar position.", 'emotion'),
							"id" => "blog_sidebar",
							"std" => "right",
							"type" => "images",
							"options" => array(
								'left' => $imagepath . '2cl.png',
								'right' => $imagepath . '2cr.png')
							);

		$options['blog_post_excerpt'] = array(
							'name' => __('Post Excerpt', 'emotion'),
							"desc" => __("The excerpt is displayed by default. <em>Un-check this box to disable this functionality and display full blog post.</em>", 'emotion'),
							'id' => 'blog_post_excerpt',
							'std' => '1',
							'type' => 'checkbox');

		$options['post_meta'] = array(
							"name" => __("Post Meta Box", 'emotion'),
							"desc" => __("The metabox is displayed by default. <em>Un-check this box to disable this functionality and hide metaboxes.</em>", 'emotion'),
							"id" => "post_meta",
							'std' => '1',
							'type' => 'checkbox');

		$options['portfolio_items'] = array( 
							"name" => __("Portfolio Items per Page", 'emotion'),
							"desc" => __("Portfolio Items per Page.", 'emotion'),
							"id" => "portfolio_items",
							"std" => "12",
							"type" => "text");

		$options['portfolio_filter'] = array(
							'name' => __('Portfolio Filter', 'emotion'),
							"desc" => __("The filter for portfolio pages is enabled by default. <em>Un-check this box to disable this functionality.</em>", 'emotion'),
							'id' => 'portfolio_filter',
							'std' => '1',
							'type' => 'checkbox');

		$options['portfolio_lightbox'] = array(
							'name' => __('Portfolio Lightbox', 'emotion'),
							"desc" => __("The lightbox for portfolio post pages is enabled by default. <em>Un-check this box to disable this functionality.</em>", 'emotion'),
							'id' => 'portfolio_lightbox',
							'std' => '1',
							'type' => 'checkbox');

		$options['portfolio_title'] = array(
							"name" => __("Show portfolio post titles and excerpts?", 'emotion'),
							"desc" => __("Show or hide titles and excerpts for portfolio posts.", 'emotion'),
							"id" => "portfolio_title",
							"std" => "true",
							"type" => "radio",
							"options" => array(
											"true" => "Yes",
											"false" => "No"));

		$options['portfolio_related_title'] = array( 
							"name" => __("Portfolio Related Title", 'emotion'),
							"desc" => __("Enter Title used on Related Projects section.", 'emotion'),
							"id" => "portfolio_related_title",
							"std" => "Related Projects",
							"type" => "text");



		$options[] = array( 
							"name" => __("Contact", 'emotion'),
							"type" => "heading");

		$options[] = array( 
							"name" => __("Show Google map?", 'emotion'),
							"desc" => __("Do you want to show google map?", 'emotion'),
							"id" => "gmap_show",
							"std" => "yes",
							"type" => "select",
							"class" => "mini",
							"options" => array('yes' => 'Yes', 'no' => 'No'));

		$options['gmap_coord'] = array( 
							"name" => __("Google Map", 'emotion'),
							"desc" => __("Google map coordinates.", 'emotion'),
							"id" => "gmap_coord",
							"std" => "57.669645,11.926832",
							"type" => "text");

		$options['gmap_zoom'] = array( 
							"name" => __("Zoom Level", 'emotion'),
							"desc" => __("Google map zoom level.", 'emotion'),
							"id" => "gmap_zoom",
							"std" => "14",
							"type" => "text");

		$options['contact_title'] = array( 
							"name" => __("Contact Title", 'emotion'),
							"desc" => __("Title for contact info block.", 'emotion'),
							"id" => "contact_title",
							"std" => "Contact Info",
							"type" => "text");

		$options['contact_txt'] = array( 
							"name" => __("Contact description", 'emotion'),
							"desc" => __("Description used in contact info block.", 'emotion'),
							"id" => "contact_txt",
							"std" => "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Suspendisse eget erat magna. Pellentesque justo ante, sollicitudin eget, interdum id nibh.",
							"type" => "textarea");

		$options['contact_address'] = array( 
							"name" => __("Address", 'emotion'),
							"desc" => __("Put your address here.", 'emotion'),
							"id" => "contact_address",
							"std" => "24 Fifth st., Los Angeles, USA",
							"type" => "text");

		$options['contact_phone'] = array( 
							"name" => __("Phone", 'emotion'),
							"desc" => __("Put your phone here.", 'emotion'),
							"id" => "contact_phone",
							"std" => "+1 (003) 460-055-274",
							"type" => "text");

		$options['contact_mail'] = array( 
							"name" => __("Email", 'emotion'),
							"desc" => __("Put your email address here.", 'emotion'),
							"id" => "contact_mail",
							"std" => "info@dan-fisher.com",
							"type" => "text");

		$options['contact_site'] = array( 
							"name" => __("Site", 'emotion'),
							"desc" => __("Put your site url here.", 'emotion'),
							"id" => "contact_site",
							"std" => "www.dan-fisher.com",
							"type" => "text");

		return $options;

	};

}


/**
* Front End Customizer
*
* WordPress 3.4 Required
*/
add_action( 'customize_register', 'emotion_register' );

if(!function_exists('emotion_register')) {

	function emotion_register($wp_customize) {
		/**
		 * This is optional, but if you want to reuse some of the defaults
		 * or values you already have built in the options panel, you
		 * can load them into $options for easy reference
		 */
		$options = optionsframework_options();



		/*-----------------------------------------------------------------------------------*/
		/*	Logo
		/*-----------------------------------------------------------------------------------*/
		
		$wp_customize->add_section( 'emotion_logo', array(
			'title' => __( 'Logo', 'emotion' ),
			'priority' => 201
		) );
		
		/* Logo Type */
		$wp_customize->add_setting( 'emotion[logo_type]', array(
				'default' => $options['logo_type']['std'],
				'type' => 'option'
		) );
		$wp_customize->add_control( 'emotion_logo_type', array(
				'label' => $options['logo_type']['name'],
				'section' => 'emotion_logo',
				'settings' => 'emotion[logo_type]',
				'type' => $options['logo_type']['type'],
				'choices' => $options['logo_type']['options']
		) );
		
		/* Logo Path */
		$wp_customize->add_setting( 'emotion[logo_url]', array(
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'logo_url', array(
			'label' => $options['logo_url']['name'],
			'section' => 'emotion_logo',
			'settings' => 'emotion[logo_url]'
		) ) );
		
		
		/*-----------------------------------------------------------------------------------*/
		/*	Styling
		/*-----------------------------------------------------------------------------------*/
		$wp_customize->add_section( 'emotion_header', array(
			'title' => __( 'Styling', 'emotion' ),
			'priority' => 200
		));


		/* Body Background Image*/
		$wp_customize->add_setting( 'emotion[bg_body][image]', array(
			'default' => $options['bg_body']['std']['image'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_body_image', array(
			'label'   => 'Body background image',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_body][image]',
			'priority' => 14
		) ) );

		/* Body Background Color*/
		$wp_customize->add_setting( 'emotion[bg_body][color]', array(
			'default' => $options['bg_body']['std']['color'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_color_Control( $wp_customize, 'bg_body_color', array(
			'label'   => 'Body background color',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_body][color]',
			'priority' => 15
		) ) );


		/* Top Header Background Image*/
		$wp_customize->add_setting( 'emotion[bg_header_top][image]', array(
			'default' => $options['bg_header_top']['std']['image'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_header_top_image', array(
			'label'   => 'Top Header image',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_header_top][image]',
			'priority' => 16
		) ) );

		/* Top Header Background Color*/
		$wp_customize->add_setting( 'emotion[bg_header_top][color]', array(
			'default' => $options['bg_header_top']['std']['color'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_color_Control( $wp_customize, 'bg_header_top_color', array(
			'label'   => 'Top Header color',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_header_top][color]',
			'priority' => 17
		) ) );


		/* Footer Widgets Background Image*/
		$wp_customize->add_setting( 'emotion[bg_footer_widgets][image]', array(
			'default' => $options['bg_footer_widgets']['std']['image'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Image_Control( $wp_customize, 'bg_footer_widgets_image', array(
			'label'   => 'Footer widgets image',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_footer_widgets][image]',
			'priority' => 18
		) ) );

		/* Footer Widgets Background Color*/
		$wp_customize->add_setting( 'emotion[bg_footer_widgets][color]', array(
			'default' => $options['bg_footer_widgets']['std']['color'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_color_Control( $wp_customize, 'bg_footer_widgets_color', array(
			'label'   => 'Footer widgets color',
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_footer_widgets][color]',
			'priority' => 19
		) ) );


		/* Footer background color */
		$wp_customize->add_setting( 'emotion[bg_footer]', array(
			'default' => $options['bg_footer']['std'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'bg_footer', array(
			'label'   => $options['bg_footer']['name'],
			'section' => 'emotion_header',
			'settings'   => 'emotion[bg_footer]',
			'priority' => 20
		) ) );


		/* Accent color */
		$wp_customize->add_setting( 'emotion[color_accent]', array(
			'default' => $options['color_accent']['std'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_accent', array(
			'label'   => $options['color_accent']['name'],
			'section' => 'emotion_header',
			'settings'   => 'emotion[color_accent]',
			'priority' => 21
		) ) );


		/* Primary color */
		$wp_customize->add_setting( 'emotion[color_primary]', array(
			'default' => $options['color_primary']['std'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_primary', array(
			'label'   => $options['color_primary']['name'],
			'section' => 'emotion_header',
			'settings'   => 'emotion[color_primary]',
			'priority' => 22
		) ) );

		/* Secondary color */
		$wp_customize->add_setting( 'emotion[color_secondary]', array(
			'default' => $options['color_secondary']['std'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_secondary', array(
			'label'   => $options['color_secondary']['name'],
			'section' => 'emotion_header',
			'settings'   => 'emotion[color_secondary]',
			'priority' => 23
		) ) );

		/* Tertiary color */
		$wp_customize->add_setting( 'emotion[color_tertiary]', array(
			'default' => $options['color_tertiary']['std'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( new WP_Customize_Color_Control( $wp_customize, 'color_tertiary', array(
			'label'   => $options['color_tertiary']['name'],
			'section' => 'emotion_header',
			'settings'   => 'emotion[color_tertiary]',
			'priority' => 24
		) ) );


		/* Primary Heading font face */
		$wp_customize->add_setting( 'emotion[typography_heading][face]', array(
			'default' => $options['typography_heading']['std']['face'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( 'emotion_typography_heading', array(
			'label' => $options['typography_heading']['name'],
			'section' => 'emotion_header',
			'settings' => 'emotion[typography_heading][face]',
			'type' => 'select',
			'choices' => $options['typography_heading']['options']['faces'],
			'priority' => 25
		) );
		
		/* Body font face */
		$wp_customize->add_setting( 'emotion[typography_body][face]', array(
			'default' => $options['typography_body']['std']['face'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( 'emotion_typography_body', array(
			'label' => $options['typography_body']['name'],
			'section' => 'emotion_header',
			'settings' => 'emotion[typography_body][face]',
			'type' => 'select',
			'choices' => $options['typography_body']['options']['faces'],
			'priority' => 26
		) );

		/* Body font face */
		$wp_customize->add_setting( 'emotion[typography_menu][face]', array(
			'default' => $options['typography_menu']['std']['face'],
			'type' => 'option'
		) );
		
		$wp_customize->add_control( 'emotion_typography_menu', array(
			'label' => $options['typography_menu']['name'],
			'section' => 'emotion_header',
			'settings' => 'emotion[typography_menu][face]',
			'type' => 'select',
			'choices' => $options['typography_menu']['options']['faces'],
			'priority' => 27
		) );

		
		
	};

}