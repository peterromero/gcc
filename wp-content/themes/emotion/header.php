<!DOCTYPE html>
<!--[if IE 7]>                  <html <?php language_attributes(); ?> class="ie7 no-js"><![endif]-->
<!--[if lte IE 8]>              <html <?php language_attributes(); ?> class="ie8 no-js"><![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html <?php language_attributes(); ?> class="not-ie no-js">  <!--<![endif]-->
<head>

	<!-- Basic Page Needs
	================================================== -->
	<meta charset="<?php bloginfo('charset'); ?>">
	<title><?php wp_title(''); ?><?php if(wp_title('', false)) { echo ' :'; } ?> <?php bloginfo('name'); ?></title>

	<!-- Mobile Specific Metas
	================================================== -->
	<meta name="viewport" content="width=device-width,initial-scale=1.0,maximum-scale=1.0,user-scalable=0">

	<!-- Metas
	================================================== -->
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="<?php bloginfo('description'); ?>">
	
	<!-- dns prefetch -->
	<link href="//www.google-analytics.com" rel="dns-prefetch">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo of_get_option('favicon', '' ); ?>">
	<link rel="apple-touch-icon" href="<?php echo of_get_option('favicon_iphone', '' ); ?>">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo of_get_option('favicon_ipad', '' ); ?>">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo of_get_option('favicon_iphone_retina', '' ); ?>">
	<link rel="apple-touch-icon" sizes="144x144" href="<?php echo of_get_option('favicon_ipad_new', '' ); ?>">

	<!--[if lt IE 9]>
		<link rel="stylesheet" href="<?php echo get_template_directory_uri(); ?>/css/ie/ie8.css" media="screen" />
	<![endif]-->
	
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- CSS + JavaScript + jQuery  -->
	<?php wp_head(); ?>
	<script type="text/javascript">
		var templateDir = "<?php bloginfo('template_directory') ?>";
	</script>

</head>
<body <?php body_class(); ?>>

	<!-- BEGIN WRAPPER -->
	<div id="wrapper">
		
		<!-- Inner Wrapper -->
		<div class="wrapp-holder">
			<div class="wrap-frame">

				<!-- BEGIN HEADER -->
				<header id="header" class="header" role="banner">

					<!-- Top Header -->
					<div class="header-top">
						<div class="container clearfix">
							<div class="grid_12">
								<!-- Top Menu -->
								<?php emotion_nav_top(); ?>
								<!-- /Top Menu -->
								
								<!-- Social Links -->
								<ul class="social-links unstyled">
									<?php if(of_get_option('social_twitter')): ?>
									<li class="ico-twitter"><a href="<?php echo of_get_option('social_twitter'); ?>">Twitter</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_facebook')): ?>
									<li class="ico-facebook"><a href="<?php echo of_get_option('social_facebook'); ?>">Facebook</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_google')): ?>
									<li class="ico-googleplus"><a href="<?php echo of_get_option('social_google'); ?>">Google+</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_dribbble')): ?>
									<li class="ico-dribbble"><a href="<?php echo of_get_option('social_dribbble'); ?>">Dribbble</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_vimeo')): ?>
									<li class="ico-vimeo"><a href="<?php echo of_get_option('social_vimeo'); ?>">Vimeo</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_youtube')): ?>
									<li class="ico-youtube"><a href="<?php echo of_get_option('social_youtube'); ?>">YouTube</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_pinterest')): ?>
									<li class="ico-pinterest"><a href="<?php echo of_get_option('social_pinterest'); ?>">Pinterest</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_instagram')): ?>
									<li class="ico-instagram"><a href="<?php echo of_get_option('social_instagram'); ?>">Instagram</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_linkedin')): ?>
									<li class="ico-linkedin"><a href="<?php echo of_get_option('social_linkedin'); ?>">LinkeIn</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_behance')): ?>
									<li class="ico-behance"><a href="<?php echo of_get_option('social_behance'); ?>">Behance</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_skype')): ?>
									<li class="ico-skype"><a href="skype:<?php echo of_get_option('social_skype'); ?>?call">Skype</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_tumblr')): ?>
									<li class="ico-tumblr"><a href="<?php echo of_get_option('social_tumblr'); ?>">Tumblr</a>
									</li>
									<?php endif; ?>
									<?php if(of_get_option('social_rss')): ?>
									<li class="ico-rss"><a href="<?php echo of_get_option('social_rss'); ?>">RSS</a>
									</li>
									<?php endif; ?>
								</ul>
								<!-- /Social Links -->
							</div>
						</div>
					</div>
					<!-- /Top Header -->

					<!-- Main Header -->
					<div class="header-main">
						<div class="container clearfix">
							<div class="grid_12 hr-bottom">
								
								<!-- Logo -->
								<div id="logo">
									<!-- Text based Logo -->
									<?php if(of_get_option('logo_type') == 'text_logo'){ ?>
										<?php if( is_front_page() || is_home() || is_404() ) { ?>
										<h1><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a></h1>
										<?php } else { ?>
										<h2><a href="<?php echo home_url(); ?>" title="<?php bloginfo('description'); ?>"><?php bloginfo('name'); ?></a></h2>
										<?php } ?>
										<p class="tagline"><?php bloginfo('description'); ?></p>
									<!-- /Text based Logo -->
									
									<!-- Image based Logo -->
									<?php } else { ?>
										<a href="<?php echo home_url(); ?>"><img src="<?php echo of_get_option('logo_url', "" ); ?>" alt="<?php bloginfo('name'); ?>" title="<?php bloginfo('description'); ?>"></a>
									<?php }?>
									<!-- /Image based Logo -->
								</div>
								<!-- /Logo -->
								
								
								<!-- Navigation -->
								<nav class="primary" role="navigation">
									<?php emotion_nav(); ?>
								</nav>
								<!-- /Navigation -->
								
							</div>
						</div>
					</div>
					<!-- /Main Header -->
				
				</header>
				<!-- END HEADER -->

				<?php if(!is_search() && !is_404()) { // search and 404 pages excluded to avoid errors
					$title = get_post_meta(get_the_ID(), 'emotion_page_title', true);
					$slider = get_post_meta(get_the_ID(), 'emotion_page_slider', true);

					// Page Heading
					if($title != "Hide") {
						get_template_part('title');
					}

					// Slider
					if($slider == "Show") {
						get_template_part('slider');
					}

				} elseif(is_search() || is_404()) {
					get_template_part('title');
				} ?>