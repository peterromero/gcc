<?php get_header(); ?>
<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container clearfix">

		<?php
		// Checking for WooCommerce. If true, load WooCommerce custom layout
		if (class_exists('woocommerce') && ((is_woocommerce() == "true") || (is_checkout() == "true") || (is_cart() == "true") || (is_account_page() == "true") )){ ?>
		
		
		<!-- Content -->
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
				<div id="page-content">
				  	<?php the_content(); ?>							
				</div><!--#pageContent -->
			</article><!--#post-# .post-->
		  	<?php endwhile; ?>
		
		</div>
		<!-- /Content -->
		
		<!-- Sidebar -->
		<aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php 
			if ( (is_cart() == "true") || (is_checkout() == "true") ) {
				dynamic_sidebar("woocommerce-cart-sidebar");
			} else {
				dynamic_sidebar("woocommerce-sidebar");
			}
			?>
		</aside>
		<!-- /Sidebar -->
		
		
		<?php // ELSE load default layout
	  	} else { ?>

		<!-- Content -->
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			
			<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
			<article id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
				<div id="page-content">
				  	<?php the_content(); ?>							
				</div><!--#pageContent -->
			</article><!--#post-# .post-->
		  	<?php endwhile; ?>
		
		</div>
		<!-- /Content -->
		
		<!-- Sidebar -->
		<aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php if(function_exists('generated_dynamic_sidebar')) { 
				generated_dynamic_sidebar();
			} else {
				get_sidebar();
			}?>
		</aside>
		<!-- /Sidebar -->
		
		<?php // END Default layout
	 	} ?>
	
	</div>
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>