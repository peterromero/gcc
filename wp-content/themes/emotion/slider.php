<?php 
	// query
	$args = array(
		'post_type'        => 'slides',
		'posts_per_page'   => -1,
		'post_status'      => 'publish',
		'orderby'          => 'menu_order',
		'order'            => 'ASC',
		'suppress_filters' => 0
	);
	$slides = get_posts($args);
	if (empty($slides)) return;
?>

<div id="slider" class="slider loading">
	<div class="container clearfix">
		<div class="grid_12">

			<!-- FlexSlider -->
			<div class="flexslider">
				<ul class="slides">

					<?php 
						foreach( $slides as $k => $slide ) {
						// Unset not translated posts
						if ( function_exists( 'wpml_get_language_information' ) ) {
							global $sitepress;
							$check = wpml_get_language_information( $slide->ID );
							$language_code = substr( $check['locale'], 0, 2 );
							if ( $language_code != $sitepress->get_current_language() ) unset( $slides[$k] );

							// Post ID is different in a second language Solution
							if ( function_exists( 'icl_object_id' ) ) $slide = get_post( icl_object_id( $slide->ID, 'slides', true ) );
							}

							$url = get_post_meta($slide->ID, 'emotion_slides_url', true);
							$txt1 = get_post_meta($slide->ID, 'emotion_slides_txt1', true);
							$txt2 = get_post_meta($slide->ID, 'emotion_slides_txt2', true);
							$sl_image_url = wp_get_attachment_image_src( get_post_thumbnail_id($slide->ID), 'slide'); ?>

							<li>
								<!-- Featured Thumbnail -->
								<?php if(has_post_thumbnail($slide->ID)) { ?>
									<?php if($url) { ?>
										<a href="<?php echo $url; ?>"><img src="<?php echo $sl_image_url[0] ?>" alt="" /></a>
									<?php } else { ?>
										<img src="<?php echo $sl_image_url[0] ?>" alt="" />
									<?php } ?>
								<?php } ?>
								<!-- /Featured Thumbnail -->
								
								<div class="flex-caption">

									<?php if($txt1 || $txt2) { ?>
									<h2><?php echo $txt1; ?> <strong><?php echo $txt2; ?></strong></h2>
									<?php } ?>

								</div>
							</li>

					<?php } ?>
				</ul>
			</div>

			<script type="text/javascript">
				jQuery(window).load(function(){
					jQuery('.flexslider').flexslider({
						animation: "<?php echo of_get_option('flex_effect'); ?>",
						controlNav: <?php echo of_get_option('flex_controls', 'true'); ?>,
						startAt: <?php echo of_get_option('flex_startat', '0'); ?>,
						slideshow: <?php echo of_get_option('flex_autoplay', 'true'); ?>,
						slideshowSpeed: <?php echo of_get_option('flex_slideshowspeed', '7000'); ?>,
						directionNav: <?php echo of_get_option('flex_directionnav', 'true'); ?>,
						// Callback API
						before: function(){},
						after: function(){},
						start: function(slider){
							jQuery('#slider').removeClass('loading');
					  	}
					});
				});
			</script>
			<!-- /FlexSlider -->
			
		</div>
	</div>
</div>

<?php wp_reset_postdata(); ?>