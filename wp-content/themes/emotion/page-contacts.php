<?php /* Template Name: Contacts */ ?>

<?php get_header(); ?>

<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container">
		
		<?php if(of_get_option('gmap_show', 'yes') == 'yes'): ?>
		<div class="clearfix">
			<div class="grid_12">
				<!-- BEGIN GOOGLE MAP -->
				<script type="text/javascript">
					jQuery(document).ready(function() {
						//getter
						var zoom= jQuery('#map_canvas').gmap('option', 'zoom');
						
						jQuery('#map_canvas').gmap().bind('init', function(ev, map) {
							jQuery('#map_canvas').gmap('addMarker', {'position': '<?php echo of_get_option('gmap_coord', '57.669645,11.926832'); ?>', 'bounds': true});
							jQuery('#map_canvas').gmap('option', 'zoom', <?php echo of_get_option('gmap_zoom', '14'); ?>);
						});
					});
				</script><!-- Google Map Init-->
			
				<div class="map-wrapper">
					<div id="map_canvas"></div>
				</div>
				<!-- END GOOGLE MAP -->
			</div>
		</div>
		<?php endif; ?>
							
		<div class="clearfix">
			<div class="grid_8">
				<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
				<div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
					<div id="page-content">
					  	<?php the_content(); ?>							
					</div>
				</div><!-- .post-->
			  	<?php endwhile; ?>
			</div>
			<aside class="sidebar grid_4">
				<!-- Contact Widget -->
				<div class="contact-widget widget widget__sidebar">
					<?php if(of_get_option('contact_title')): ?>
					<h3 class="widget-title"><?php echo of_get_option('contact_title'); ?></h3>
					<?php endif; ?>
					<?php if(of_get_option('contact_txt')): ?>
					<?php echo of_get_option('contact_txt'); ?>
					<?php endif; ?>
					<address>
						<?php if(of_get_option('contact_address')): ?>
						<i class="icon-home"></i> <strong><?php echo of_get_option('contact_address'); ?></strong><br>
						<?php endif; ?>

						<?php if(of_get_option('contact_phone')): ?>
						<i class="icon-phone"></i> <?php echo of_get_option('contact_phone'); ?><br>
						<?php endif; ?>

						<?php if(of_get_option('contact_mail')): ?>
						<i class="icon-envelope-alt"></i> <a href="mailto:<?php echo of_get_option('contact_mail'); ?>"><?php echo of_get_option('contact_mail'); ?></a><br>
						<?php endif; ?>

						<?php if(of_get_option('contact_site')): ?>
						<i class="icon-globe"></i> <a href="http://<?php echo of_get_option('contact_site'); ?>"><?php echo of_get_option('contact_site'); ?></a>
						<?php endif; ?>
					</address>
				</div>
				<!-- /Contact Widget -->
			</aside>
		</div>
	</div>

</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>