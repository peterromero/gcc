<?php get_header(); ?>

<?php
	// get the meta information and display if supplied
	$position = get_post_meta($post->ID, 'emotion_team_role', true);

	// get the social links information and display if supplied
	$team_twitter = get_post_meta($post->ID, 'emotion_team_twitter', true);
	$team_facebook = get_post_meta($post->ID, 'emotion_team_facebook', true);
	$team_gplus = get_post_meta($post->ID, 'emotion_team_gplus', true);
	$team_dribbble = get_post_meta($post->ID, 'emotion_team_dribbble', true);
	$team_linkedin = get_post_meta($post->ID, 'emotion_team_linkedin', true);
?>

	<div class="container clearfix">
		<!-- Content -->
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php while (have_posts()) : the_post(); ?>
			
			<!-- Single Employee -->
			<article id="post-<?php the_ID(); ?>" <?php post_class('clearfix item-team item-team__single'); ?>>

				<div class="clearfix">
					<div class="grid_3 alpha">
						<!-- Post Thumbnail -->
						<?php if ( has_post_thumbnail()) : // Check if Thumbnail exists ?>
							<figure class="team-img">
								<?php the_post_thumbnail('medium'); // Fullsize image for the single post ?>
							</figure>
						<?php endif; ?>
						<!-- /Post Thumbnail -->
						<!-- Post Heading -->
						<hgroup>
							<h4><?php the_title(); ?></h4>
							<h5><?php echo $position; ?></h5>
						</hgroup>
						<!-- /Post Heading -->
						<!-- Social Links -->
						<ul class="social-links unstyled">

							<?php if($team_twitter) { ?>
							<li class="ico-twitter">
								<a href="<?php echo $team_twitter; ?>"><?php _e('Twitter', 'emotion') ?></a>
							</li>
							<?php } ?>

							<?php if($team_facebook) { ?>
							<li class="ico-facebook">
								<a href="<?php echo $team_facebook; ?>"><?php _e('Facebook', 'emotion') ?></a>
							</li>
							<?php } ?>
							
							<?php if($team_gplus) { ?>
							<li class="ico-googleplus">
								<a href="<?php echo $team_gplus; ?>"><?php _e('Google+', 'emotion') ?></a>
							</li>
							<?php } ?>
							
							<?php if($team_dribbble) { ?>
							<li class="ico-dribbble">
								<a href="<?php echo $team_dribbble; ?>"><?php _e('Dribbble', 'emotion') ?></a>
							</li>
							<?php } ?>

							<?php if($team_linkedin) { ?>
							<li class="ico-linkedin">
								<a href="<?php echo $team_linkedin; ?>"><?php _e('Linkedin', 'emotion') ?></a>
							</li>
							<?php } ?>
							
						</ul>
						<!-- /Social Links -->
					</div>
					<!-- Post Content -->
					<div class="grid_5 omega">
						<?php the_content(''); ?>
					</div>
					<!-- /Post Content -->
				</div>
				
			</article>
			<!-- /Single Employee -->
			
		<?php endwhile; ?>
		</div>
		<!-- Content / End -->
		<!-- Sidebar -->
		<aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php if(function_exists('generated_dynamic_sidebar')) { 
				generated_dynamic_sidebar();
			} else {
				get_sidebar();
			}?>
		</aside>
		<!-- /Sidebar -->
	</div>

<?php get_footer(); ?>