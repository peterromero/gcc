<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container">
		<div class="clearfix">
			<div class="grid_12">
				<div id="content">

					<?php
					//Theme Options vars
					$filter = of_get_option('portfolio_filter', 'true');
					$count = of_get_option('portfolio_items', '12');
					$title = of_get_option('portfolio_title', 'true');
					$lightbox = of_get_option('portfolio_lightbox', '1');
					?>

					<?php if($filter == 1) { ?>
					<!-- Project Feed Filter -->
					<div class="filter-wrapper clearfix">
						<ul class="project-feed-filter">
						<li><a href="#" class="current" data-filter="*">All</a></li>
						<?php 
						$portfolio_categories = get_categories(array('taxonomy'=>'portfolio_category'));
						foreach($portfolio_categories as $portfolio_category)
							echo '<li><a href="#" data-filter=".'.$portfolio_category->slug.'">' . $portfolio_category->name . '</a></li>';
						?>
						</ul>
					</div>
					<!-- Project Feed Filter / End -->
					<?php } ?>

					<?php
					$grid = "";
					$imgsize = "";
					if ( is_page_template('portfolio-3cols.php') ) {
						$grid = "grid_4";
						$imgsize = "folio-medium";
					} elseif (is_page_template('portfolio-2cols.php') ) {
						$grid = "grid_6";
						$imgsize = "folio-large";
					} else {
						$grid = "grid_3";
						$imgsize = "folio-small";
					}
					?>
					
					<?php
					// Category 
					$values = get_post_custom_values("category"); $cat=$values[0];
				  	$catinclude = 'portfolio_category='. $cat; ?>

					<?php $temp = $wp_query;
					$wp_query= null;
					$wp_query = new WP_Query(); ?>
					<?php $wp_query->query("post_type=portfolio&".$catinclude."&paged=".$paged.'&showposts='.$count."&suppress_filters=0"); ?>
					<?php if ( ! have_posts() ) : ?>
					<div class="post error404 not-found">
						<h2><?php _e( 'Not Found', 'emotion' ); ?></h2>
						<div>
							<p><?php _e( 'Apologies, but no results were found for the requested archive. Perhaps searching will help find a related post.', 'emotion' ); ?></p>
							<?php get_search_form(); ?>
						</div><!-- .entry-content -->
					</div><!-- #post-0 -->
					<?php endif; ?>


					<!-- Projects Feed -->
					<div class="project-feed">
						<?php while ( have_posts() ) : the_post(); 

						$portfolio_terms = wp_get_object_terms($post->ID, 'portfolio_category');
						$portfolio_class = "";
						$portfolio_sort = "";
						$portfolio_type = "";

						if(!empty($portfolio_terms)) {
							$portfolio_class = "folioItem " . $portfolio_terms[0]->slug;
							$portfolio_sort = $portfolio_terms[0]->slug . '[1][0]';
							$portfolio_type = $portfolio_terms[0]->slug;
						}
						?>
					   <div class="<?php echo $grid; ?> project-item <?php foreach( $portfolio_terms as $portfolio_class ) { echo $portfolio_class->slug.' ';} ?>">
							<?php
								//Video vars
								$embed = get_post_meta(get_the_ID(), 'emotion_video_embed', true);
								$type = get_post_meta(get_the_ID(), 'emotion_portfolio_type', true);
								
								$thumb = get_post_thumbnail_id();
								$img_url = wp_get_attachment_url( $thumb,'full'); //get img URL
								$image = wp_get_attachment_image_src( get_post_thumbnail_id($post->ID), $imgsize);

								$excerpt = get_the_excerpt();
							?>
							
							<?php if($lightbox != "1") { //Disabled lightbox ?>

								<?php if(has_post_thumbnail()): ?>
								<figure class="project-img">
									<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>">
										<img src="<?php echo $image[0] ?>" alt="" />
									</a>
								</figure>
								<?php endif; ?>
								
							<?php } else { //Enabled lightbox ?>
								
								<?php if($embed != '' && $type == "Video") { ?>
									<?php if(has_post_thumbnail()): ?>
									<figure class="project-img">
										<img src="<?php echo $image[0] ?>" alt="" />
										<div class="overlay"></div>
										<div class="mask">
											<a class="icon-image" 
												href="<?php echo $embed; ?>"
												rel="prettyphoto[gallery]"
												title="<?php the_title_attribute(); ?>">
												<i class="icon-film"></i>
											</a>
										</div>
									</figure>
									<?php endif; ?>
								<?php } else { ?>
									<?php if(has_post_thumbnail()): ?>
									<figure class="project-img">
										<img src="<?php echo $image[0] ?>" alt="" />
										<div class="overlay"></div>
										<div class="mask">
											<a class="icon-image" 
												href="<?php echo $img_url ?>"
												rel="prettyphoto[gallery]"
												title="<?php the_title_attribute(); ?>">
												<i class="icon-search"></i>
											</a>
										</div>
									</figure>
									<?php endif; ?>
								<?php } ?>
								
							<?php } ?>
							
							
							<?php if($title == "true") { ?>
							<div class="project-desc">
								<h3><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
								<div class="project-desc-txt">
									<?php echo emotion_string_limit_words($excerpt,15); ?>
								</div>
							</div>
							<?php } ?>
					   </div>
					 <?php endwhile; ?>
					</div>
					<!-- Projects Feed / End -->
				</div>
			</div>
		</div>

		<div class="hr hr-dashed"></div>
			
		<!-- Pagination -->
		<div class="clearfix">
			<div class="grid_12">
				<?php emotion_pagination(); ?>
			</div>
		</div>
		<!-- /Pagination -->

		<?php $wp_query = null; $wp_query = $temp;?>
	</div><!-- /#content -->
	
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>