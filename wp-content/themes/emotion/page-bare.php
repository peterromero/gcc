<?php // Template Name: Bare ?>
<?php get_header(); ?>
<!-- Content -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>
		<div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
			<?php the_content(); ?>							
		</div><!--// Page -->
	  	<?php endwhile; ?>
	</div>
</div>
<!-- /Content -->

<?php get_footer(); ?>