<?php
	$blog_post_excerpt = of_get_option('blog_post_excerpt');
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry entry__standard'); ?>>
	
	<?php if(has_post_thumbnail()) { ?>
	<!-- begin post image -->
	<figure class="featured-thumb"><a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('large'); ?></a></figure>
	<!-- end post image -->
	<?php } ?>

	<!-- begin post heading -->
	<header class="entry-header clearfix">
		<div class="format-icon">
			<i class="icon-file-alt"></i>
		</div>
		<div class="entry-header-inner">
			<?php if(!is_singular()) : ?>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php _e('Permalink to:', 'emotion');?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<?php else :?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php endif; ?>

			<?php get_template_part('post-formats/post-meta'); ?>
			
		</div>
	</header>
	<!-- end post heading -->
	
	<?php if(!is_singular()) : ?>
	
		<!-- begin post content -->
		<div class="entry-content">
			<?php if($blog_post_excerpt == "1") {
				the_excerpt();
			} else {
				the_content('');
			}?>
		</div>
		<!-- end post content -->

		<?php if($blog_post_excerpt == "1") { ?>
			<!-- begin post footer -->
			<footer class="entry-footer">
				<a href="<?php the_permalink() ?>" class="btn"><?php _e('Read more', 'emotion'); ?></a>
			</footer>
			<!-- end post footer -->
		<?php }?>
	
	<?php else :?>
	
		<!-- begin post content -->
		<div class="entry-content">
			<?php the_content(''); ?>
		</div>
		<!-- end post content -->
	
	<?php endif; ?>
 
</article>