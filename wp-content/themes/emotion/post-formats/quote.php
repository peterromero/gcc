<?php
	$blog_post_excerpt = of_get_option('blog_post_excerpt');
	$quote =  get_post_meta(get_the_ID(), 'emotion_quote', true);
	$author =  get_post_meta(get_the_ID(), 'emotion_author_quote', true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry entry__quote'); ?>>

	<!-- begin post heading -->
	<header class="entry-header">
		<div class="format-icon">
			<i class="icon-quote-left"></i>
		</div>
	</header>
	<!-- end post heading -->
	
	<!-- begin post content -->
	<div class="entry-content">
		<blockquote>
		<?php echo $quote; ?>
		<?php if($author) {
			echo '<cite>&mdash; ' . $author . '</cite>';
		} ?>
		</blockquote>
		
		<?php if(is_singular()) : ?>
		<?php the_content(''); ?>
		<?php endif; ?>
	</div>
	<!-- end post content -->
 
</article>