<?php
	$blog_post_excerpt = of_get_option('blog_post_excerpt');
	$url =  get_post_meta(get_the_ID(), 'emotion_link_url', true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry entry__link'); ?>>

	<!-- begin post heading -->
	<header class="entry-header clearfix">
		<div class="format-icon">
			<i class="icon-link"></i>
		</div>
		<div class="entry-header-inner">
			<?php if(!is_singular()) : ?>
			<h2 class="entry-title">
				<a target="_blank" href="<?php echo $url; ?>" title="<?php _e('Permalink to:', 'emotion');?> <?php echo $url; ?>"><span><?php the_title(); ?></span></a>
			</h2>
			<small><?php echo $url; ?></small>
			<?php else :?>
			<h1 class="entry-title">
				<a target="_blank" href="<?php echo $url; ?>" title="<?php _e('Permalink to:', 'emotion');?> <?php echo $url; ?>"><span><?php the_title(); ?></span></a>
			</h1>
			<small><?php echo $url; ?></small>
			<?php endif; ?>
		</div>
	</header>
	<!-- end post heading -->
	
	<!-- begin post content -->
	<div class="entry-content">
		<?php the_content(''); ?>
	</div>
	<!-- end post content -->
 
</article>