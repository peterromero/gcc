<?php
	$blog_post_excerpt = of_get_option('blog_post_excerpt');
	$embed = get_post_meta(get_the_ID(), 'emotion_video_embed', true);
?>

<article id="post-<?php the_ID(); ?>" <?php post_class('entry entry__video'); ?>>
	
	<!-- begin video -->
	<figure class="video-holder">
		<?php if ($embed) {
			echo stripslashes(htmlspecialchars_decode($embed));
		}?>
	</figure>
	<!-- end video -->

	<!-- begin post heading -->
	<header class="entry-header clearfix">
		<div class="format-icon">
			<i class="icon-film"></i>
		</div>
		<div class="entry-header-inner">
			<?php if(!is_singular()) : ?>
			<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php _e('Permalink to:', 'emotion');?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>
			<?php else :?>
			<h1 class="entry-title"><?php the_title(); ?></h1>
			<?php endif; ?>

			<?php get_template_part('post-formats/post-meta'); ?>
			
		</div>
	</header>
	<!-- end post heading -->
	
	<?php if(!is_singular()) : ?>
	
		<!-- begin post content -->
		<div class="entry-content">
			<?php if($blog_post_excerpt == "1") {
				the_excerpt();
			} else {
				the_content('');
			}?>
		</div>
		<!-- end post content -->

		<?php if($blog_post_excerpt == "1") { ?>
			<!-- begin post footer -->
			<footer class="entry-footer">
				<a href="<?php the_permalink() ?>" class="btn"><?php _e('Read more', 'emotion'); ?></a>
			</footer>
			<!-- end post footer -->
		<?php }?>
	
	<?php else :?>
	
		<!-- begin post content -->
		<div class="entry-content">
			<?php the_content(''); ?>
		</div>
		<!-- end post content -->
	
	<?php endif; ?>
 
</article>