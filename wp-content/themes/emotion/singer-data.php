<?php
/*
Template Name: Singer Data
*/
get_header();
?>
<!-- Content -->
<div id="content-wrapper" class="content-wrapper">
  <div class="container">
    <div id="content" class="grid_12">
      <?php if (have_posts()) {
        while (have_posts()) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
            <?php
            $gcc_roster_data = get_gcc_roster_data();

            $content = apply_filters('the_content', get_the_content());
            $content = str_replace(']]>', ']]&gt;', $content);
            $content = str_replace(
              array(
                '[peak_roster]',
                '[summit_roster]',
                '[all_roster]',
                '[valley_roster]',
                '[peak_count]',
                '[summit_count]',
                '[all_count]',
                '[valley_count]'
              ),
              array(
                $gcc_roster_data['peak']['list'],
                $gcc_roster_data['summit']['list'],
                $gcc_roster_data['all']['list'],
                $gcc_roster_data['valley']['list'],
                $gcc_roster_data['peak']['count'],
                $gcc_roster_data['summit']['count'],
                $gcc_roster_data['all']['count'],
                $gcc_roster_data['valley']['count']
              ),
              $content
            );
            echo($content);
            ?>
          </div><!--// Page -->
        <?php endwhile;
      } ?>
    </div>
  </div>
</div>
<!-- /Content -->
<?php get_footer(); ?>
