<?php
/*
Template Name: Repertoire Data
*/
get_header();
?>
<!-- Content -->
<div id="content-wrapper" class="content-wrapper">
  <div class="container">
    <div id="content" class="grid_12">
      <?php if (have_posts()) {
        while (have_posts()) : the_post(); ?>
          <div id="post-<?php the_ID(); ?>" <?php post_class('page'); ?>>
            <?php

            $gcc_repertoire_data = get_gcc_repertoire_data();

            $content = get_the_content();
            $content = str_replace(']]>', ']]&gt;', $content);
            $content = str_replace(
              array(
                '[repertoire]'
              ),
              array(
                $gcc_repertoire_data
              ),
              $content
            );

            // This comes here to process the shortcodes we've inserted into
            // $gcc_repertoire_data.
            $content = apply_filters('the_content', $content);
            echo($content);
            ?>
          </div><!--// Page -->
        <?php endwhile;
      } ?>
    </div>
  </div>
</div>
<!-- /Content -->
<?php get_footer(); ?>
