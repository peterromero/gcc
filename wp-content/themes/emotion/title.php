<div id="page-title" class="page-title">
	<div class="container clearfix">
		<div class="grid_12">
			<div class="page-title-holder clearfix">
				
				<?php 
				// check for WooCommerce. If true, load WooCommerce custom layout
				if (class_exists('woocommerce') && ((is_woocommerce() == "true") || (is_checkout() == "true") || (is_cart() == "true") || (is_account_page() == "true") )){ ?>
				
				<h1>
					<?php if ( is_search() ) : ?>
						<?php printf( __( 'Search Results: &ldquo;%s&rdquo;', 'woocommerce' ), get_search_query() ); ?>
					<?php elseif ( is_tax() ) : ?>
						<?php echo single_term_title( "", false ); ?>
					<?php elseif ( is_shop() ) : ?>
						<?php
							$shop_page = get_post( woocommerce_get_page_id( 'shop' ) );

							echo apply_filters( 'the_title', ( $shop_page_title = get_option( 'woocommerce_shop_page_title' ) ) ? $shop_page_title : $shop_page->post_title );
						?>
					<?php else : ?>
						<?php the_title(); ?>
					<?php endif; ?>
					<?php if ( get_query_var( 'paged' ) ) : ?>
						<?php printf( __( '&nbsp;&ndash; Page %s', 'woocommerce' ), get_query_var( 'paged' ) ); ?>
					<?php endif; ?>
				</h1>
				
				<?php // Standard Heading
				} else { ?>
				
					<?php if(is_home()){ ?>
						<h1><?php echo of_get_option('blog_text'); ?></h1>
					<?php } elseif(is_search()) { ?>
						<h1><?php echo sprintf( __( '%s Search Results for ', 'emotion' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
					
					<?php } elseif ( is_author() ) { ?>
						<?php 
							global $author;
							$userdata = get_userdata($author);
						?>
							<h1><?php echo $userdata->display_name; ?></h1>
							
					<?php } elseif ( is_404() ) { ?>
						<h1><?php printf( __( 'Page not found', 'emotion' )); ?></h1>
						
					<?php } elseif ( is_page() ) { ?>
						<h1><?php the_title(); ?></h1>
					
					<?php } elseif ( is_category() ) { ?>
						<h1><?php printf( __( 'Category Archives', 'emotion' )); ?></h1>
						
					<?php } elseif ( is_tax('portfolio_category') ) { ?>
						<h1><?php $terms_as_text = get_the_term_list( $post->ID, 'portfolio_category', '', ', ', '' ) ;
						echo strip_tags($terms_as_text); ?></h1>
					
					<?php } elseif ( is_day() ) { ?>
						<h1><?php printf( __( 'Daily Archives', 'emotion' )); ?></h1>
						
					<?php } elseif ( is_month() ) { ?>	
						<h1><?php printf( __( 'Monthly Archives', 'emotion' )); ?></h1>
						
					<?php } elseif ( is_year() ) { ?>	
						<h1><?php printf( __( 'Yearly Archives', 'emotion' )); ?></h1>
							
					<?php } elseif ( is_tag() ) { ?>
						<h1><?php printf( __( 'Tag Archives', 'emotion' )); ?></h1>
					
					<?php } else { ?>
						<h1><?php the_title(); ?></h1>
					<?php } ?>
				
				<?php } ?>
				
			</div>
		</div>
	</div>
</div>