			</div>
		</div>
		<!-- Inner Wrapper / End-->
		<!-- BEGIN FOOTER -->
		<footer id="footer" class="footer" role="contentinfo">

			<div class="footer-holder">
				<div class="footer-frame">
					
					<?php if(of_get_option('footer_widgets', 'yes') == 'yes'): ?>
					<!-- Footer Widgets -->
					<div class="footer-widgets">
						<div class="container clearfix">
							
							<div class="grid_5">
								<?php
								if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer1')): 
								endif;
								?>
							</div>
							<div class="grid_3">
								<?php
								if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer2')): 
								endif;
								?>
							</div>
							<div class="grid_4">
								<?php
								if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('footer3')): 
								endif;
								?>
							</div>
							
						</div>
					</div>
					<!-- /Footer Widgets -->
					<?php endif; ?>

					<!-- Copyright -->
					<div class="copyright">
						<div class="container clearfix">
							<div class="grid_12">
								<div class="clearfix">
									<div class="copyright-primary">
										<?php
											$copyright = of_get_option('copyright', '&copy; 2013 Blitz | All rights reserved');
											$copyright = str_replace('[YYYY]', date('Y'), $copyright);
											echo($copyright);
										?>
									</div>
								</div>
							</div>
						</div>
					</div>
					<!-- /Copyright -->

				</div>
			</div>
			
		</footer>
		<!-- END FOOTER -->
	
	</div>
	<!-- END WRAPPER -->

	<?php wp_footer(); ?>

	<?php if(of_get_option('ga_code')) { ?>
	<script type="text/javascript">
		<?php echo stripslashes(of_get_option('ga_code')); ?>
	</script>
	<!-- Google Analytics Code-->	
	<?php } ?>
	
</body>
</html>