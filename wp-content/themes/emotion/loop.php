<?php if (have_posts()): while (have_posts()) : the_post(); 
	// The following determines what the post format is and shows the correct file accordingly
	$format = get_post_format();
	get_template_part( 'post-formats/'.$format );					
	if($format == '')
	get_template_part( 'post-formats/standard' );		
	
endwhile; else: ?>

	<!-- Article -->
	<article class="post">
		<h2><?php _e( 'Sorry, nothing to display.', 'emotion' ); ?></h2>
	</article>
	<!-- /Article -->

<?php endif; ?>