<?php get_header(); ?>
	
<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container clearfix">

		<!-- Content -->
		<div id="content" class="grid_8 <?php echo of_get_option('blog_sidebar', 'right'); ?>">


			<?php if (have_posts()): while (have_posts()) : the_post(); ?>
			
			<article id="post-<?php the_ID(); ?>" <?php post_class('entry'); ?>>

				<!-- begin post heading -->
				<header class="entry-header entry-header__small clearfix">
					<div class="entry-header-inner">
						<h2 class="entry-title"><a href="<?php the_permalink(); ?>" title="<?php _e('Permalink to:', 'emotion');?> <?php the_title(); ?>"><?php the_title(); ?></a></h2>

						<?php get_template_part('post-formats/post-meta'); ?>
						
					</div>
				</header>
				<!-- end post heading -->
			 
			</article>

		<?php endwhile; else: ?>

			<!-- Article -->
			<article class="post">
				<h2><?php _e( 'Sorry, nothing to display.', 'emotion' ); ?></h2>
			</article>
			<!-- /Article -->

		<?php endif; ?>
			
			<!-- Pagination -->
			<?php emotion_pagination(); ?>
			<!-- /Pagination -->

		</div>
		<!--/Content -->

		<!-- Sidebar -->
		<aside id="sidebar" class="grid_4 <?php echo of_get_option('blog_sidebar', 'right'); ?>">
			<?php get_sidebar(); ?>
		</aside>
		<!-- /Sidebar -->

	</div>
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>