<?php get_header(); ?>

<!-- BEGIN CONTENT WRAPPER -->
<div id="content-wrapper" class="content-wrapper">
	<div class="container clearfix">

		<div class="grid_12">
			<h1 class="error-404"><?php _e( '404', 'emotion' ); ?></h1>
			<div class="error-404-desc center">
				<h3>
					<a href="<?php echo home_url(); ?>"><?php _e( 'Return home?', 'emotion' ); ?></a>
				</h3>
				<?php echo '<p>' . __('Please try using our search box below to look for information on the our site.', 'emotion') . '</p>'; ?>
	      	<?php get_search_form(); /* outputs the default Wordpress search form */ ?>
			</div>
		</div>
	
	</div>
</div>
<!-- END CONTENT WRAPPER -->

<?php get_footer(); ?>