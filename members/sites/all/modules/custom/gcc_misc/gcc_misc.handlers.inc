<?php

class gcc_misc_registration extends views_handler_field {
  function render($values) {
    return '$50';
  }

  function query() {
  }
}

class gcc_misc_tuition extends views_handler_field {
  function render($values) {
    $ensembles = cache_get('ensemble_names');

    $ensemble_tid = $values->_field_data['nid']['entity']->field_ensemble['und'][0]['tid'];
    $amount = '$0';
    switch ($ensembles->data[$ensemble_tid]) {
      case 'Summit Singers':
      case 'Peak Performers':
        $amount = '$490';
        break;
      case 'Valley Voices':
        $amount = '$150';
        break;
    }
    return $amount;
  }

  function query() {
  }
}

class gcc_misc_uniform extends views_handler_field {
  function render($values) {
    $ensembles = cache_get('ensemble_names');

    $ensemble_tid = $values->_field_data['nid']['entity']->field_ensemble['und'][0]['tid'];
    $amount = '$0';
    switch ($ensembles->data[$ensemble_tid]) {
      case 'Summit Singers':
      case 'Peak Performers':
        $amount = '$50';
        break;
      case 'Valley Voices':
        $amount = '$0';
        break;
    }
    return $amount;
  }

  function query() {
  }
}

class gcc_misc_total extends views_handler_field {
  function render($values) {
    $ensembles = cache_get('ensemble_names');

    $ensemble_tid = $values->_field_data['nid']['entity']->field_ensemble['und'][0]['tid'];
    $amount = 0;
    switch ($ensembles->data[$ensemble_tid]) {
      case 'Summit Singers':
      case 'Peak Performers':
        $amount = 590;
        break;
      case 'Valley Voices':
        $amount = 200;
        break;
    }

    $amount -= $values->_field_data['nid']['entity']->field_grant_amount['und'][0]['value'];

    return "\${$amount}";
  }

  function query() {
  }
}

class gcc_misc_monthly extends views_handler_field {
  function render($values) {
    $ensembles = cache_get('ensemble_names');

    $ensemble_tid = $values->_field_data['nid']['entity']->field_ensemble['und'][0]['tid'];
    $amount = 0;
    switch ($ensembles->data[$ensemble_tid]) {
      case 'Summit Singers':
      case 'Peak Performers':
        $amount = 540;
        $num_payments = 5;
        break;
      case 'Valley Voices':
        $amount = 150;
        $num_payments = 2;
        break;
    }

    $amount -= $values->_field_data['nid']['entity']->field_grant_amount['und'][0]['value'];
    $amount /= $num_payments;

    return "\${$amount}";
  }

  function query() {
  }
}
