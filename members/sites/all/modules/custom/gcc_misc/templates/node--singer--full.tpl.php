<div id="singer-info">
  <h1>Registration Form</h1>
  <p class="note"><em>You may print this page for your records.</em></p>
  <p class="note">Please complete the following information. We must have day phone numbers for the parents and/or guardians of each child. If you are a single-parent household or do not want a spouse listed in any Chorale information report, please put N/A in the appropriate blank.
    <strong>Do not leave any section blank and please print legibly.</strong>
  </p>
  <div class="field half">
    <label>Singer Name</label>
    <input type="text" value="<?php
    $first_name = isset($field_first_name[0]['safe_value']) ? $field_first_name[0]['safe_value'] : '';
    $last_name  = isset($field_last_name[0]['safe_value']) ? $field_last_name[0]['safe_value'] : '';
    echo "$first_name $last_name"; ?> " disabled>
  </div>
  <div class="field half">
    <label>Singer Cell</label>
    <input type="text"
           value="<?php if (isset($field_cell_phone[0]['safe_value'])) {
             echo $field_cell_phone[0]['safe_value'];
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Mailing Address</label>
    <input type="text" value="<?php
    $mailing_address = isset($field_mailing_address[0]['safe_value']) ? $field_mailing_address[0]['safe_value'] : '';
    $city            = isset($field_city[0]["taxonomy_term"]->name) ? $field_city[0]["taxonomy_term"]->name : '';
    $zip             = isset($field_zip_code[0]["safe_value"]) ? $field_zip_code[0]["safe_value"] : '';
    echo "$mailing_address, $city CO $zip";
    ?> " disabled>
  </div>
  <div class="field half">
    <label>School for <?php echo _gcc_current_school_year_string(); ?></label>
    <input type="text"
           value="<?php if (isset($field_school[0]['taxonomy_term']->name)) {
             echo $field_school[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field half">
    <label>Grade for <?php echo _gcc_current_school_year_string(); ?></label>
    <input type="text"
           value="<?php if (isset($field_k_start_year[0]['value'])) {
             echo _gcc_format_grade_string(_gcc_get_grade_int_from_k_start_year($field_k_start_year[0]['value']));
           } ?> "
           disabled>
  </div>
  <div class="field third">
    <label>Birthdate</label>
    <input type="text" value="<?php if (isset($field_birthdate[0]['value'])) {
      echo date('F j, Y', strtotime($field_birthdate[0]['value']));
    } ?> " disabled>
  </div>
  <div class="field third">
    <label>Sex</label>
    <input type="text"
           value="<?php if (isset($field_sex[0]['taxonomy_term']->name)) {
             echo $field_sex[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field third">
    <label>Shirt Size</label>
    <input type="text"
           value="<?php if (isset($field_t_shirt_size[0]['taxonomy_term']->name)) {
             echo $field_t_shirt_size[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Medical Concerns</label>
    <input type="text"
           value="<?php if (isset($field_medical_concerns[0]['safe_value'])) {
             echo $field_medical_concerns[0]['safe_value'];
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Allergies</label>
    <input type="text"
           value="<?php if (isset($field_allergies[0]['safe_value'])) {
             echo $field_allergies[0]['safe_value'];
           } ?> "
           disabled>
  </div>
  <div class="field non-flex photo-permission">
    <input type="checkbox"
           value="" <?php if (isset($field_photo_permission[0]['value']) && $field_photo_permission[0]['value']) {
      echo 'checked';
    } ?>>
    <label>Permission to Photograph</label>
    <p>By submitting this form, I/we, being the parent(s)/legal guardian(s) of this child, do hereby give the Greeley Children’s Chorale, Inc. permission to allow my/our child to be photographed during the <?php echo _gcc_current_school_year_string(); ?> choir season. I/We understand that such photographs, as well as my/our child’s identity, may be published both in print and on the choir’s website, and I/we waive any rights under applicable state or federal law to object to such publication. I/We further understand that I/we may revoke this permission and waiver at any time by providing written notice to the Greeley Children’s Chorale, Inc. I understand that by unchecking this box, my child will not be permitted to participate in group photos or performance opportunities where photos are taken.</p>
  </div>
  <h2>Parent 1</h2>
  <div class="field half">
    <label>Relationship</label>
    <input type="text"
           value="<?php if (isset($field_parent_1_relationship[0]['taxonomy_term']->name)) {
             echo $field_parent_1_relationship[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field half">
    <label>Name</label>
    <input type="text" value="<?php
    $parent_1_first_name = isset($field_parent_1_first_name[0]['safe_value']) ? $field_parent_1_first_name[0]['safe_value'] : '';
    $parent_1_last_name  = isset($field_parent_1_last_name[0]['safe_value']) ? $field_parent_1_last_name[0]['safe_value'] : '';
    echo "$parent_1_first_name $parent_1_last_name"; ?> " disabled>
  </div>
  <div class="field half">
    <label>Phone</label>
    <input type="text" value="<?php
    if (isset($field_parent_1_phone[0])) {
      $phone_str_1 = implode(', ', array_map(function ($phone) {
        return $phone['safe_value'];
      }, $field_parent_1_phone));
      echo $phone_str_1;
    }
    ?>" disabled>
  </div>
  <div class="field half">
    <label>Email</label>
    <input type="text" value="<?php
    if (isset($field_parent_1_email[0])) {
      $email_str_1 = implode(', ', array_map(function ($email) {
        return $email['email'];
      }, $field_parent_1_email));
      echo $email_str_1;
    }
    ?>" disabled>
  </div>
  <h2>Parent 2</h2>
  <div class="field half">
    <label>Relationship</label>
    <input type="text"
           value="<?php if (isset($field_parent_2_relationship[0]['taxonomy_term']->name)) {
             echo $field_parent_2_relationship[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field half">
    <label>Name</label>
    <input type="text" value="<?php
    $parent_2_first_name = isset($field_parent_2_first_name[0]['safe_value']) ? $field_parent_2_first_name[0]['safe_value'] : '';
    $parent_2_last_name  = isset($field_parent_2_last_name[0]['safe_value']) ? $field_parent_2_last_name[0]['safe_value'] : '';
    echo "$parent_2_first_name $parent_2_last_name"; ?> " disabled>
  </div>
  <div class="field half">
    <label>Phone</label>
    <input type="text" value="<?php
    if (isset($field_parent_2_phone[0])) {
      $phone_str_2 = implode(', ', array_map(function ($phone) {
        return $phone['safe_value'];
      }, $field_parent_2_phone));
      echo $phone_str_2;
    }
    ?>" disabled>
  </div>
  <div class="field half">
    <label>Email</label>
    <input type="text" value="<?php
    if (isset($field_parent_2_email[0])) {
      $email_str_2 = implode(', ', array_map(function ($email) {
        return $email['email'];
      }, $field_parent_2_email));
      echo $email_str_2;
    }
    ?>" disabled>
  </div>
  <h2>Committee Obligation</h2>
  <div class="field">
    <label>Which parent will serve on a committee?</label>
    <input type="text"
           value="<?php if (isset($field_committee_parent[0]['value'])) {
             echo $field_committee_parent[0]['value'];
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>First choice of committee</label>
    <input type="text"
           value="<?php if (isset($field_committee_choice_1[0]['taxonomy_term']->name)) {
             echo $field_committee_choice_1[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Second choice of committee</label>
    <input type="text"
           value="<?php if (isset($field_committee_choice_2[0]['taxonomy_term']->name)) {
             echo $field_committee_choice_2[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Would you consider being a committee chair?</label>
    <input type="radio"
           name="committee_chair"
           value="1 " <?php if (isset($field_committee_chair[0]['taxonomy_term']->name) && $field_committee_chair[0]['taxonomy_term']->name == 'Yes') {
      echo 'checked';
    } ?>>
    Yes
    <input type="radio"
           name="committee_chair"
           value="0 " <?php if (isset($field_committee_chair[0]['taxonomy_term']->name) && $field_committee_chair[0]['taxonomy_term']->name == 'Maybe') {
      echo 'checked';
    } ?>>
    Maybe
    <input type="radio"
           name="committee_chair"
           value="-1 " <?php if (isset($field_committee_chair[0]['taxonomy_term']->name) && $field_committee_chair[0]['taxonomy_term']->name == 'No') {
      echo 'checked';
    } ?>>
    No
  </div>
  <div class="field">
    <label>Would you consider serving on the board?</label>
    <input type="radio"
           name="board"
           value="1 " <?php if (isset($field_board[0]['taxonomy_term']->name) && $field_board[0]['taxonomy_term']->name == 'Yes') {
      echo 'checked';
    } ?>>
    Yes
    <input type="radio"
           name="board"
           value="0 " <?php if (isset($field_board[0]['taxonomy_term']->name) && $field_board[0]['taxonomy_term']->name == 'Maybe') {
      echo 'checked';
    } ?>>
    Maybe
    <input type="radio"
           name="board"
           value="-1 " <?php if (isset($field_board[0]['taxonomy_term']->name) && $field_board[0]['taxonomy_term']->name == 'No') {
      echo 'checked';
    } ?>>
    No
  </div>
  <h2>Emergency Contact</h2>
  <div class="field half">
    <label>Name</label>
    <input type="text"
           value="<?php
           $emergency_contact_first_name = isset($field_emergency_contact_first_na[0]['safe_value']) ? $field_emergency_contact_first_na[0]['safe_value'] : '';
           $emergency_contact_last_name  = isset($field_emergency_contact_last_nam[0]['safe_value']) ? $field_emergency_contact_last_nam[0]['safe_value'] : '';
           echo "$emergency_contact_first_name $emergency_contact_last_name"; ?> "
           disabled>
  </div>
  <div class="field half">
    <label>Relationship to singer</label>
    <input type="text"
           value="<?php if (isset($field_emergency_contact_relation[0]['taxonomy_term']->name)) {
             echo $field_emergency_contact_relation[0]['taxonomy_term']->name;
           } ?> "
           disabled>
  </div>
  <div class="field">
    <label>Phone</label>
    <input type="text" value="<?php
    if (isset($field_emergency_contact_phone[0])) {
      $phone_emergency = implode(', ', array_map(function ($phone) {
        return $phone['safe_value'];
      }, $field_emergency_contact_phone));
      echo $phone_emergency;
    }
    ?>" disabled>
  </div>
</div>
