(function ($) {
  Drupal.behaviors.singerEditAlert = {
    attach: function (context, settings) {
      alert("Thanks for submitting your information! We have received your data. If this is a new singer record, it will not show up until it has been reviewed by a GCC administration. There is no need to resubmit this information. Changes to existing singers will also not be visible until approved.");
    }
  };
})(jQuery);
