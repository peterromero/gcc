(function($) {
  $(function() {
    // Handle rollouts.
    $('.rollout').click(function(e) {
      e.stopPropagation();
      $(this).toggleClass('open');
    });
  });
})(jQuery);
