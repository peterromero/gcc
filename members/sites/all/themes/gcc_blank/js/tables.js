(function($) {
  $(function() {
    // Handle page opacity while page is loading.
    $('#loading-shade').removeClass('loading');
    $('.views-submit-button').click(function() {
      $('#loading-shade').addClass('loading');
    });

    // Check row checkboxes when clicking on rows.
    $('td').click(function() {
      var $tr = $(this).parents('tr');
      if ($tr.find('.vbo-select').prop('checked') == true) {
        // Do this the reverse of what seems right because this function happens
        // before the checkbox's state changes.
        $tr.removeClass('row-selected');
      }
      else {
        $tr.addClass('row-selected');
      }
    });
  });
})(jQuery);
