<div id="page-wrapper">
  <div id="page">
    <div id="main-wrapper">
      <div id="main" class="clearfix">
        <div id="content" class="column">
          <div class="section">
            <?php print render($page['content']); ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
