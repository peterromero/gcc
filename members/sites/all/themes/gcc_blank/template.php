<?php

function gcc_blank_preprocess_html(&$variables) {
  // Hide the toolbar.
  unset($variables['page']['page_top']['toolbar']);
  unset($variables['page']['page_bottom']['admin_menu']);
}

function gcc_blank_preprocess_page(&$variables) {
  $path = current_path();

  // Add custom stylesheets to certain paths.
  if (strpos($path, 'reports/name-tags') === 0) {
    drupal_add_css(drupal_get_path('theme', 'gcc_blank') . '/stylesheets/css/nametags.css', array(
      'weight' => 10,
      'group' => CSS_THEME
    ));
  }
  if ($path == 'reports/mailing-labels' || $path == 'reports/mailing-labels-parent') {
    drupal_add_css(drupal_get_path('theme', 'gcc_blank') . '/stylesheets/css/labels.css', array(
      'weight' => 10,
      'group' => CSS_THEME
    ));
  }
  if (preg_match('/node\/\d+/', $path)) {
    drupal_add_css(drupal_get_path('theme', 'gcc_blank') . '/stylesheets/css/singer-info.css', array(
      'weight' => 10,
      'group' => CSS_THEME
    ));
  }
}

function gcc_blank_preprocess_views_view_list(&$variables) {
  if ($variables['view']->name == 'name_tags') {
    foreach ($variables['classes_array'] as $index => &$row_classes) {
      $color_class = '';
      $last_name = $variables['view']->result[$index]->field_data_field_last_name_field_last_name_value;
      if ($last_name) {
        $first_letter = $last_name[0];
        if ($first_letter >= 'A' && $first_letter <= 'C') {
          $color_class = ' red';
        }
        if ($first_letter >= 'D' && $first_letter <= 'H') {
          $color_class = ' orange';
        }
        if ($first_letter >= 'I' && $first_letter <= 'M') {
          $color_class = ' green';
        }
        if ($first_letter >= 'N' && $first_letter <= 'R') {
          $color_class = ' blue';
        }
        if ($first_letter >= 'S' && $first_letter <= 'Z') {
          $color_class = ' violet';
        }
      }
      $row_classes .= $color_class;
    }
  }
}
