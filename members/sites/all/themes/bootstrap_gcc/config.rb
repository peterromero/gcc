preferred_syntax = :sass
http_path = '/members/'
css_dir = 'stylesheets/css/'
sass_dir = 'stylesheets/sass/'
images_dir = 'stylesheets/images/'
relative_assets = true
line_comments = true
output_style = :compact
