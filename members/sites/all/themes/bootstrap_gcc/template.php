<?php

function bootstrap_gcc_preprocess_node(&$variables) {
  if ($variables['view_mode'] == 'teaser') {
    drupal_add_js(drupal_get_path('theme', 'bootstrap_gcc') . '/js/singer-tiles.js');

    // Remove "Read More" link.
    unset($variables['content']['links']['node']['#links']['node-readmore']);

    // Remove contextual links.
    unset($variables['title_suffix']['contextual_links']);

    // Change submission info to last edited date.
    $variables['submitted'] = 'Last edited on ' . date('F j, Y \a\t g:i a', $variables['changed']);

    switch ($variables['view']->name) {
      case 'updates_to_approve':
        // Add a link to view diff.
        $variables['content']['links']['node']['#links']['moderation-diff'] = [
          'attributes' => [
            'rel' => 'tag',
            'class' => 'js-diff-link',
            'title' => 'View Changeset'
          ],
          'href' => 'node/' . $variables['nid'] . '/moderation/diff',
          'html' => TRUE,
          'title' => 'View changeset diff'
        ];

        // Make title link to edit page instead of node view page.
        $variables['node_url'] .= '/moderation/diff';
        break;
      case 'your_singers':
        // Add a class to differentiate inactive singers.
        $current_member_items = field_get_items('node', $variables['node'], 'field_current_member');
        $is_current_member    = $current_member_items[0]['value'];
        if (!$is_current_member) {
          $variables['classes_array'][] = 'node-singer-inactive';
        }

        // Add a class to differentiate genders.
        $sex_items = field_get_items('node', $variables['node'], 'field_sex');
        $sex       = $sex_items[0]['tid'];
        if ($sex == SEX_FEMALE) {
          $variables['classes_array'][] = 'node-singer-girl';
        }

        // Make title link to edit page instead of node view page.
        $variables['node_url'] .= '/edit';
        break;
    }
  }
}

function bootstrap_gcc_preprocess_html(&$variables) {
  global $user;
  if (in_array('administrator', $user->roles)) {
    $variables['classes_array'][] = 'admin';
  }
  else {
    if (in_array('subadministrator', $user->roles)) {
      $variables['classes_array'][] = 'subadmin';
    }
    else {
      $variables['classes_array'][] = 'not-admin';
    }
  }
}

function bootstrap_gcc_preprocess_page(&$variables) {
  // Show a popup after singers have been edited.
  if (isset($_SESSION['singer_updated'])) {
    drupal_add_js(drupal_get_path('module', 'gcc_misc') . '/js/edit_alert.js');
    unset($_SESSION['singer_updated']);
  }

  if (!empty($variables['page']['content']['system_main']['#form_id']) && $variables['page']['content']['system_main']['#form_id'] == 'singer_node_form') {
    drupal_add_js(drupal_get_path('theme', 'bootstrap_gcc') . '/js/singer-edit.js');

    $school_year                                 = _gcc_current_school_year_string();
    $variables['page']['content']['grade_modal'] = array(
      '#type' => 'markup',
      '#markup' => <<<HTML
        <div id="grade_modal">
          <div class="message">
            <p>Please enter or confirm the singer's grade during the <strong>{$school_year} school year</strong>.</p>
            <button class="btn btn-success">Got it</button>
          </div>
        </div>
HTML
    );
  }
}
