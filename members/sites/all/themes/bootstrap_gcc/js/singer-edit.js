(function ($) {
  $(function () {
    $('#edit-field-k-start-year-und-0-value').focus(function () {
      if (!$(this).hasClass('popup-shown')) {
        $(this).addClass('popup-shown');
        $('#grade_modal').fadeIn(200);
      }
    });

    $('#grade_modal').click(function() {
      $(this).fadeOut(200, function() {
        $('#edit-field-k-start-year-und-0-value').focus();
      });
    });
  });
})(jQuery);
