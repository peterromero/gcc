(function ($) {
  $(function () {
    // Register clicks on singer blocks instead of just the links inside them.
    $('.view-your-singers .views-row, #block-block-1').click(function (e) {
      e.stopPropagation();
      var href = $(this).find('.js-edit-link').attr('href');
      if (!href) {
        href = $(this).find('.js-add-link').attr('href')
      }
      if (href) {
        window.location.href = href;
      }
    });
    $('.view-updates-to-approve .views-row').click(function (e) {
      e.stopPropagation();
      var href = $(this).find('.js-diff-link').attr('href');
      if (href) {
        window.location.href = href;
      }
    });
  });
})(jQuery);
