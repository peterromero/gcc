<?php

require(__DIR__ . '/wp-config.php');

$db = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME);

$start_date = ($_GET['start']) ? $_GET['start'] : date('Ymd');
$end_date = ($_GET['end']) ? $_GET['end'] : date('Ymd');

$start_date_is_valid = preg_match('/^\d{8}$/', $start_date);
$end_date_is_valid = preg_match('/^\d{8}$/', $end_date);

if (!$start_date_is_valid || !$end_date_is_valid) {
  die('Start/end dates must be in the format YYYYMMDD. Try copying this querystring for an example: ?start=20170901&end=20170930');
}

$query = <<<SQL
SELECT
  count(ID)        AS count,
  lower(trim(meta_value)) AS name
FROM wp_posts
  LEFT JOIN wp_postmeta ON post_id = ID
WHERE post_type = 'nf_sub'
      AND meta_key = '_field_1'
      AND post_date > '$start_date'
      AND post_date < '$end_date'
GROUP BY name
ORDER BY name;
SQL;

$rows = mysqli_query($db, $query);

print <<<HTML
<table>
<thead>
<tr>
<td>Singer</td>
<td>Logins</td>
</tr>
</thead>
<tbody>
HTML;

while ($row = $rows->fetch_assoc()) {
  $name = ucwords($row['name']);
  print "<tr><td>$name</td><td>{$row['count']}</td></tr>";
}

print <<<HTML
</tbody>
</table>
HTML;
