	function closeAllMenuPanels() {
		$('.menu_panel').hide();
		$('.menu_bar_li > a').removeClass('active');
	}
	function closeAllNav() {
		$('.menu_bar_wrapper').hide();
		$('.nav_bar_li > a').removeClass('active');
		closeAllMenuPanels();
	}
	function activateNavBarItem(linkClicked) {
		$('.nav_bar_li > a').removeClass('active');
		linkClicked.addClass('active');
		$('.menu_bar_wrapper').removeClass('active');
		closeAllMenuPanels();
		var selectedMenuBar = linkClicked.parent().find('.menu_bar_wrapper');
		selectedMenuBar.addClass('active');
		slideMenuBarLeft($('.menu_bar_wrapper').filter(function() {
			if ($(this).hasClass('active')) {
				return false;
			} else {
				return true;
			}
		}),200);
		slideMenuBarRight(selectedMenuBar, 200);
	}
	function slideMenuBarRight(mbw, d) {
		mbw.css('left', '-720px');
		mbw.show();
		mbw.animate({'left': '120px'}, d);
	}
	function slideMenuBarLeft(mbw, d) {
		mbw.animate({'left': '-720px'}, d, function() {mbw.css('display','none')});
	}
	function activateMenuItem(linkClicked) {
		$('.menu_bar_li > a').removeClass('active');
		linkClicked.addClass('active');
		var selectedMenuPanel = linkClicked.parent().find('.menu_panel');
		openMenuPanel(selectedMenuPanel);
	}
	function openMenuPanel(selectedMenuPanel) {
		$('.menu_panel').removeClass('active');
		selectedMenuPanel.addClass('active');
		$('.menu_panel').filter(function() {
			if ($(this).hasClass('active')) {
				return false;
			} else {
				return true;
			}
		}).slideUp(200);
		selectedMenuPanel.slideDown(200);
	}
	function loadData() {
		$('body').css('cursor', 'wait');
		$.post(
			'ajax/loadDataDisplay.php',
			function(data) {
				$('#dataArea').html(data);
				attachDisplayTableBehaviors();
				$('body').css('cursor', 'auto');
			}
		);
	}
	function attachDisplayTableBehaviors() {
		$('.rowCheckbox').click(function() {
			if (anyRowCheckboxesChecked()) {
				$('#editLinks').fadeIn(200);
			} else {
				$('#editLinks').fadeOut(200);
			}
		});
	}
	function anyRowCheckboxesChecked() {
		var cx = false;
		$('.rowCheckbox').each(function() {
			if ($(this).attr('checked')) {
				cx = true;
			}
		});
		return cx;
	}
	function clearView() {
		$('#dataTable').children().removeAttr('selected');
		$('#outputFormat').children().removeAttr('selected');
		$('#nameDisplay').children().removeAttr('selected');
		$('.fieldSelectorCheckbox').removeAttr('checked');
	}
	function loadView(id) {
		clearView();
		$.post(
			'ajax/view.php',
			{
				'function': 'load',
				'id': id
			},
			function(data) {
				$('#dataTable').children('[value=' + data.table + ']').attr('selected', 'selected');
				$('#outputFormat').children('[value=' + data.outputFormat + ']').attr('selected', 'selected');
				$('#nameDisplay').children('[value=' + data.nameDisplay + ']').attr('selected', 'selected');
				$('#filterQuery').val(data.filterQuery);
				$('#maxResults').val(data.maxResults);
				$('#numPartitions').val(data.numPartitions);
				if (data.multi_showEdit==1) {$('#multi_showEdit').attr('checked', 'checked')};
				if (data.multi_describe==1) {$('#multi_describe').attr('checked', 'checked')};
				var includeFields = $.parseJSON(data.includeFields);
				for (var i in includeFields) {
					$('.fieldSelectorCheckbox[name=' + includeFields[i].name + ']').attr('checked', 'checked');
				}
				loadData();
			},
			'json'
		);
	}
	function saveView(onCompleteFunction) {
		var fieldSelector = $('#fieldSelector').serializeArray();
		var multi_showEdit = ($('#multi_showEdit').attr('checked'))?1:0;
		var multi_describe = ($('#multi_describe').attr('checked'))?1:0;
		$.post(
			'ajax/view.php',
			{
				'function': 'update',
				'table': $('#dataTable').val(),
				'includeFields': fieldSelector,
				'outputFormat': $('#outputFormat').val(),
				'nameDisplay': $('#nameDisplay').val(),
				'rowSortFieldName': $('#rowSortFieldName').val(),
				'filterQuery': $('#filterQuery').val(),
				'maxResults': $('#maxResults').val(),
				'numPartitions': $('#numPartitions').val(),
				'multi_showEdit': multi_showEdit,
				'multi_describe': multi_describe
			},
			onCompleteFunction
		);
	}
	function setSortField(fieldName) {
		$('#rowSortFieldName').val(fieldName);
		$('.tableHeaderSortLink').removeClass('active');
		$('.tableHeaderSortLink').each(function() {
			if ($(this).attr('rel')==fieldName) {
				$(this).addClass('active');
			}
		});
		saveView(function() {loadData();});
	}
	function getEmailLinks() {
		var recordIds = $('.rowCheckbox').serializeArray();
		$.post(
			'ajax/emailLinks.php',
			{
				'recordIds': recordIds
			},
			function(data) {
				window.scroll(0,0);
				$('#editFormArea').empty();
				$('#shader').show();
				$('#editFormArea').html(data);
			}
		);
	}
	function loadReport(fileName) {
		var recordIds = $('.rowCheckbox').serialize();
		$('#reportFileName').val(fileName);
		$('#reportRecordIds').val(recordIds);
		$('#reportDataTable').val($('#dataTable').val());
		$('#reportForm').submit();
		closeAllNav();
	}
	function getModForm(method, tableName, altId) { //Usually this function uses the recordIds derived from which checkboxes are checked. The altId, if passed, overrules these values. This is only functional for the 'edit' method.
		switch (method) {
			case 'add': {
				$.post(
					'ajax/dataMod.php',
					{
						'method': 'addEdit',
						'tableName': tableName
					},
					function(data) {
						window.scroll(0,0);
						$('#editFormArea').empty();
						$('#shader').show();
						$('#editFormArea').html(data);
						$('div.fieldWrapper').children().eq(0).focus();
						citron_fksField_attachBehaviors();
					}
				);
				break;
			}
			case 'edit': {
				if (altId) {
					var recordIds = {
						0: {
							'value': altId
						}
					}
				} else {
					var recordIds = $('.rowCheckbox').serializeArray();
				}
				$.post(
					'ajax/dataMod.php',
					{
						'method': 'addEdit',
						'tableName': tableName,
						'recordIds': recordIds
					},
					function(data) {
						window.scroll(0,0);
						$('#editFormArea').empty();
						$('#shader').show();
						$('#editFormArea').html(data);
						citron_fksField_attachBehaviors();
					}
				);
				break;
			}
			case 'delete': {
				var yes = confirm('Are you sure you want to PERMANENTLY DELETE these records?');
				if (yes) {
					if (altId) {
						var recordIds = {
							0: {
								'value': altId
							}
						}
					} else {
						var recordIds = $('.rowCheckbox').serializeArray();
					}
					$.post(
						'ajax/dataMod.php',
						{
							'method': 'delete',
							'tableName': tableName,
							'recordIds': recordIds
						},
						function(data) {
							alert(data);
							loadData();
						}
					);
				}
				break;
			}
		}
	}
	$(function() {
		loadView(1);
		$('.ok_button').click(function() {
			closeAllNav();
			if ($(this).hasClass('force_reload')) {
				$('#filterQuery').val('');
				$('.fieldSelectorCheckbox').removeAttr('checked');
				saveView(function() {window.location.reload();});
			} else {
				saveView(function() {loadData();});
			}
		});
		$('.nav_bar_li > a').click(function() {
			if ($(this).hasClass('active')) {
				closeAllNav();
			} else {
				activateNavBarItem($(this));
			}
			return false;
		});
		$('.menu_bar_li > a').click(function() {
			if ($(this).hasClass('active')) {
				closeAllMenuPanels();
			} else {
				activateMenuItem($(this));
			}
			return false;
		});
	});