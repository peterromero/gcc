<?php
	if ($dataTableName == 'gcc_people' && count($recordIds)>0) {
	} else {
		$recordIds = array();
		$q = "
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people
			WHERE gcc_people.isStaff = '2')
			UNION
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people, gcc_committee_memberships
			WHERE gcc_people.id = gcc_committee_memberships.member
			AND gcc_committee_memberships.committee = '7'
			AND gcc_committee_memberships.type = '1')
			UNION
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people
			WHERE gcc_people.isCurrentSinger = '2')
			ORDER BY isCurrentSinger DESC, isStaff, nameLast, nameFirst
		";
		$allPeople = mysql_query($q);
		while ($person = mysql_fetch_array($allPeople)) {
			$recordIds[] = $person['id'];
		}
	}
	echo("
		<style type=\"text/css\">
			table {
				width: 100%;
			}
			.evenColumn {
				width: 60pt;
			}
			.remainderColumn {
				width: auto;
			}
		</style>
	");
	echo("
		<table border=\"1\">
			<thead>
				<tr>
					<th colspan=\"2\">Singer</th>
					<th>Qty.</th>
					<th>Unit Price</th>
					<th>Amount<br />Due</th>
					<th colspan=\"3\">Amt. Pd.<br />by Check</th>
					<th>Amt. Pd.<br />by PFA</th>
					<th>Amt. Pd.<br />by Cash</th>
					<th>Balance Due</th>
					<th>Notes</th>
				</tr>
			</thead>
			<tbody>
	");
	$singerObject = new person;
	$row = 2;
	foreach($recordIds as $recordId) {
		$singerObject->loadSingleRecord($recordId);
		echo("
			<tr>
				<td class=\"shrinkwrapColumn noRightBorder right\">" . $singerObject->getFieldDataByCodeName('nameFirst') . "</td>
				<td class=\"shrinkwrapColumn\">" . $singerObject->getFieldDataByCodeName('nameLast') . "</td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\">=IF(C$row*D$row>0;C$row*D$row;\"\")</td>
				<td class=\"shrinkwrapColumn noRightBorder\">Ck.&nbsp;#</td>
				<td class=\"evenColumn noRightBorder\">&nbsp;</td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\">=IF(E$row-H$row-I$row-J$row<>0;E$row-H$row-I$row-J$row;\"\")</td>
				<td class=\"remainderColumn\"></td>
			</tr>
		");
		$row++;
		$firstRow = false;
	}
	echo("
				<tr>
					<td colspan=\"2\">Total</td>
					<td>=SUM(C2:C" . ($row-1) . ")</td>
					<td></td>
					<td>=SUM(E2:E" . ($row-1) . ")</td>
					<td></td>
					<td></td>
					<td>=SUM(H2:H" . ($row-1) . ")</td>
					<td>=SUM(I2:I" . ($row-1) . ")</td>
					<td>=SUM(J2:J" . ($row-1) . ")</td>
					<td>=SUM(K2:K" . ($row-1) . ")</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	");
?>