<?php
	if ($dataTableName == 'gcc_people' && count($recordIds)>0) {
	} else {
		$recordIds = array();
		$allPeople = mysql_query('select `id` from `gcc_people` where `isCurrentSinger`=\'2\' order by `nameLast`, `nameFirst`');
		while ($person = mysql_fetch_array($allPeople)) {
			$recordIds[] = $person['id'];
		}
	}
	echo("<table border=\"1\">");
	echo("
		<tr>
			<th>Singer</th>
			<th colspan=\"4\">Payment 1<br />April 30, 2012</th>
			<th colspan=\"4\">Payment 2<br />August 1, 2012</th>
			<th colspan=\"4\">Payment 3<br />September 10, 2012</th>
			<th colspan=\"4\">Payment 4<br />October 1, 2012</th>
			<th colspan=\"4\">Payment 5<br />November 5, 2012</th>
			<th>Total Payments<br />Received</th>
			<th>Registration</th>
			<th>Tuition</th>
			<th>Tickets</th>
			<th>Uniform</th>
		</tr>
	");
	$singerObject = new person;
	$firstRowInRowset = 2;
	foreach($recordIds as $recordId) {
		$firstRow = true;
		$singerObject->loadSingleRecord($recordId);
		$compositeTuition = $singerObject->getFieldDataByCodeName('compositeTuitionRaw');
		$simpleTuition = $singerObject->getFieldDataByCodeName('simpleTuitionRaw');
		$tickets = $singerObject->getFieldDataByCodeName('ticketsRaw');
		$registration = 50;
		$uniform = 50;
		$tuitionRatio = $simpleTuition/($compositeTuition-$registration);
		$ticketsRatio = $tickets/($compositeTuition-$registration);
		$uniformRatio = $uniform/($compositeTuition-$registration);
		$normalPayment = ($compositeTuition-$registration)/5;
		$firstPayment = $normalPayment+$registration;
		echo("<tr>");
			if ($firstRow) {
				echo("<td rowspan=\"10\">" . $singerObject->getFieldDataByCodeName('nameFull') . "</td>");
			}
			echo("
				<td colspan=\"3\" rowspan=\"3\">Amount due:</td>
				<td rowspan=\"3\">$firstPayment</td>
				<td colspan=\"3\">New amount due:</td>
				<td>$normalPayment</td>
				<td colspan=\"3\">New amount due:</td>
				<td>$normalPayment</td>
				<td colspan=\"3\">New amount due:</td>
				<td>$normalPayment</td>
				<td colspan=\"3\">New amount due:</td>
				<td>$normalPayment</td>
			");
			if ($firstRow) {
				echo("
					<td rowspan=\"10\">=E" . ($firstRowInRowset+3) . "+E" . ($firstRowInRowset+4) . "+E" . ($firstRowInRowset+5) . "+I" . ($firstRowInRowset+3) . "+I" . ($firstRowInRowset+4) . "+I" . ($firstRowInRowset+5) . "+M" . ($firstRowInRowset+3) . "+M" . ($firstRowInRowset+4) . "+M" . ($firstRowInRowset+5) . "+Q" . ($firstRowInRowset+3) . "+Q" . ($firstRowInRowset+4) . "+Q" . ($firstRowInRowset+5) . "+U" . ($firstRowInRowset+3) . "+U" . ($firstRowInRowset+4) . "+U" . ($firstRowInRowset+5) . "</td>
					<td rowspan=\"10\">=IF(V{$firstRowInRowset}>50;50;V{$firstRowInRowset})</td>
					<td rowspan=\"10\">=(V{$firstRowInRowset}-W{$firstRowInRowset})*$tuitionRatio</td>
					<td rowspan=\"10\">=(V{$firstRowInRowset}-W{$firstRowInRowset})*$ticketsRatio</td>
					<td rowspan=\"10\">=(V{$firstRowInRowset}-W{$firstRowInRowset})*$uniformRatio</td>
				");
			}
			echo("
			</tr>
			<tr>
				<td colspan=\"2\">Previous balance:</td>
				<td>+</td>
				<td>=E" . ($firstRowInRowset+6) . "</td>
				<td colspan=\"2\">Previous balance:</td>
				<td>+</td>
				<td>=I" . ($firstRowInRowset+6) . "</td>
				<td colspan=\"2\">Previous balance:</td>
				<td>+</td>
				<td>=M" . ($firstRowInRowset+6) . "</td>
				<td colspan=\"2\">Previous balance:</td>
				<td>+</td>
				<td>=Q" . ($firstRowInRowset+6) . "</td>
			</tr>
			<tr>
				<td colspan=\"2\">TOTAL DUE:</td>
				<td>=</td>
				<td>=IF(I" . ($firstRowInRowset+1) . "=\"\";\"\";I$firstRowInRowset+I" . ($firstRowInRowset+1) . ")</td>
				<td colspan=\"2\">TOTAL DUE:</td>
				<td>=</td>
				<td>=IF(M" . ($firstRowInRowset+1) . "=\"\";\"\";M$firstRowInRowset+M" . ($firstRowInRowset+1) . ")</td>
				<td colspan=\"2\">TOTAL DUE:</td>
				<td>=</td>
				<td>=IF(Q" . ($firstRowInRowset+1) . "=\"\";\"\";Q$firstRowInRowset+Q" . ($firstRowInRowset+1) . ")</td>
				<td colspan=\"2\">TOTAL DUE:</td>
				<td>=</td>
				<td>=IF(U" . ($firstRowInRowset+1) . "=\"\";\"\";U$firstRowInRowset+U" . ($firstRowInRowset+1) . ")</td>
			</tr>
			<tr>
				<td>Check #</td>
				<td>&nbsp;</td>
				<td>-</td>
				<td></td>
				<td>Check #</td>
				<td>&nbsp;</td>
				<td>-</td>
				<td></td>
				<td>Check #</td>
				<td>&nbsp;</td>
				<td>-</td>
				<td></td>
				<td>Check #</td>
				<td>&nbsp;</td>
				<td>-</td>
				<td></td>
				<td>Check #</td>
				<td>&nbsp;</td>
				<td>-</td>
				<td></td>
			</tr>
			<tr>
				<td colspan=\"2\">PFA:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">PFA:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">PFA:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">PFA:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">PFA:</td>
				<td>-</td>
				<td></td>
			</tr>
			<tr>
				<td colspan=\"2\">Cash:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">Cash:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">Cash:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">Cash:</td>
				<td>-</td>
				<td></td>
				<td colspan=\"2\">Cash:</td>
				<td>-</td>
				<td></td>
			</tr>
			<tr>
				<td colspan=\"2\">Balance:</td>
				<td>=</td>
				<td>=IF(AND(E" . ($firstRowInRowset+3) . "=\"\";E" . ($firstRowInRowset+4) . "=\"\";E" . ($firstRowInRowset+5) . "=\"\");\"\";E$firstRowInRowset-E" . ($firstRowInRowset+3) . "-E" . ($firstRowInRowset+4) . "-E" . ($firstRowInRowset+5) . ")</td>
				<td colspan=\"2\">Balance:</td>
				<td>=</td>
				<td>=IF(AND(I" . ($firstRowInRowset+3) . "=\"\";I" . ($firstRowInRowset+4) . "=\"\";I" . ($firstRowInRowset+5) . "=\"\");\"\";I" . ($firstRowInRowset+2) . "-I" . ($firstRowInRowset+3) . "-I" . ($firstRowInRowset+4) . "-I" . ($firstRowInRowset+5) . ")</td>
				<td colspan=\"2\">Balance:</td>
				<td>=</td>
				<td>=IF(AND(M" . ($firstRowInRowset+3) . "=\"\";M" . ($firstRowInRowset+4) . "=\"\";M" . ($firstRowInRowset+5) . "=\"\");\"\";M" . ($firstRowInRowset+2) . "-M" . ($firstRowInRowset+3) . "-M" . ($firstRowInRowset+4) . "-M" . ($firstRowInRowset+5) . ")</td>
				<td colspan=\"2\">Balance:</td>
				<td>=</td>
				<td>=IF(AND(Q" . ($firstRowInRowset+3) . "=\"\";Q" . ($firstRowInRowset+4) . "=\"\";Q" . ($firstRowInRowset+5) . "=\"\");\"\";Q" . ($firstRowInRowset+2) . "-Q" . ($firstRowInRowset+3) . "-Q" . ($firstRowInRowset+4) . "-Q" . ($firstRowInRowset+5) . ")</td>
				<td colspan=\"2\">Balance:</td>
				<td>=</td>
				<td>=IF(AND(U" . ($firstRowInRowset+3) . "=\"\";U" . ($firstRowInRowset+4) . "=\"\";U" . ($firstRowInRowset+5) . "=\"\");\"\";U" . ($firstRowInRowset+2) . "-U" . ($firstRowInRowset+3) . "-U" . ($firstRowInRowset+4) . "-U" . ($firstRowInRowset+5) . ")</td>
			</tr>
			<tr>
				<td colspan=\"4\">Notes:</td>
				<td colspan=\"4\">Notes:</td>
				<td colspan=\"4\">Notes:</td>
				<td colspan=\"4\">Notes:</td>
				<td colspan=\"4\">Notes:</td>
			</tr>
			<tr>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
			</tr>
			<tr>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
				<td colspan=\"4\">&nbsp;</td>
			</tr>
		");
		$firstRow = false;
		$firstRowInRowset += 10;
	}
	echo("
		<tr>
			<td colspan=\"21\">Total</td>
			<td>=SUM(V2:V" . ($firstRowInRowset-1) . ")</td>
			<td>=SUM(W2:W" . ($firstRowInRowset-1) . ")</td>
			<td>=SUM(X2:X" . ($firstRowInRowset-1) . ")</td>
			<td>=SUM(Y2:Y" . ($firstRowInRowset-1) . ")</td>
			<td>=SUM(Z2:Z" . ($firstRowInRowset-1) . ")</td>
		</tr>
	");
	echo("</table>");
?>