<?php
	{ //CSS
		echo("
			<style type=\"text/css\" media=\"print\">
				body {
					padding: 0;
					font-size: 70%;
				}
				a {
					color: black;
				}
				td.dataField {
					border-bottom: solid 1px black;
				}
				* {
				}
			</style>
			<style type=\"text/css\" media=\"all\">
				h1 {
					font: bold 2.25em/100% georgia, serif;
					text-align: center;
					margin: 0;
				}
				h2 {
					font: bold 1.75em/100% georgia, serif;
					text-align: center;
					margin: 1em 0 0 0;
				}
				* {
					font-family: georgia, serif;
				}
				a {
					text-decoration: none;
				}
				.subtext {
					font: italic 1em/1.2em georgia, serif;
					text-align: center;
				}
				.page-break {
					page-break-after: always;
				}
				td.dataField {
					font-size: 1.333em;
					width: 50%;
				}
				td.dataLabel {
					width: 1px;
					text-align: right;
					padding: .5em .5em 0 .5em;
				}
				td.blankRow {
				}
				table {
					width: 100%;
				}
				td {
					vertical-align: bottom;
				}
			</style>
			<style type=\"text/css\" media=\"screen\">
				body {
					background-color: #e8e8e8;
					color: #606060;
					text-shadow: 0 1px 0 white;
					padding: 100px;
				}
				td.dataField {
					border-bottom: solid 1px #c0c0c0;
				}
				a {
					color: #606060;
				}
			</style>
		");
	}
	$singerObject = new person;
	$relatedPersonObject = new person;
	$singerObject->setBlankFieldChar('&nbsp;');
	$relatedPersonObject->setBlankFieldChar('&nbsp;');
	if (count($recordIds)==0) {
		echo("You must select at least one singer record on the previous page.");
	} else {
		foreach($recordIds as $singerId) {
			$singerObject->loadSingleRecord($singerId);
			echo("
				<h1>
					Singer Information Update
				</h1>
				<p class=\"subtext\">
					Please check the following information and fill in any blanks. We would especially like to have a cell phone number for each singer, and additional email addresses for parents/guardians if available. If there is no information for a certain blank, please write \"N/A.\"
				</p>
				<table>
					<tr>
						<td class=\"dataLabel\">
							Child's&nbsp;Name:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('nameFull') . "
						</td>
						<td class=\"dataLabel\">
							Child's&nbsp;Cell&nbsp;Phone (not&nbsp;parents'):
						</td>
						<td colspan=\"3\" class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('phoneCell') . "
						</td>
					</tr>
						<tr>
							<td class=\"dataLabel\">
								School:
							</td>
							<td class=\"dataField\">
								" . $singerObject->getFieldDataByCodeName('school') . "
							</td>
							<td class=\"dataLabel\">
								Grade&nbsp;for&nbsp" . formatSchoolYear(time()) . " School&nbsp;Year:
							</td>
							<td class=\"dataField\">
								" . $singerObject->getFieldDataByCodeName('grade') . "
							</td>
						</tr>
					<tr>
						<td colspan=\"4\" class=\"blankRow\">
							&nbsp;
						</td>
					</tr>
			");
			$guardians = mysql_query("
				select gcc_people.id, gcc_relationships.type
				from gcc_relationships, gcc_people
				where gcc_relationships.memberLower = $singerId
				and gcc_relationships.guardian = 2
				and gcc_people.id = gcc_relationships.memberUpper
				order by gcc_relationships.type
			");
			$hasMother = false;
			$hasFather = false;
			while ($guardian = mysql_fetch_array($guardians)) {
				$relatedPersonObject->loadSingleRecord($guardian['id']);
				switch($relatedPersonObject->getFieldDataByCodeName('gender')) {
					case 'Female': {
						$relationshipTypeFieldName = 'nameUpperFemale';
						break;
					}
					case 'Male': {
						$relationshipTypeFieldName = 'nameUpperMale';
						break;
					}
					case 'Unknown': {
						$relationshipTypeFieldName = 'nameUpperNa';
						break;
					}
				}
				$relationshipType = mysql_fetch_array(mysql_query("select $relationshipTypeFieldName as name from gcc_relationship_types where id = {$guardian['type']}"));
				if ($relationshipType['name']=='Mother') {$hasMother = true;}
				if ($relationshipType['name']=='Father') {$hasFather = true;}
				echo("
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('nameFull') . "
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneCell') . "
							</td>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneHome') . "
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('email1') . "
							</td>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('email2') . "
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			if (!$hasMother) {
				echo("
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Mother's&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Mother's&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			if (!$hasFather) {
				echo("
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Father's&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Father's&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			echo("</table>");
			echo("<div class=\"page-break\">&nbsp;</div>");
		}
	}
?>