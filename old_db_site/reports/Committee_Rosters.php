<?php
	if ($dataTableName == 'gcc_committees' && count($recordIds)>0) {
	} else {
		$recordIds = array();
		$allCommittees = mysql_query('select `id` from `gcc_committees` order by `name`');
		while ($committee = mysql_fetch_array($allCommittees)) {
			$recordIds[] = $committee['id'];
		}
	}
	$committeeObject = new committee;
	$committeeMembershipObject = new committeeMembership;
	$memberObject = new person;
	$singerObject = new person;
	echo("
		<table border=\"1\">
			<thead>
				<tr>
					<th>Committee Name</th>
					<th>Person Name</th>
					<th>Board<br />Liaison</th>
					<th>Chair</th>
					<th>Membership</th>
					<th>Phone Numbers</th>
					<th>Email Addresses</th>
					<th>Singers</th>
				</tr>
			</thead>
			<tbody>
	");
	foreach($recordIds as $recordId) {
		$firstRow = true;
		$committeeObject->loadSingleRecord($recordId);
		$memberships = mysql_query("
			select gcc_committee_memberships.id, gcc_committee_memberships.member
			from gcc_committee_memberships, gcc_people
			where gcc_committee_memberships.committee = $recordId
			and gcc_people.id = gcc_committee_memberships.member
			order by gcc_committee_memberships.isBoardLiaison desc, gcc_committee_memberships.isChair desc, gcc_committee_memberships.type, gcc_people.nameLast, gcc_people.nameFirst
		");
		while($membership = mysql_fetch_array($memberships)) {
			echo('<tr>');
				if ($firstRow) {
					echo("<td rowspan=\"" . mysql_num_rows($memberships) . "\">");
						echo($committeeObject->getFieldDataByCodeName('name'));
					echo('</td>');
				}
				$memberObject->loadSingleRecord($membership['member']);
				$committeeMembershipObject->loadSingleRecord($membership['id']);
				$isBoardLiaison = ($committeeMembershipObject->getFieldDataByCodeName('isBoardLiaison')=='Yes')?'X':(($committeeMembershipObject->getFieldDataByCodeName('isBoardLiaison')=='Maybe')?'Maybe':'');
				$isChair = ($committeeMembershipObject->getFieldDataByCodeName('isChair')=='Yes')?'X':(($committeeMembershipObject->getFieldDataByCodeName('isChair')=='Maybe')?'Maybe':'');
				echo("<td>" . $memberObject->getFieldDataByCodeName('nameFull') . "</td>");
				echo("<td class=\"center\">$isBoardLiaison</td>");
				echo("<td class=\"center\">$isChair</td>");
				echo("<td>" . $committeeMembershipObject->getFieldDataByCodeName('type') . "</td>");
				$phones = array();
				if (($tmpPhone = $memberObject->getFieldDataByCodeName('phoneCell'))!='-') {
					$phones[] = $tmpPhone;
				}
				if (($tmpPhone = $memberObject->getFieldDataByCodeName('phoneHome'))!='-') {
					$phones[] = $tmpPhone;
				}
				if (($tmpPhone = $memberObject->getFieldDataByCodeName('phoneOffice'))!='-') {
					$phones[] = $tmpPhone;
				}
				echo("<td>" . implode('<br />', $phones) . "</td>");
				$emails = array();
				if (($tmpEmail = $memberObject->getFieldDataByCodeName('email1'))!='-') {
					$emails[] = $tmpEmail;
				}
				if (($tmpEmail = $memberObject->getFieldDataByCodeName('email2'))!='-') {
					$emails[] = $tmpEmail;
				}
				echo("<td>" . implode('<br />', $emails) . "</td>");
				$singers = mysql_query("select memberLower from gcc_relationships where memberUpper = {$membership['member']} and (type = 1 or type = 2)");
				$singerNames = array();
				while($singer = mysql_fetch_array($singers)) {
					$singerObject->loadSingleRecord($singer['memberLower']);
					if ($singerObject->getFieldDataByCodeName('isCurrentSinger')=='Yes') {
						$singerNames[] = $singerObject->getFieldDataByCodeName('nameFull') . " (" . $singerObject->getFieldDataByCodeName('grade') . ")";
					}
				}
				echo("<td>" . implode('<br />', $singerNames) . "</td>");
			echo('</tr>');
			$firstRow = false;
		}
	}
	echo("
			</tbody>
		</table>
	");
?>