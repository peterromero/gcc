<?php
	$q = "
		SELECT
			concat(kids.nameFirst, \" \", kids.nameLast) as kid_name,
			group_concat(concat(adults.nameFirst, \" \", adults.nameLast) separator \"<br />\") as adult_names,
			group_concat(committees.name separator \"<br />\") as committees
		FROM
			gcc_people as kids,
			gcc_relationships as relationships,
			gcc_people as adults,
			gcc_committee_memberships as memberships,
			gcc_committees as committees
		WHERE
			kids.isCurrentSinger = 2
			AND
			relationships.memberLower = kids.id
			AND
			adults.id = relationships.memberUpper
			AND
			memberships.member = adults.id
			AND
			committees.id = memberships.committee
		GROUP BY
			kid_name
		ORDER BY
			kids.nameLast, kids.nameFirst
	";
	$rows = mysql_query($q);
	echo("
		<table border=\"1\">
			<thead>
				<tr>
					<th>Singer</th>
					<th>&nbsp;</th>
					<th>Adult(s)</th>
					<th>Committee Membership(s)</th>
				</tr>
			</thead>
			<tbody>
	");
	while ($row = mysql_fetch_array($rows)) {
		echo("
			<tr>
				<td>{$row['kid_name']}</td>
				<td>&nbsp;</td>
				<td>{$row['adult_names']}</td>
				<td>{$row['committees']}</td>
			</tr>
		");
	}
	echo("
			</tbody>
		</table>
	");
?>