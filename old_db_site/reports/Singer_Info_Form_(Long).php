<?php
	{ //CSS
		echo("
			<style type=\"text/css\" media=\"print\">
				body {
					padding: 0;
					font-size: 70%;
				}
				a {
					color: black;
				}
				td.dataField {
					border-bottom: solid 1px black;
				}
				* {
				}
			</style>
			<style type=\"text/css\" media=\"all\">
				h1 {
					font: bold 2.25em/100% georgia, serif;
					text-align: center;
					margin: 0;
				}
				h2 {
					font: bold 1.75em/100% georgia, serif;
					text-align: center;
					margin: 1em 0 0 0;
				}
				* {
					font-family: georgia, serif;
				}
				a {
					text-decoration: none;
				}
				.subtext {
					font: italic 1em/1.2em georgia, serif;
					text-align: center;
				}
				.page-break {
					page-break-after: always;
				}
				td.dataField {
					font-size: 1.333em;
					width: 50%;
				}
				td.dataField.fullWidth {
					width: 100% !important;
				}
				td.dataLabel {
					width: 1px;
					text-align: right;
					padding: .5em .5em 0 .5em;
				}
				td.blankRow {
				}
				table {
					width: 100%;
				}
				td {
					vertical-align: bottom;
				}
			</style>
			<style type=\"text/css\" media=\"screen\">
				body {
					background-color: #e8e8e8;
					color: #606060;
					text-shadow: 0 1px 0 white;
					padding: 100px;
				}
				td.dataField {
					border-bottom: solid 1px #c0c0c0;
				}
				a {
					color: #606060;
				}
			</style>
		");
	}
	$singerObject = new person;
	$relatedPersonObject = new person;
	$singerObject->setBlankFieldChar('&nbsp;');
	$relatedPersonObject->setBlankFieldChar('&nbsp;');
	if (count($recordIds)==0) {
		echo("
			<h1>
				Singer Information
			</h1>
			<p class=\"subtext\">
				Please complete the following information. We must have day phone numbers for the parents and/or guardians of each child. If you are a single-parent household or do not want a spouse listed in any Chorale information report, please put N/A in the appropriate blank. <b>Do not leave any section blank</b> and <b>please print legibly</b>.
			</p>
			<table>
				<tr>
					<td class=\"dataLabel\">
						Child's&nbsp;Name:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
							Child's&nbsp;Cell&nbsp;Phone (not&nbsp;parents'):
					</td>
					<td colspan=\"3\" class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Child's&nbsp;Mailing&nbsp;Address:
					</td>
					<td colspan=\"3\" class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						School:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Grade&nbsp;for&nbsp" . formatSchoolYear(time()) . " School&nbsp;Year:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Date&nbsp;of&nbsp;Birth:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Gender:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan=\"4\" class=\"blankRow\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Mother's&nbsp;Name:
					</td>
					<td colspan=\"3\" class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Mother's&nbsp;Cell&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Mother's&nbsp;Home&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Mother's&nbsp;Preferred&nbsp;Email:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Mother's&nbsp;Alternate&nbsp;Email:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan=\"4\" class=\"blankRow\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Father's&nbsp;Name:
					</td>
					<td colspan=\"3\" class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Father's&nbsp;Cell&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Father's&nbsp;Home&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Father's&nbsp;Preferred&nbsp;Email:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Father's&nbsp;Alternate&nbsp;Email:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan=\"4\" class=\"blankRow\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Emergency&nbsp;Contact (other&nbsp;than&nbsp;parents):
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Relationship&nbsp;to&nbsp;singer:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Cell&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Home&nbsp;Phone:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td colspan=\"4\" class=\"blankRow\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Special Medical Concerns:
					</td>
					<td class=\"dataField\" colspan=\"3\">
						" . $singerObject->getFieldDataByCodeName('medical') . "
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						Allergies:
					</td>
					<td class=\"dataField\">
						" . $singerObject->getFieldDataByCodeName('allergies') . "
					</td>
					<td class=\"dataLabel\">
						My&nbsp;child&nbsp;has permission&nbsp;to&nbsp;take Tylenol&nbsp;upon&nbsp;request:
					</td>
					<td class=\"dataField\">
						" . $singerObject->getFieldDataByCodeName('tylenol') . "
					</td>
				</tr>
			</table>
			<h2>Permission to Photograph Child</h2>
			<p class=\"subtext\">
				I/We, being the parent(s)/legal guardian(s) of this child, do hereby give the Greeley Children’s Chorale, Inc. permission to allow my/our child to be photographed during the 2011–2012 choir season. I/We understand that such photographs, as well as my/our child’s identity, may be published both in print and on the choir’s website, and I/we waive any rights under applicable state or federal law to object to such publication. I/We further understand that I/we may revoke this permission and waiver at any time by providing written notice to the Greeley Children’s Chorale, Inc.
			</p>
			<table>
				<tr>
					<td class=\"dataLabel\">
						Parent&nbsp;Signature:
					</td>
					<td class=\"dataField fullWidth\">
						&nbsp;
					</td>
				</tr>
			</table>
			<h2>Parent Committees</h2>
			<p class=\"subtext\">
				(please see attached page for committee descriptions)
			</p>
			<table>
				<tr>
					<td class=\"dataLabel\">
						First&nbsp;Choice of&nbsp;Committee:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						Second&nbsp;Choice of&nbsp;Committee:
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
				<tr>
					<td class=\"dataLabel\">
						I&nbsp;am&nbsp;interested&nbsp;in&nbsp;being a&nbsp;committee&nbsp;chairperson (yes/no/maybe):
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
					<td class=\"dataLabel\">
						I&nbsp;am&nbsp;interested&nbsp;in&nbsp;being a&nbsp;board&nbsp;member (yes/no/maybe):
					</td>
					<td class=\"dataField\">
						&nbsp;
					</td>
				</tr>
			</table>
		");
	} else {
		foreach($recordIds as $singerId) {
			$singerObject->loadSingleRecord($singerId);
			echo("
				<h1>
					Singer Information
				</h1>
				<p class=\"subtext\">
					Please check and make changes to the information we have on file. We must have day phone numbers for the parents and/or guardians of each child. If you are a single-parent household or do not want a spouse listed in any Chorale information report, please put N/A in the appropriate blank. <b>Do not leave any section blank</b> and <b>please print legibly</b>.
				</p>
				<table>
					<tr>
						<td class=\"dataLabel\">
							Child's&nbsp;Name:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('nameFull') . "
						</td>
						<td class=\"dataLabel\">
							Child's&nbsp;Cell&nbsp;Phone:
						</td>
						<td colspan=\"3\" class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('phoneCell') . "
						</td>
					</tr>
					<tr>
						<td class=\"dataLabel\">
							Child's&nbsp;Mailing&nbsp;Address:
						</td>
						<td colspan=\"3\" class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('address') . "
						</td>
					</tr>
					<tr>
						<td class=\"dataLabel\">
							School:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('school') . "
						</td>
						<td class=\"dataLabel\">
							Grade&nbsp;for&nbsp" . formatSchoolYear(time()) . " School&nbsp;Year:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('grade') . "
						</td>
					</tr>
					<tr>
						<td class=\"dataLabel\">
							Date&nbsp;of&nbsp;Birth:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('birthdate') . "
						</td>
						<td class=\"dataLabel\">
							Gender:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('gender') . "
						</td>
					</tr>
					<tr>
						<td colspan=\"4\" class=\"blankRow\">
							&nbsp;
						</td>
					</tr>
			");
			$guardians = mysql_query("
				select gcc_people.id, gcc_relationships.type
				from gcc_relationships, gcc_people
				where gcc_relationships.memberLower = $singerId
				and gcc_relationships.guardian = 2
				and gcc_people.id = gcc_relationships.memberUpper
				order by gcc_relationships.type
			");
			$hasMother = false;
			$hasFather = false;
			while ($guardian = mysql_fetch_array($guardians)) {
				$relatedPersonObject->loadSingleRecord($guardian['id']);
				switch($relatedPersonObject->getFieldDataByCodeName('gender')) {
					case 'Female': {
						$relationshipTypeFieldName = 'nameUpperFemale';
						break;
					}
					case 'Male': {
						$relationshipTypeFieldName = 'nameUpperMale';
						break;
					}
					case 'Unknown': {
						$relationshipTypeFieldName = 'nameUpperNa';
						break;
					}
				}
				$relationshipType = mysql_fetch_array(mysql_query("select $relationshipTypeFieldName as name from gcc_relationship_types where id = {$guardian['type']}"));
				if ($relationshipType['name']=='Mother') {$hasMother = true;}
				if ($relationshipType['name']=='Father') {$hasFather = true;}
				echo("
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('nameFull') . "
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneCell') . "
							</td>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneHome') . "
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('email1') . "
							</td>
							<td class=\"dataLabel\">
								{$relationshipType['name']}'s&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('email2') . "
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			if (!$hasMother) {
				echo("
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Mother's&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Mother's&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Mother's&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			if (!$hasFather) {
				echo("
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Name:
							</td>
							<td colspan=\"3\" class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Father's&nbsp;Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Father's&nbsp;Preferred&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Father's&nbsp;Alternate&nbsp;Email:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			$emergencyContacts = mysql_query("
				select gcc_people.id, gcc_relationships.type
				from gcc_relationships, gcc_people
				where gcc_relationships.memberLower = $singerId
				and gcc_relationships.emergency = 2
				and gcc_people.id = gcc_relationships.memberUpper
				order by gcc_relationships.type
			");
			while ($emergencyContact = mysql_fetch_array($emergencyContacts)) {
				$relatedPersonObject->loadSingleRecord($emergencyContact['id']);
				switch($relatedPersonObject->getFieldDataByCodeName('gender')) {
					case 'Female': {
						$relationshipTypeFieldName = 'nameUpperFemale';
						break;
					}
					case 'Male': {
						$relationshipTypeFieldName = 'nameUpperMale';
						break;
					}
					case 'Unknown': {
						$relationshipTypeFieldName = 'nameUpperNa';
						break;
					}
				}
				$relationshipType = mysql_fetch_array(mysql_query("select $relationshipTypeFieldName as name from gcc_relationship_types where id = {$emergencyContact['type']}"));
				echo("
						<tr>
							<td class=\"dataLabel\">
								Emergency&nbsp;Contact (other&nbsp;than&nbsp;parents):
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('nameFull') . "
							</td>
							<td class=\"dataLabel\">
								Relationship&nbsp;to&nbsp;singer:
							</td>
							<td class=\"dataField\">
								{$relationshipType['name']}
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneCell') . "
							</td>
							<td class=\"dataLabel\">
								Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								" . $relatedPersonObject->getFieldDataByCodeName('phoneHome') . "
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			if (mysql_num_rows($emergencyContacts)==0) {
				echo("
						<tr>
							<td class=\"dataLabel\">
								Emergency&nbsp;Contact (other&nbsp;than&nbsp;parents):
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Relationship&nbsp;to&nbsp;singer:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td class=\"dataLabel\">
								Cell&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
							<td class=\"dataLabel\">
								Home&nbsp;Phone:
							</td>
							<td class=\"dataField\">
								&nbsp;
							</td>
						</tr>
						<tr>
							<td colspan=\"4\" class=\"blankRow\">
								&nbsp;
							</td>
						</tr>
				");
			}
			echo("
					<tr>
						<td class=\"dataLabel\">
							Special Medical Concerns:
						</td>
						<td class=\"dataField\" colspan=\"3\">
							" . $singerObject->getFieldDataByCodeName('medical') . "
						</td>
					</tr>
					<tr>
						<td class=\"dataLabel\">
							Allergies:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('allergies') . "
						</td>
						<td class=\"dataLabel\">
							My&nbsp;child&nbsp;has permission&nbsp;to&nbsp;take Tylenol&nbsp;upon&nbsp;request:
						</td>
						<td class=\"dataField\">
							" . $singerObject->getFieldDataByCodeName('tylenol') . "
						</td>
					</tr>
				</table>
				<h2>Permission to Photograph Child</h2>
				<p class=\"subtext\">
					I/We, being the parent(s)/legal guardian(s) of this child, do hereby give the Greeley Children’s Chorale, Inc. permission to allow my/our child to be photographed during the 2011–2012 choir season. I/We understand that such photographs, as well as my/our child’s identity, may be published both in print and on the choir’s website, and I/we waive any rights under applicable state or federal law to object to such publication. I/We further understand that I/we may revoke this permission and waiver at any time by providing written notice to the Greeley Children’s Chorale, Inc.
				</p>
				<table>
					<tr>
						<td class=\"dataLabel\">
							Parent&nbsp;Signature:
						</td>
						<td class=\"dataField fullWidth\">
							&nbsp;
						</td>
					</tr>
				</table>
				<h2>Parent Committees</h2>
				<p class=\"subtext\">
					(please see attached page for committee descriptions)
				</p>
				<table>
					<tr>
						<td class=\"dataLabel\">
							First&nbsp;Choice of&nbsp;Committee:
						</td>
						<td class=\"dataField\">
							&nbsp;
						</td>
						<td class=\"dataLabel\">
							Second&nbsp;Choice of&nbsp;Committee:
						</td>
						<td class=\"dataField\">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td class=\"dataLabel\">
							I&nbsp;am&nbsp;interested&nbsp;in&nbsp;being a&nbsp;committee&nbsp;chairperson (yes/no/maybe):
						</td>
						<td class=\"dataField\">
							&nbsp;
						</td>
						<td class=\"dataLabel\">
							I&nbsp;am&nbsp;interested&nbsp;in&nbsp;being a&nbsp;board&nbsp;member (yes/no/maybe):
						</td>
						<td class=\"dataField\">
							&nbsp;
						</td>
					</tr>
				</table>
			");
			echo("<div class=\"page-break\">&nbsp;</div>");
		}
	}
?>