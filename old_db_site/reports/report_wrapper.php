<?php
	require_once("/home/meesezc/public_html/citron/php/html.php");
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->loadModule('formatting');
	$x->loadModule('sql');
	$x->loadModule('formFields');
	$x->loadModule('lists');
	//Selectively load style sheets.
	if ($_REQUEST['reportFileName']=='Singer_Info_Form_(Long).php'||$_REQUEST['reportFileName']=='Singer_Info_Form_(Short).php') {
	} else {
		$x->importCss('../css/gcc_report_styles.php');
	}
	require_once("../functions/gcc_functions.php");
	require_once("../classes/gcc_classes.php");
	$dataTableName = $_REQUEST['reportDataTable'];
	parse_str($_REQUEST['reportRecordIds'], $recordIds);
	$x->setTitle(str_replace(array('_', '.php'), array(' ', null), $_REQUEST['reportFileName']));
	$x->initialize();
	require_once($_REQUEST['reportFileName']);
	$x->close();
?>