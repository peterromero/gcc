<?php
	if ($dataTableName == 'gcc_people' && count($recordIds)>0) {
	} else {
		$recordIds = array();
		$q = "
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people
			WHERE gcc_people.isStaff = '2')
			UNION
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people, gcc_committee_memberships
			WHERE gcc_people.id = gcc_committee_memberships.member
			AND gcc_committee_memberships.committee = '7'
			AND gcc_committee_memberships.type = '1')
			UNION
			(SELECT gcc_people.id as id, gcc_people.nameFirst as nameFirst, gcc_people.nameLast as nameLast, gcc_people.isCurrentSinger as isCurrentSinger, gcc_people.isStaff as isStaff
			FROM gcc_people
			WHERE gcc_people.isCurrentSinger = '2')
			ORDER BY isCurrentSinger DESC, isStaff, nameLast, nameFirst
		";
		$allPeople = mysql_query($q);
		while ($person = mysql_fetch_array($allPeople)) {
			$recordIds[] = $person['id'];
		}
	}
	echo("
		<style type=\"text/css\">
			table {
				width: 100%;
			}
			.evenColumn {
				width: 60pt;
			}
			.remainderColumn {
				width: auto;
			}
		</style>
	");
	echo("
		<table border=\"1\">
			<thead>
				<tr>
					<th colspan=\"2\" rowspan=\"2\">Singer</th>
					<th colspan=\"9\">Shirt Size</th>
					<th rowspan=\"2\">Unit Price</th>
					<th rowspan=\"2\">Amount<br />Due</th>
					<th colspan=\"3\" rowspan=\"2\">Amt. Pd.<br />by Check</th>
					<th rowspan=\"2\">Amt. Pd.<br />by PFA</th>
					<th rowspan=\"2\">Amt. Pd.<br />by Cash</th>
					<th rowspan=\"2\">Balance Due</th>
					<th rowspan=\"2\">Notes</th>
				</tr>
				<tr>
					<th>YS</th>
					<th>YM</th>
					<th>YL</th>
					<th>AS</th>
					<th>AM</th>
					<th>AL</th>
					<th>XL</th>
					<th>XXL</th>
					<th>XXXL</th>
				</tr>
			</thead>
			<tbody>
	");
	$singerObject = new person;
	$row = 3;
	foreach($recordIds as $recordId) {
		$singerObject->loadSingleRecord($recordId);
		echo("
			<tr>
				<td class=\"shrinkwrapColumn noRightBorder right\">" . $singerObject->getFieldDataByCodeName('nameFirst') . "</td>
				<td class=\"shrinkwrapColumn\">" . $singerObject->getFieldDataByCodeName('nameLast') . "</td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\">=IF((C$row+D$row+E$row+F$row+G$row+H$row+I$row+J$row+K$row)*L$row>0;(C$row+D$row+E$row+F$row+G$row+H$row+I$row+J$row+K$row)*L$row;\"\")</td>
				<td class=\"shrinkwrapColumn noRightBorder\">Ck.&nbsp;#</td>
				<td class=\"evenColumn noRightBorder\">&nbsp;</td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\"></td>
				<td class=\"evenColumn\">=IF(M$row-P$row-Q$row-R$row<>0;M$row-P$row-Q$row-R$row;\"\")</td>
				<td class=\"remainderColumn\"></td>
			</tr>
		");
		$row++;
		$firstRow = false;
	}
	echo("
				<tr>
					<td colspan=\"2\">Total</td>
					<td>=SUM(C2:C" . ($row-1) . ")</td>
					<td>=SUM(D2:D" . ($row-1) . ")</td>
					<td>=SUM(E2:E" . ($row-1) . ")</td>
					<td>=SUM(F2:F" . ($row-1) . ")</td>
					<td>=SUM(G2:G" . ($row-1) . ")</td>
					<td>=SUM(H2:H" . ($row-1) . ")</td>
					<td>=SUM(I2:I" . ($row-1) . ")</td>
					<td>=SUM(J2:J" . ($row-1) . ")</td>
					<td>=SUM(K2:K" . ($row-1) . ")</td>
					<td></td>
					<td>=SUM(M2:M" . ($row-1) . ")</td>
					<td></td>
					<td></td>
					<td>=SUM(P2:P" . ($row-1) . ")</td>
					<td>=SUM(Q2:Q" . ($row-1) . ")</td>
					<td>=SUM(R2:R" . ($row-1) . ")</td>
					<td>=SUM(S2:S" . ($row-1) . ")</td>
					<td></td>
				</tr>
			</tbody>
		</table>
	");
?>