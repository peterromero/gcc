<?php
	$peakKids = mysql_query("select `id` from `gcc_people` where `ensemble` = '2' and `isCurrentSinger` = '2' order by `birthdate` desc");
	$summitKids = mysql_query("select `id` from `gcc_people` where `ensemble` = '1' and `isCurrentSinger` = '2' order by `birthdate`");
	
	$numGroups = min(floor(mysql_num_rows($peakKids)/2), floor(mysql_num_rows($summitKids)/2));
	
	$group = array();
	for ($i = 1; $i<=$numGroups; $i++) {
		$group[$i] = array();
	}
	$i = 1;
	while($kid = mysql_fetch_array($peakKids)) {
		if (count($group[$i])==5) {
			$i++;
		}
		$group[$i][] = $kid['id'];
		$i++;
		if ($i > $numGroups) {
			$i = 1;
		}
	}
	while($kid = mysql_fetch_array($summitKids)) {
		if (count($group[$i])==5) {
			$i++;
		}
		$group[$i][] = $kid['id'];
		$i++;
		if ($i > $numGroups) {
			$i = 1;
		}
	}
	$singerObject = new person;
	echo("
		<table>
			<thead>
				<tr>
					<th>Singer</th>
					<th>Age</th>
					<th>Grade</th>
					<th>Ensemble</th>
				</tr>
			</thead>
			<tbody>
	");
		for ($i = 1; $i<=$numGroups; $i++) {
			echo("<tr><td colspan=\"4\">Group $i</td></tr>");
				foreach($group[$i] as $kidId) {
					$singerObject->loadSingleRecord($kidId);
					echo("
						<tr>
							<td>
								" . $singerObject->getFieldDataByCodeName('nameFull') . "
							</td>
							<td class=\"center\">
								" . $singerObject->getFieldDataByCodeName('age') . "
							</td>
							<td class=\"center\">
								" . $singerObject->getFieldDataByCodeName('grade') . "
							</td>
							<td>
								" . $singerObject->getFieldDataByCodeName('ensemble') . "
							</td>
						</tr>
					");
				}
			echo("<tr><td colspan=\"4\">&nbsp;</td></tr>");
		}
	echo("
			</tbody>
		</table>
	");
?>