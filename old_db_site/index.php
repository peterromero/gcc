<?php
	define('FORCE_PAGE_RELOAD', true);
	define('NO_PAGE_RELOAD', false);
	require_once("../citron/php/html.php");
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->loadModule('sql');
	$x->loadModule('formFields');
	require_once('functions/gcc_functions.php');
	require_once('classes/gcc_classes.php');
	$x->setTitle("Greeley Children's Chorale Database");
	$x->importCSS("css/gcc_styles.php");
	$x->importJavascript('js/gcc_scripts.js');
	$x->importJavascript('../citron/modules/sql/sql_scripts.js');
	$x->initialize();
	{
		$fieldDisplaySection = "
			<form id=\"fieldSelector\">
				<table>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td><input type=\"checkbox\" id=\"fieldSelectorSelAll\" onclick=\"$('.fieldSelectorCheckbox').attr('checked', $(this).attr('checked'));\" /></td>
						<td>Check/uncheck all</td>
					</tr>
					";
					$className = lookupForeignKey(getViewAttribute('table'), 'gcc_object_class_names', 'className');
					$dummyClass = new $className;
					$order = 0;
					foreach($dummyClass->dbFields as $field) {
						$fieldDisplaySection .= "
							<tr id=\"fieldSelRow$order\">
								<td><a href=\"#\" onclick=\"
									var curRow = $(this).parent().parent();
									var prevRow = curRow.prev();
									var curInput = curRow.find('input:first');
									var prevInput = prevRow.find('input:first');
									curInput.attr('value', function() {
										return Number(curInput.attr('value'))-1;
									});
									prevInput.attr('value', function() {
										return Number(prevInput.attr('value'))+1;
									});
									prevRow.before(curRow);
									return false;
								\"><img src=\"images/arrow_up.gif\" /></a></td>
								<td><a href=\"#\" onclick=\"
									var curRow = $(this).parent().parent();
									var nextRow = curRow.next();
									var curInput = curRow.find('input:first');
									var nextInput = nextRow.find('input:first');
									curInput.attr('value', function() {
										return Number(curInput.attr('value'))+1;
									});
									nextInput.attr('value', function() {
										return Number(nextInput.attr('value'))-1;
									});
									nextRow.after(curRow);
									return false;
								\"><img src=\"images/arrow_down.gif\" /></a></td>
								<td><input type=\"checkbox\" class=\"fieldSelectorCheckbox\" name=\"{$field->codeName}\" value=\"$order\" /></td>
								<td>{$field->displayName}</td>
							</tr>
						";
						$order++;
					}
					$fieldDisplaySection .= "
				</table>
			</form>
		";
		$reportSection = "";
		$reports = scandir('reports');
		foreach ($reports as $report) {
			if($report != '.' && $report != '..' && $report != 'error_log' && $report != 'report_wrapper.php' && $report != '.htaccess' && $report != 'index.php') {
				$cleanName = str_replace(array('_', '.php'), array(' ', null), $report);
				$reportSection .= "
					<p><a href=\"#\" onclick=\"loadReport('$report');return false;\">$cleanName</a></p>
				";
			}
		}
	}
	echo("
		<div id=\"content_area\">
			<div id=\"left_column\" class=\"column\">
				<div class=\"nav_bar_wrapper\">
					<ul class=\"nav_bar\">
						"
							.getNavBarItem(
								"data",
								getMenuBar(
									getMenuBarItem(
										"Data type",
										"
											<select id=\"dataTable\" size=6>
												<option value=\"gcc_people\">People</option>
												<option value=\"gcc_schools\">Schools</option>
												<option value=\"gcc_relationships\">Relationships</option>
												<option value=\"gcc_committees\">Committees</option>
												<option value=\"gcc_committee_memberships\">Committee Members</option>
												<option value=\"gcc_news_articles\">News Articles</option>
											</select>
										",
										FORCE_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Output format",
										"
											<select id=\"outputFormat\" size=2>
												<option value=\"table\">Table</option>
												<option value=\"list\">List</option>
											</select>
										",
										NO_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Filtering",
										"
											<table>
												<tr>
													<td>
														Filter:
													</td>
													<td>
														<input type=\"text\" id=\"filterQuery\" size=\"48\" />
													</td>
												</tr>
												<tr>
													<td>
														Limit results to:
													</td>
													<td>
														<input type=\"text\" id=\"maxResults\" size=\"3\" /> records
													</td>
												</tr>
												<tr>
													<td>
														Partition results into:
													</td>
													<td>
														<input type=\"text\" id=\"numPartitions\" size=\"3\" /> pieces
													</td>
												</tr>
											</table>
										",
										NO_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Fields to display",
										$fieldDisplaySection,
										NO_PAGE_RELOAD
									)
								)
							)
							.getNavBarItem(
								"formatting",
								getMenuBar(
									getMenuBarItem(
										"Lists",
										"No content here yet!",
										NO_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Names",
										"
											<select id=\"nameDisplay\" size=\"2\">
												<option value=\"firstLast\">First Last</option>
												<option value=\"lastFirst\">Last, First</option>
											</select>
										",
										NO_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Multiply associated fields",
										"
											<p>
												<input type=\"checkbox\" id=\"multi_showEdit\" value=\"1\" /> Allow editing from parent records
											</p>
											<p>
												<input type=\"checkbox\" id=\"multi_describe\" value=\"1\" /> Show descriptors
											</p>
										",
										NO_PAGE_RELOAD
									)
								)
							)
							.getNavBarItem(
								"reports &amp views",
								getMenuBar(
									getMenuBarItem(
										"Reports",
										$reportSection,
										NO_PAGE_RELOAD
									)
									.getMenuBarItem(
										"Views",
										"Content",
										NO_PAGE_RELOAD
									)
								)
							)
						."
					</ul>
				</div>
			</div>
			<div id=\"right_column\" class=\"column\">
				<div class=\"column_content\">
					<h1>Data Result</h1>
					<div id=\"dataArea\">
						Data not yet loaded
					</div>
					<div id=\"hiddenFields\">
						<input type=\"hidden\" id=\"rowSortFieldName\" value=\"\" />
					</div>
				</div>
			</div>
		</div>
		<div id=\"shader\">
			<div id=\"popup\">
				<div id=\"editFormArea\"></div>
				<p><a href=\"#\" onclick=\"$('#shader').hide();return false;\">Close window</a></p>
			</div>
		</div>
		<form id=\"reportForm\" method=\"get\" action=\"reports/report_wrapper.php\" target=\"_blank\">
			<input type=\"hidden\" id=\"reportFileName\" name=\"reportFileName\" value=\"\" />
			<input type=\"hidden\" id=\"reportRecordIds\" name=\"reportRecordIds\" value=\"\" />
			<input type=\"hidden\" id=\"reportDataTable\" name=\"reportDataTable\" value=\"\" />
		</form>
	");
	$x->close();
	function getNavBarItem($displayName, $content) {
		return "
			<li class=\"nav_bar_li\">
				<a href=\"#\">$displayName</a>
				$content
			</li>
		";
	}
	function getMenuBar($content) {
		return "
			<div class=\"menu_bar_wrapper\">
				<img src=\"images/side_menu_pointer.gif\" />
				<ul class=\"menu_bar\">
					$content
				</ul>
			</div>
		";
	}
	function getMenuBarItem($displayName, $content, $forceReload) {
		$forceReloadClass = ($forceReload == FORCE_PAGE_RELOAD)?' force_reload':null;
		return "
			<li class=\"menu_bar_li\">
				<a href=\"#\">$displayName</a>
				<div class=\"menu_panel\">
					<div class=\"menu_content\">
						$content
					</div>
					<a class=\"ok_button{$forceReloadClass}\" href=\"#\">&nbsp;</a>
				</div>
			</li>
		";
	}
?>