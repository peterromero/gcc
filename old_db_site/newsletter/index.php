<?php
	require_once('../../citron/php/html.php');
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->setTitle("Greeley Children's Chorale Newsletter");
	$x->initialize();
	
	$articles = mysql_query("select title, text from gcc_news_articles order by title");
	
	while($article = mysql_fetch_array($articles)) {
		echo("
			<h1>
				{$article['title']}
			</h1>
			<p>
				{$article['text']}
			</p>
		");
	}
	
	$x->close();
?>