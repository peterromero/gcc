<?php
	require_once("/home/meesezc/public_html/citron/php/html.php");
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->loadModule('formatting');
	$x->loadModule('sql');
	$x->loadModule('formFields');
	$x->loadModule('lists');
	require_once("../functions/gcc_functions.php");
	require_once("../classes/gcc_classes.php");

	foreach($_POST['recordIds'] as $recordId) {
		$recordIds[] = $recordId['value'];
	}
	$recordObject = new person;
	$recordObject->loadRecords($recordIds);
	
	$q = "SELECT `id`, `honorific`, `nameFirst`, `nameLast`, `email1`, `email2` FROM `gcc_people` WHERE " . $recordObject->getSqlRecordIdList();
	$recordset = mysql_query($q);
	$emails = array();
	while ($record = mysql_fetch_array($recordset)) {
		if (!array_key_exists($record['email1'], $emails)&&$record['email1']!='') {
			$emails[$record['email1']] = formatName($record['honorific'], $record['nameFirst'], $record['nameLast'], 'firstLast', 'full');
		}
		if (!array_key_exists($record['email2'], $emails)&&$record['email2']!='') {
			$emails[$record['email2']] = formatName($record['honorific'], $record['nameFirst'], $record['nameLast'], 'firstLast', 'full');
		}
		$qR = "SELECT `honorific`, `nameFirst`, `nameLast`, `email1`, `email2` FROM `gcc_people`, `gcc_relationships` WHERE `gcc_relationships`.`memberLower` = '{$record['id']}' AND `gcc_people`.`id` = `gcc_relationships`.`memberUpper`";
		$upperRelatedPeople = mysql_query($qR);
		while ($upperRelation = mysql_fetch_array($upperRelatedPeople)) {
			if (!array_key_exists($upperRelation['email1'], $emails)&&$upperRelation['email1']!='') {
				$emails[$upperRelation['email1']] = formatName($upperRelation['honorific'], $upperRelation['nameFirst'], $upperRelation['nameLast'], 'firstLast', 'full');
			}
			if (!array_key_exists($upperRelation['email2'], $emails)&&$upperRelation['email2']!='') {
				$emails[$upperRelation['email2']] = formatName($upperRelation['honorific'], $upperRelation['nameFirst'], $upperRelation['nameLast'], 'firstLast', 'full');
			}
		}
	}
	foreach($emails as $email => $name) {
		$displayEmails[] = "$name &lt;$email&gt;";
		// $displayEmails[] = "$email";
	}
	echo("
		<p>You may copy and paste the email addresses below into your email program:</p>
		<p class=\"code\">" . implode(', ', $displayEmails) . "</p>
		<p>Please note that some selected names may not have email addresses in the system.</p>
	");
?>