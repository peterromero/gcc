<?php
	require_once("/home/meesezc/public_html/citron/php/html.php");
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->loadModule('formatting');
	$x->loadModule('formFields');
	$x->loadModule('sql');
	$x->loadModule('math');
	require_once('../classes/gcc_classes.php');
	require_once('../functions/gcc_functions.php');

	{ //Load view options.
		$outputFormat = getViewAttribute('outputFormat');
		$fields = json_decode(getViewAttribute('includeFields'));
		if (count($fields)==0||!is_array($fields)) { //Having no fields selected is the same as having them all selected.
			$includeFields = ALL_FIELDS;
		} else {
			foreach($fields as $field) {
				$includeFields[$field->value] = $field->name;
			}
		}
		$tableName = getViewAttribute('table');
		$className = lookupForeignKey($tableName, 'gcc_object_class_names', 'className');
	}
	
	$recordObject = new $className;
	$filterQuery = ($fQ=getViewAttribute('filterQuery'))?$fQ:1;
	$maxResults = getViewAttribute('maxResults');
	$limitQuery = ($maxResults>0)?" LIMIT $maxResults":'';
	$q = "SELECT `id` FROM `$tableName` WHERE ($filterQuery)$limitQuery";
	// echo($q); exit();
	$records = mysql_query($q);
	switch($outputFormat) {
		case 'table': {
			echo(buildTableFromSqlRecordset($records, $className, $includeFields));
			echo("
				<p id=\"addLink\" style=\"margin-top: 20px\">
					<a accesskey=\"a\" href=\"#\" onclick=\"getModForm('add', '$tableName', null);return false;\">
						<span class=\"accessKeyLabel\">A</span>dd new record
					</a>
				</p>
				<p id=\"editLinks\" style=\"margin-top: 20px\">
					With checked:
					<a href=\"#\" onclick=\"getModForm('edit', '$tableName', null);return false;\">
						Edit
					</a>
					<a href=\"#\" onclick=\"getModForm('delete', '$tableName', null);return false;\">
						Delete
					</a>
					<a href=\"#\" onclick=\"getEmailLinks();return false;\">
						Get email links
					</a>
				</p>
			");
			break;
		}
		case 'list': {
			echo(buildListFromSqlRecordset($records, $className, $includeFields));
			break;
		}
	}
?>