<?php
	require_once("/home/meesezc/public_html/citron/php/html.php");
	$x = new htmlDocument();
	$x->loadModule('database');
	$x->loadModule('formatting');
	$x->loadModule('sql');
	$x->loadModule('formFields');
	$x->loadModule('lists');
	require_once("../functions/gcc_functions.php");
	require_once("../classes/gcc_classes.php");
	
	// var_dump($_REQUEST); exit();
	$recordIds = array();
	if (is_array($_POST['recordIds'])) {
		foreach($_POST['recordIds'] as $recordId) {
			$recordIds[] = $recordId['value'];
		}
	}
	$className = lookupForeignKey($_REQUEST['tableName'], 'gcc_object_class_names', 'className');
	$recordObject = new $className;
	$recordObject->loadRecords($recordIds);
	
	switch($_POST['method']) {
		case 'addEdit': {
			echo($recordObject->getEditForm('form1'));
			break;
		}
		case 'delete': {
			if($success = mysql_query($recordObject->getDeleteQuery())) {
				echo("Query was successful!");
			} else {
				echo("Query was unsuccessful (" . $recordObject->getDeleteQuery() . "). The database has not been altered.");
			}
			break;
		}
	}
?>