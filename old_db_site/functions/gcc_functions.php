<?php
	define('GENDER_FEMALE', 1);
	define('GENDER_MALE', 2);
	define('GENDER_UNKNOWN', 3);
	define('VALUE_NO', 1);
	define('VALUE_YES', 2);
	define('VALUE_MAYBE', 3);
	function getViewAttribute($attrName) {
		$attr = mysql_fetch_array(mysql_query("SELECT `$attrName` FROM `gcc_data_views` WHERE `id` = '1'"));
		return $attr[0];
	}
	function calcYearsInGcc($isSinger, $startYear, $endYear) {
		if (is_blank($startYear)) {
			if (is_blank($endYear)) {
				if($isSinger == INT_YES) {
					$yGcc = '?';
				} else {
					$yGcc = '';
				}
			} else {
				if($isSinger == INT_YES) {
					$yGcc = '?';
				} else {
					$yGcc = '';
				}
			}
		} else {
			if (is_blank($endYear)) {
				$yGcc = getSchoolYear(time()) - $startYear + 1;
			} else {
				$yGcc = $endYear - $startYear;
			}
		}
		if ($yGcc<0) $yGcc=0;
		return $yGcc;
	}
	function calcCompositeTuition($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup) {
		if ($isSinger == INT_NO) return '';
		$bareTuition = 200;
		$uniform = 50;
		$registration = 50;
		$tickets = 150 / ($siblingsInGroup+1);
		switch($grantCalculation) {
			case 1: { //Percentage
				$bareTuition *= (1 - $grantAmount);
				break;
			}
			case 2: { //Dollar amount
				$bareTuition -= $grantAmount;
				break;
			}
		}
		return $bareTuition + $uniform + $tickets + $registration;
	}
	function calcTickets($isSinger, $siblingsInGroup) {
		if ($isSinger == INT_NO) return '';
		$tickets = 150 / ($siblingsInGroup+1);
		return $tickets;
	}
	function calcSimpleTuition($isSinger, $grantAmount, $grantCalculation) {
		if ($isSinger == INT_NO) return '';
		$bareTuition = 200;
		switch($grantCalculation) {
			case 1: { //Percentage
				$bareTuition *= (1 - $grantAmount);
				break;
			}
			case 2: { //Dollar amount
				$bareTuition -= $grantAmount;
				break;
			}
		}
		return $bareTuition;
	}
	function calcCompositeTuitionString($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup) {
		if ($isSinger == INT_NO) return '';
		return formatMoney(calcCompositeTuition($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup), USE_DOLLAR_SIGN, USE_SUPERSCRIPTS);
	}
	function calcAveragePaymentString($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup) {
		if ($isSinger == INT_NO) return '';
		return formatMoney(calcCompositeTuition($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup)/5, USE_DOLLAR_SIGN, USE_SUPERSCRIPTS);
	}
	function calcFirstPaymentString($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup) {
		if ($isSinger == INT_NO) return '';
		return formatMoney(50+calcCompositeTuition($isSinger, $grantAmount, $grantCalculation, $siblingsInGroup)/5, USE_DOLLAR_SIGN, USE_SUPERSCRIPTS);
	}
	function formatGrant($amount, $calcMethod) {
		switch  ($calcMethod) {
			case 2: { //Straight dollar amount
				$g = formatMoney($amount,true,true);
				break;
			}
			case 1: { //Percentage
				$g = percent($amount);
				break;
			}
		}
		return $g;
	}
	function isCurrentParent($personId) {
		$q = "select null as `x` from gcc_people, gcc_relationships where gcc_people.id = gcc_relationships.memberLower and gcc_relationships.memberUpper = '$personId' and (gcc_relationships.type = 1 or gcc_relationships.type = 2) and gcc_people.isCurrentSinger = 2";
		$kids = mysql_query($q);
		if (mysql_num_rows($kids)>0) {
			return 'Yes';
		} else {
			return 'No';
		}
	}
	function getCommitteeMembers($committeeId) {
		$members = lookupMultiAssoc(
			'gcc_committees',
			'gcc_committee_memberships',
			'gcc_people',
			$committeeId,
			'committee',
			'member',
			null,
			array('id', 'type', 'isChair', 'isBoardLiaison'),
			array('id', 'honorific', 'nameFirst', 'nameLast'),
			null,
			null,
			null
		);
		foreach($members as $record) {
			// var_dump($record);
			$nameDisplay = formatName($record['honorific'], $record['nameFirst'], $record['nameLast'], getViewAttribute('nameDisplay'), 'full');
			$typeDisplay = lookupForeignKey($record['type'], 'gcc_committee_involvement_types', 'name');
			$chairClass = ($record['isChair']==VALUE_YES)?' class="committeeChair"':'';
			$boardLiaisonClass = ($record['isBoardLiaison']==VALUE_YES)?' class="boardLiaison"':'';
			$chairMarker = ($record['isChair']==VALUE_YES)?'*':'';
			$boardLiaisonMarker = ($record['isBoardLiaison']==VALUE_YES)?'**':'';
			if (getViewAttribute('multi_showEdit')=='1') {
				$output .= "
					<a href=\"#\" title=\"Edit membership\" onclick=\"getModForm('edit', 'gcc_committee_memberships', '{$record['id']}');return false;\" >
						<img src=\"images/pencil.png\" class=\"edit icon\" />
					</a>
					<a href=\"#\" title=\"Cancel membership\" onclick=\"getModForm('delete', 'gcc_committee_memberships', '{$record['id']}');return false;\" >
						<img src=\"images/cross.png\" class=\"delete icon\" />
					</a>
				";
			}
			$output .= "<a href=\"#\"{$chairClass}{$boardLiaisonClass} onclick=\"getModForm('edit', 'gcc_people', {$record[0]});return false;\">$nameDisplay{$chairMarker}{$boardLiaisonMarker}</a>";
			if (getViewAttribute('multi_describe')=='1') {
				$output .= " ($typeDisplay)";
			}
			$output .= "<br />";
		}
		if (getViewAttribute('multi_showEdit')=='1') {
			$output = $output . "<a href=\"#\" onclick=\"getModForm('add', 'gcc_committee_memberships', null);return false;\"><img src=\"images/plus.png\" class=\"add icon\" />Link to member...</a>";
		}
		return $output;
	}
	function getCommitteeMemberships($personId) {
		$members = lookupMultiAssoc(
			'gcc_people',
			'gcc_committee_memberships',
			'gcc_committees',
			$personId,
			'member',
			'committee',
			null,
			array('type', 'id', 'isChair'),
			array('name', 'id'),
			null,
			null,
			null
		);
		foreach($members as $record) {
			$typeDisplay = lookupForeignKey($record['type'], 'gcc_committee_involvement_types', 'name');
			$chairClass = ($record['isChair']==VALUE_YES)?' class="committeeChair"':'';
			if (getViewAttribute('multi_showEdit')=='1') {
				$output .= "
					<a href=\"#\" title=\"Edit membership\" onclick=\"getModForm('edit', 'gcc_committee_memberships', '{$record['id']}');return false;\" >
						<img src=\"images/pencil.png\" class=\"edit icon\" />
					</a>
					<a href=\"#\" title=\"Cancel membership\" onclick=\"getModForm('delete', 'gcc_committee_memberships', '{$record['id']}');return false;\" >
						<img src=\"images/cross.png\" class=\"delete icon\" />
					</a>
				";
			}
			$output .= "
				<a href=\"#\"{$chairClass} onclick=\"getModForm('edit', 'gcc_committees', {$record[1]});return false;\">{$record['name']}</a> ($typeDisplay)
				<br />
			";
		}
		if (getViewAttribute('multi_showEdit')=='1') {
			$output = $output . "<a href=\"#\" onclick=\"getModForm('add', 'gcc_committee_memberships', null);return false;\"><img src=\"images/plus.png\" class=\"add icon\" />Link to committee...</a>";
		}
		return $output;
	}
	function getRelatedPeople($id, $joinConditions) {
		$upperRelations = lookupMultiAssoc('gcc_people', 'gcc_relationships', 'gcc_people', $id, 'memberLower', 'memberUpper', array('id', 'honorific', 'nameFirst', 'nameLast', 'gender'), array('id', 'type', 'emergency'), array('id', 'honorific', 'nameFirst', 'nameLast', 'gender'), null, $joinConditions, null);
		foreach($upperRelations as $record) {
			switch($record['gender']) {
				case GENDER_FEMALE: {
					$lookupFieldName = 'nameUpperFemale';
					break;
				}
				case GENDER_MALE: {
					$lookupFieldName = 'nameUpperMale';
					break;
				}
				case GENDER_UNKNOWN: {
					$lookupFieldName = 'nameUpperNa';
					break;
				}
			}
			$nameDisplay = formatName($record['honorific'], $record['nameFirst'], $record['nameLast'], getViewAttribute('nameDisplay'), 'full');
			$typeDisplay = lookupForeignKey($record['type'], 'gcc_relationship_types', $lookupFieldName);
			$emergencyClass = ($record['emergency']==VALUE_YES)?' class="emergencyContact"':'';
			if (getViewAttribute('multi_showEdit')=='1') {
				$output .= "
					<a href=\"#\" title=\"Edit relationship\" onclick=\"getModForm('edit', 'gcc_relationships', '{$record['id']}');return false;\" >
						<img src=\"images/pencil.png\" class=\"edit icon\" />
					</a>
					<a href=\"#\" title=\"Break relationship\" onclick=\"getModForm('delete', 'gcc_relationships', '{$record['id']}');return false;\" >
						<img src=\"images/cross.png\" class=\"delete icon\" />
					</a>
				";
			}
			if (getViewAttribute('multi_describe')=='1') {
				$output .= "$typeDisplay: ";
			}
			$output .= "
				<a href=\"#\"{$emergencyClass} onclick=\"getModForm('edit', 'gcc_people', {$record[0]});return false;\">$nameDisplay</a>
				<br />
			";
		}
		$lowerRelations = lookupMultiAssoc( 'gcc_people', 'gcc_relationships', 'gcc_people', $id, 'memberUpper', 'memberLower', array('id', 'honorific', 'nameFirst', 'nameLast', 'gender'), array('id', 'type', 'emergency'), array('id', 'honorific', 'nameFirst', 'nameLast', 'gender'), null, null, null );
		foreach($lowerRelations as $record) {
			$nameDisplay = formatName($record['honorific'], $record['nameFirst'], $record['nameLast'], getViewAttribute('nameDisplay'), 'full');
			switch($record['gender']) {
				case GENDER_FEMALE: {
					$lookupFieldName = 'nameLowerFemale';
					break;
				}
				case GENDER_MALE: {
					$lookupFieldName = 'nameLowerMale';
					break;
				}
				case GENDER_UNKNOWN: {
					$lookupFieldName = 'nameLowerNa';
					break;
				}
			}
			$typeDisplay = lookupForeignKey($record['type'], 'gcc_relationship_types', $lookupFieldName);
			$emergencyClass = ($record['emergency']==VALUE_YES)?' class="emergencyContact"':'';
			if (getViewAttribute('multi_showEdit')=='1') {
				$output .= "
					<a href=\"#\" title=\"Edit relationship\" onclick=\"getModForm('edit', 'gcc_relationships', '{$record['id']}');return false;\" >
						<img src=\"images/pencil.png\" class=\"edit icon\" />
					</a>
					<a href=\"#\" title=\"Break relationship\" onclick=\"getModForm('delete', 'gcc_relationships', '{$record['id']}');return false;\" >
						<img src=\"images/cross.png\" class=\"delete icon\" />
					</a>
				";
			}
			if (getViewAttribute('multi_describe')=='1') {
				$output .= "$typeDisplay: ";
			}
			$output .= "
				<a href=\"#\"{$emergencyClass} onclick=\"getModForm('edit', 'gcc_people', {$record[0]});return false;\">$nameDisplay</a>
				<br />
			";
		}
		if (getViewAttribute('multi_showEdit')=='1') {
			$output = $output . "<a href=\"#\" onclick=\"getModForm('add', 'gcc_relationships', null);return false;\"><img src=\"images/plus.png\" class=\"add icon\" />Link to relation...</a>";
		}
		return $output;
	}
	function getCurrentStatus($personId) {
		$person = mysql_fetch_array(mysql_query("select * from gcc_people where id = '$personId' limit 1"));
		$statuses = array();
		switch ($person['isCurrentSinger']) {
			case 2: {
				$statuses[] = "Current Singer";
				break;
			}
			case 3: {
				$statuses[] = "Possible Current Singer";
				break;
			}
		}
		switch ($person['isStaff']) {
			case 2: {
				$statuses[] = "GCC Staff Member";
				break;
			}
			case 3: {
				$statuses[] = "Possible GCC Staff Member";
				break;
			}
		}
		switch ($person['isAlumnus']) {
			case 2: {
				switch ($person['isGraduate']) {
					case 1: {
						$statuses[] = "Alumnus (Non-Graduate)";
						break;
					}
					case 2: {
						$statuses[] = "Alumnus (Graduate)";
						break;
					}
					case 3: {
						$statuses[] = "Alumnus (Possible Graduate)";
						break;
					}
				}
				break;
			}
			case 3: {
				switch ($person['isGraduate']) {
					case 1: {
						$statuses[] = "Possible Alumnus (Non-Graduate)";
						break;
					}
					case 2: {
						$statuses[] = "Possible Alumnus (Graduate)";
						break;
					}
					case 3: {
						$statuses[] = "Possible Alumnus (Possible Graduate)";
						break;
					}
				}
				break;
			}
		}
		$q = "select gcc_people.id, gcc_relationships.type from gcc_people, gcc_relationships where gcc_people.id = gcc_relationships.memberLower and gcc_relationships.memberUpper = '$personId'";
		$relatedSingers = mysql_query($q);
		if (mysql_num_rows($relatedSingers)>0) {
			while($relatedSinger = mysql_fetch_array($relatedSingers)) {
				$kid = mysql_fetch_array(mysql_query("select isCurrentSinger, isAlumnus, isGraduate from gcc_people where id = '{$relatedSinger['id']}' limit 1"));
				$relationshipType = mysql_fetch_array(mysql_query("select nameUpperNa as name from gcc_relationship_types where id = '{$relatedSinger['type']}' limit 1"));
				switch ($kid['isCurrentSinger']) {
					case 2: {
						$statuses[] = "{$relationshipType['name']} of Current Singer";
						break;
					}
					case 3: {
						$statuses[] = "{$relationshipType['name']} of Possible Current Singer";
						break;
					}
				}
				if ($kid['isAlumnus']>1) {
					if ($kid['isGraduate']>1) {
						$statuses[] = "{$relationshipType['name']} of Alumnus (Graduate)";
					} else {
						$statuses[] = "{$relationshipType['name']} of Alumnus (Non-Graduate)";
					}
				}
			}
		}
		$q = "select gcc_committees.id, gcc_committee_memberships.type from gcc_committees, gcc_committee_memberships where gcc_committees.id = gcc_committee_memberships.committee and gcc_committee_memberships.member = '$personId'";
		// echo($q);
		$committeeMemberships = mysql_query($q);
		if (mysql_num_rows($committeeMemberships)>0) {
			while ($committeeMembership = mysql_fetch_array($committeeMemberships)) {
				switch ($committeeMembership['type']) {
					case 1: {
						$statuses[] = "Current Committee Member";
						break;
					}
					case 2: {
						$statuses[] = "Interested Committee Member";
						break;
					}
					case 3: {
						$statuses[] = "Interested Committee Member";
						break;
					}
					case 4: {
						$statuses[] = "Interested Committee Member";
						break;
					}
				}
			}
		}
		if (count($statuses)) {
			$statusStr = implode('<br />', array_unique($statuses));
		} else {
			$statusStr = "Unknown";
		}
		return $statusStr;
	}
?>