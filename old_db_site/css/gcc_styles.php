<?php
	header("Content-type: text/css");
	echo("
	* {
		color: #4d4d4d;
		font: 12px/14px arial;
		margin: 0;
		padding: 0;
	}
	input, textarea {
		border: solid 1px #c0c0c0;
		padding: 3px;
		background-color: #ffffff;
	}
	input:focus, select:focus, textarea:focus {
		background-color: #ffffa0;
	}
	input[type=checkbox] {
		vertical-align: middle;
		margin: 0 6px;
	}
	html {
		height: 100%;
	}
	body {
		background-color: #f2f2f2;
		height: 100%;
	}
	a {
		text-decoration: none;
		outline: none;
		color: #006ab5;
		cursor: pointer;
	}
	a:hover {
		text-decoration: underline;
		color: #652b80;
	}
	p {
		margin: 5px 0 5px 0;
	}
	p.code {
		margin: 20px;
		font: 12px/14px courier, fixed;
	}
	h1 {
		color: #b2b2b2;
		font: italic 24px/24px georgia, serif;
		padding-bottom: 18px;
	}
	img {
		border: none;
	}
	td {
		padding: 3px;
	}
	select {
		border: solid 1px #c0c0c0;
	}
	option {
		padding: 3px;
	}
	span.accessKeyLabel {
		text-decoration: underline;
		color: #006ab5;
	}
	#content_area {
		background-color: white;
		box-shadow: 0 0 12px 0 #909090;
		-moz-box-shadow: 0 0 12px 0 #909090;
		-webkit-box-shadow: 0 0 12px 0 #909090;
		-khtml-box-shadow: 0 0 12px 0 #909090;
		float: left;
		height: auto;
		left: 50%;
		margin-left: -480px;
		position: relative;
		width: 960px;
		background: white url('../images/columnbg.gif') repeat-y 0 0;
		overflow: visible;
	}
	.column {
		float: left;
	}
	#left_column {
		background-color: #e5e5e5;
		width: 120px;
	}
	#right_column {
		width: 840px;
	}
	.column_content {
		padding: 25px;
	}
	.nav_bar_wrapper {
		position: relative;
		top: 25px;
	}
	.nav_bar, .menu_bar {
		list-style-type: none;
	}
	.nav_bar_li {
		position: relative;
		z-index: 5;
	}
	.nav_bar_li > a, .tableHeaderSortLink {
		background-color: #808080;
		color: #e5e5e5;
		height: 24px;
		display: block;
		text-align: center;
		line-height: 22px;
		z-index: 1;
		padding: 0 5px;
	}
	.nav_bar_li > a.active, .menu_bar_li > a.active, .tableHeaderSortLink.active {
		background-color: #006ab5;
		color: #ffffff;
	}
	.nav_bar_li > a:hover, .tableHeaderSortLink:hover {
		color: #ffffff;
		background-color: #666666;
		text-decoration: none;
	}
	.nav_bar_li > a.active:hover, .menu_bar_li > a.active:hover, .tableHeaderSortLink.active:hover {
		background-color: #006ab5;
	}
	.menu_bar_wrapper {
		background-color: #e5e5e5;
		left: 120px;
		position: absolute;
		top: 0;
		width: 840px;
		display: none;
		z-index: -1;
		box-shadow: 0 0 6px 0 #909090;
		-moz-box-shadow: 0 0 6px 0 #909090;
		-webkit-box-shadow: 0 0 6px 0 #909090;
		-khtml-box-shadow: 0 0 6px 0 #909090;
	}
	.menu_bar_wrapper img {
		float: left;
	}
	.menu_bar {
		float: left;
	}
	.menu_bar_li {
		margin-left: 50px;
		float: left;
		position: relative;
	}
	.menu_bar_li > a { 
		background-color: transparent;
		color: #4d4d4d;
		text-transform: lowercase;
		height: 24px;
		display: block;
		line-height: 22px;
		padding: 0 20px;
	}
	.menu_bar_li > a:hover {
		background-color: #cccccc;
		text-decoration: none;
	}
	.menu_panel {
		position: absolute;
		left: 0px;
		top: 27px;
		background-color: #e2e2e2;
		box-shadow: 0 0 6px 0 #909090;
		-moz-box-shadow: 0 0 6px 0 #909090;
		-webkit-box-shadow: 0 0 6px 0 #909090;
		-khtml-box-shadow: 0 0 6px 0 #909090;
		display: none;
	}
	.menu_content {
		padding: 18px;
	}
	.ok_button {
		width: 24px;
		height: 24px;
		display: block;
		outline: none;
		background: transparent url('../images/okbutton.gif') no-repeat 0 0;
		float: right;
	}
	.ok_button:hover {
		background-position: -24px 0px;
	}
	table {
		border-collapse: collapse;
	}
	th {
		font-weight: bold;
		background-color: #808080;
		color: #ffffff;
		text-align: center;
		padding: 4px 8px;
	}
	table.records td {
		padding: 4px 8px;
	}
	table.records img {
		vertical-align: middle;
	}
	table.editForm td {
		padding: 4px 12px;
	}
	table.records th {
		padding: 0 !important;
	}
	table.records tr:nth-child(odd) td, table.editForm tr:nth-child(odd) td {
		//background-color: #e5f0f7; //GCC blue at 10%;
		background-color: #f2f2f2;
	}
	table.records tr:nth-child(even):hover td {
		background-color: #e5f0f7;
	}
	table.records tr:nth-child(odd):hover td {
		background-color: #d9e4ea;
	}
	table.editForm td, table.editForm th {
		border-width: 0 1px 0 0;
		border-color: #c0c0c0;
		border-style: solid;
	}
	table.editForm td:last-child {
		border-width: 0;
	}
	table.editForm td:nth-child(1) {
		text-align: right;
	}
	table.editForm td:nth-child(2), table.editForm td:nth-child(3) {
		text-align: center;
	}
	#shader {
		background-color: #808080;
		height: 100%;
		margin: 0;
		padding: 0;
		position: absolute;
		width: 100%;
		z-index: 1000;
		display: none;
	}
	#popup {
		background-color: white;
		box-shadow: 0 5px 5px;
		margin: 50px;
		padding: 50px;
		width: auto;
		-moz-border-radius: 6px;
		border-radius: 6px;
		-webkit-border-radius: 6px;
		-khtml-border-radius: 6px;
	}
	#editLinks {
		display: none;
	}
	#editLinks a {
		margin: 0 0 0 12px;
	}
	tr.partitionClass1 td {
	}
	tr.partitionClass2:nth-child(odd) td {
		background-color: #a9a9a9 !important;
	}
	tr.partitionClass2:nth-child(even) td {
		background-color: #b2b2b2 !important;
	}
	tr.partitionClass2:nth-child(odd):hover td {
		background-color: #989fa4 !important;
	}
	tr.partitionClass2:nth-child(even):hover td {
		background-color: #a0a8ac !important;
	}
	.emergencyContact, .committeeChair, .boardLiaison {
		font-weight: bold;
	}
	.icon {
		margin-right: 5px;
	}
	");
?>