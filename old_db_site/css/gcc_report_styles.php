<?php
	header("Content-type: text/css");
	echo("
		* {
			font-family: candara, georgia;
			text-decoration: none;
		}
		@media print {
			body {
				padding: 0;
				margin: 0;
			}
			* {
				color: black;
				font-size: 8pt;
				line-height: 8pt;
			}
			td, th {
				padding: 1pt;
			}
			td {
				border: solid 1pt #000000;
			}
			th {
				color: white;
				background-color: black;
				border: solid 1pt white;
			}
			tr.multiFirst td {
				border-top: solid 2pt black;
			}
			td.thickRightBorder {
				border-right: solid 2pt black;
			}
		}
		@media screen {
			body {
				padding: 100px;
				margin: 0;
				background-color: #e8e8e8;
			}
			* {
				font-size: 9pt;
				line-height: 9pt;
				color: #606060;
				text-shadow: 0 1px 0 white;
			}
			td, th {
				padding: 3pt;
				border: solid 1px #c0c0c0;
			}
			th {
				color: #e8e8e8;
				background-color: #606060;
			}
			tr.multiFirst td {
				border-top: solid 2pt #a0a0a0;
			}
			td.thickRightBorder {
				border-right: solid 2pt #a0a0a0;
			}
		}
		table {
			border-collapse: collapse;
		}
		td, th {
			vertical-align: middle;
		}
		th {
			font-weight: bold;
			text-align: center;
			text-shadow: none;
		}
		thead {
			display: table-header-group;
		}
		td.center {
			text-align: center;
		}
		td.right {
			text-align: right;
		}
		td.shrinkwrapColumn {
			width: 1pt;
		}
		td.noRightBorder {
			border-right: none;
		}
		td.noRightBorder + td {
			border-left: none;
		}
	");
?>