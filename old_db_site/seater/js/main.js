var num_sections = 1;
var section_rows = new Array();
section_rows[1] = 1;
$(function() {
	$('.collapsible_section h2').click(function() {
		$(this).parent().find('.collapsible_section_contents').slideToggle(400);
	});
});
function submit_seating_area_definition() {
	if (validate_seating_area_definition()==false) {
		alert('Please fill in the fields marked in red.');
	} else {
		$.post(
			'ajax/main.php',
			$('#seating_area_definition').serialize().replace(/%5B%5D/g, '[]'),
			function(data) {
				data = JSON.parse(data);
				$('.mainColumn').html(data.html);
			}
		);
	}
}
function validate_seating_area_definition() {
	var valid = true;
	$('.section_id').add('.row_id').add('.seat_range_number').each(function() {
		$(this).removeClass('invalid_input');
		if ($(this).val()=='') {
			valid = false;
			$(this).addClass('invalid_input');
		}
	});
	return valid;
}
function add_row_definition(section_id) {
	section_rows[section_id]++;
	$('#sd_'+section_id + ' table.data_entry').append("\
		<tr class=\"row_definition\" id=\"sd_"+num_sections+"_rd_"+section_rows[section_id]+"\">\
			<td>\
				<input type=\"text\" name=\"section["+section_id+"][rows]["+section_rows[section_id]+"][id]\" class=\"row_id\" />\
			</td>\
			<td class=\"no_right_border\">\
				<input type=\"text\" name=\"section["+section_id+"][rows]["+section_rows[section_id]+"][seat_range_low]\" class=\"seat_range_number\" />\
			</td>\
			<td class=\"no_right_border\">\
				&nbsp;to&nbsp;\
			</td>\
			<td>\
				<input type=\"text\" name=\"section["+section_id+"][rows]["+section_rows[section_id]+"][seat_range_high]\" class=\"seat_range_number\" />\
			</td>\
			<td>\
				<a href=\"#\" onclick=\"delete_definition('sd_"+num_sections+"_rd_"+section_rows[section_id]+"');return false;\" tabindex=\"-1\" class=\"char_button\">x</a>\
			</td>\
		</tr>\
	");
}
function add_section_definition() {
	num_sections++;
	section_rows[num_sections]=1;
	$('#seating_area_definition').append("\
		<div class=\"section_definition\" id=\"sd_"+num_sections+"\">\
			<div class=\"title\">\
				Section <a href=\"#\" onclick=\"delete_definition('sd_"+num_sections+"');return false;\" class=\"char_button\">x</a>\
			</div>\
			<div class=\"contents\">\
				<p><strong>Section&nbsp;ID:</strong> <input type=\"text\" name=\"section["+num_sections+"][id]\" class=\"section_id\" /></p>\
				<p><strong>Rows &amp; Seats:</strong> <a href=\"#\" class=\"add_row_button\"\ onclick=\"add_row_definition("+num_sections+");return false;\">Add row</a></p>\
				<table class=\"data_entry\">\
					<tr class=\"row_definition\" id=\"sd_"+num_sections+"_rd_"+section_rows[num_sections]+"\">\
						<td>\
							<input type=\"text\" name=\"section["+num_sections+"][rows]["+section_rows[num_sections]+"][id]\" class=\"row_id\" />\
						</td>\
						<td class=\"no_right_border\">\
							<input type=\"text\" name=\"section["+num_sections+"][rows]["+section_rows[num_sections]+"][seat_range_low]\" class=\"seat_range_number\" />\
						</td>\
						<td class=\"no_right_border\">\
							&nbsp;to&nbsp;\
						</td>\
						<td>\
							<input type=\"text\" name=\"section["+num_sections+"][rows]["+section_rows[num_sections]+"][seat_range_high]\" class=\"seat_range_number\" />\
						</td>\
						<td>\
							<a href=\"#\" onclick=\"delete_definition('sd_"+num_sections+"_rd_"+section_rows[num_sections]+"');return false;\" tabindex=\"-1\" class=\"char_button\">x</a>\
						</td>\
					</tr>\
				</table>\
			</div>\
		</div>\
	");
}
function delete_definition(id) {
	$('#'+id).slideUp(300, function() {
		$('#'+id).remove();
	});
}