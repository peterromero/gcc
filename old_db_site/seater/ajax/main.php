<?php
	function swap($a, $b) {
		if ($a['id']>$b['id']) {
			return 1;
		} else {
			return -1;
		}
	}
	{ //Define constants.
		define('UNAVAILABLE', 0);
		define('AVAILABLE', 1);
	}
	{ //Validate input.
		$valid_input = true;
		if (count($_POST['section'])<1) {
			$valid_input = false;
		} else {
			foreach($_POST['section'] as $section) {
				foreach($section['rows'] as $row) {
					if ($row['id']<1 || $row['seat_range_low']<1 || $row['seat_range_high']<1 || $row['seat_range_high']<$row['seat_range_low']) {
						$valid_input = false;
					}
				}
			}
		}
		if (!$valid_input) {
			$output['html'] = "<p>Sorry, the input you have entered contains one or more errors. Please check and try again.</p><p>Note that you must submit at least one section, and each section must contain at least one row. Row and seat identifiers must be numeric.</p>";
			echo(json_encode($output));
			exit();
		}
	}
	{ //Sort rows within each section.
		foreach($_POST['section'] as &$section) {
			usort($section['rows'], 'swap');
		}
	}
	{ //Calculate highest and lowest row and highest and lowest seat (rectangular bounds) for each section.
		foreach($_POST['section'] as &$section) {
			$section['seat_range_low'] = null;
			$section['seat_range_high'] = null;
			$section['row_range_low'] = null;
			$section['row_range_high'] = null;
			foreach($section['rows'] as $row) {
				if ($section['seat_range_high']<$row['seat_range_high']||$section['seat_range_high']==null) {
					$section['seat_range_high'] = $row['seat_range_high'];
				}
				if ($section['seat_range_low']>$row['seat_range_low']||$section['seat_range_low']==null) {
					$section['seat_range_low'] = $row['seat_range_low'];
				}
				if ($section['row_range_low']>$row['id']||$section['row_range_low']==null) {
					$section['row_range_low'] = $row['id'];
				}
				if ($section['row_range_high']<$row['id']||$section['row_range_high']==null) {
					$section['row_range_high'] = $row['id'];
				}
				for($i = $row['seat_range_low']; $i <= $row['seat_range_high']; $i++) {
					$section['seats'][$row['id']][$i] = AVAILABLE;
				}
			}
			// $output['html'] .= "<pre>" . print_r($section['seats'], true) . "</pre>";
		}
	}
	unset($section);
	{ //Output each section as a table.
		foreach($_POST['section'] as $section) {
			$output['html'] .= "<h3>Section {$section['id']}</h3><table class=\"section_diagram\">";
			$first_row = true;
			for ($row = $section['row_range_high']; $row >= $section['row_range_low']; $row--) {
				if ($first_row) {
					$output['html'] .= "<td class=\"key_number\">&nbsp;</td>";
					for ($seat = $section['seat_range_low']; $seat <= $section['seat_range_high']; $seat++) {
						$output['html'] .= "<td class=\"key_number column\">$seat</td>";
					}
				}
				$first_column = true;
				$output['html'] .= "<tr>";
				for ($seat = $section['seat_range_low']; $seat <= $section['seat_range_high']; $seat++) {
					$class = ($section['seats'][$row][$seat]==AVAILABLE)?'purchaseable seat':'non_purchaseable seat';
					if ($first_column) {
						$output['html'] .= "<td class=\"key_number row\">$row</td>";
					}
					$output['html'] .= "<td class=\"$class\">B­i­t­t­e­r­m­a­n-T­o­z­e­r</td>";
					$first_column = false;
				}
				$output['html'] .= "</tr>";
				$first_row = false;
			}
			$output['html'] .= "</table>";
		}
	}
	echo(json_encode($output));
?>