<?php
	require_once('../../citron/php/html.php');
	$x = new htmlDocument();
	$x->setTitle('GCC Seater &amp; Ticket Assigner');
	$x->importCSS('css/main.css');
	$x->importJavascript('js/main.js');
	$x->initialize();
	
	echo("
		<link href='http://fonts.googleapis.com/css?family=Yanone+Kaffeesatz:700' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Chivo:400,900' rel='stylesheet' type='text/css'>
		<div class=\"leftColumn structural\">
			<div class=\"collapsible_section\">
				<h2>Available Seating Area</h2>
				<div class=\"collapsible_section_contents\">
					<form id=\"seating_area_definition\">
						<div class=\"section_definition\" id=\"sd_1\">
							<div class=\"title\">
								Section <a href=\"#\" onclick=\"delete_definition('sd_1');return false;\" class=\"char_button\">x</a>
							</div>
							<div class=\"contents\">
								<p><strong>Section&nbsp;ID:</strong> <input type=\"text\" name=\"section[1][id]\" class=\"section_id\" /></p>
								<p><strong>Rows &amp; Seats:</strong> <a href=\"#\" class=\"add_row_button\" onclick=\"add_row_definition(1);return false;\">Add row</a></p>
								<table class=\"data_entry\">
									<tr class=\"row_definition\" id=\"sd_1_rd_1\">
										<td>
											<input type=\"text\" name=\"section[1][rows][1][id]\" class=\"row_id\" />
										</td>
										<td class=\"no_right_border\">
											<input type=\"text\" name=\"section[1][rows][1][seat_range_low]\" class=\"seat_range_number\" />
										</td>
										<td class=\"no_right_border\">
											&nbsp;to&nbsp;
										</td>
										<td>
											<input type=\"text\" name=\"section[1][rows][1][seat_range_high]\" class=\"seat_range_number\" />
										</td>
										<td>
											<a href=\"#\" onclick=\"delete_definition('sd_1_rd_1');return false;\" tabindex=\"-1\" class=\"char_button\">x</a>
										</td>
									</tr>
								</table>
							</div>
						</div>
					</form>
					<a href=\"#\" class=\"add_section_button\" onclick=\"add_section_definition();return false;\">New Section</a>
					<a class=\"button\" href=\"#\" onclick=\"submit_seating_area_definition();return false;\">Submit Seating Chart</a>
				</div>
			</div>
			<div class=\"collapsible_section\">
				<h2>Ticket Holders</h2>
				<div class=\"collapsible_section_contents\">
				</div>
			</div>
		</div>
		<div class=\"mainColumn structural\">
			Main
		</div>
	");
	
	$x->close();
?>