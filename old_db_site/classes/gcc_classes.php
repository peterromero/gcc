<?php
	class newsArticle extends dbRecord {
		function __construct() {
			$this->tableName = 'gcc_news_articles';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->formFormattingArguments = array('**id');
			}
			$tmpField = new dbField;
				$tmpField->displayName = 'Title'; {
				$tmpField->codeName = 'title';
				$tmpField->sqlUpdateFieldName = 'title';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**title');
				$tmpField->formFormattingArguments = array('**title');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Text'; {
				$tmpField->codeName = 'text';
				$tmpField->sqlUpdateFieldName = 'text';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXTAREA);
				$tmpField->displayFormattingArguments = array('**text');
				$tmpField->formFormattingArguments = array('**text');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Start Publishing'; {
				$tmpField->codeName = 'runStartDate';
				$tmpField->sqlUpdateFieldName = 'runStartDate';
				$tmpField->displayFormattingFunctionName = 'formatSqlDate';
				$tmpField->displayFormattingArguments = array('**runStartDate', 'dateOnly');
				$tmpField->setFormFieldType(FIELD_TYPE_DATE_SELECTOR);
				$tmpField->formFormattingArguments = array('**runStartDate');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'End Publishing'; {
				$tmpField->codeName = 'runEndDate';
				$tmpField->sqlUpdateFieldName = 'runEndDate';
				$tmpField->displayFormattingFunctionName = 'formatSqlDate';
				$tmpField->displayFormattingArguments = array('**runEndDate', 'dateOnly');
				$tmpField->setFormFieldType(FIELD_TYPE_DATE_SELECTOR);
				$tmpField->formFormattingArguments = array('**runEndDate');
			}
			$this->addField($tmpField);
		}
	}
	class committee extends dbRecord {
		function __construct() {
			$this->tableName = 'gcc_committees';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->formFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Name'; {
				$tmpField->codeName = 'name';
				$tmpField->sqlUpdateFieldName = 'name';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**name');
				$tmpField->formFormattingArguments = array('**name');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Members'; {
				$tmpField->codeName = 'members';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getCommitteeMembers';
				$tmpField->displayFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
		}
	}
	class committeeMembership extends dbRecord {
		function __construct() {
			$this->tableName = 'gcc_committee_memberships';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->formFormattingArguments = array('**id');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Committee'; {
				$tmpField->codeName = 'committee';
				$tmpField->sqlUpdateFieldName = 'committee'; //The database field where data from this field will go on INSERT or UPDATE.
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**committee', 'gcc_committees', 'name'); //To input field names into this array, prefix them with **. The data from these fields will be looked up and "flattened" into the array before formatting.
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_committees';
				$tmpField->foreignTableOrderFieldName = 'name';
				$tmpField->linkToEditForm = EDIT_FOREIGN;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Member'; {
				$tmpField->codeName = 'member';
				$tmpField->sqlUpdateFieldName = 'member'; //The database field where data from this field will go on INSERT or UPDATE.
				$tmpField->displayFormattingFunctionName = 'lookupFormattedForeignKey';
				$tmpField->displayFormattingArguments = array('**member', 'gcc_people', 'formatName', array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full')); //To input field names into this array, prefix them with **. The data from these fields will be looked up and "flattened" into the array before formatting.
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_SUGGESTION);
				$tmpField->foreignKeyTableName = 'gcc_people';
				$tmpField->formFormattingFunctionName = 'formatName';
				$tmpField->formFormattingArguments = array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full');
				$tmpField->linkToEditForm = EDIT_FOREIGN;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Membership Type'; {
				$tmpField->codeName = 'type';
				$tmpField->sqlUpdateFieldName = 'type'; //The database field where data from this field will go on INSERT or UPDATE.
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**type', 'gcc_committee_involvement_types', 'name'); //To input field names into this array, prefix them with **. The data from these fields will be looked up and "flattened" into the array before formatting.
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_committee_involvement_types';
				$tmpField->foreignTableOrderFieldName = 'name';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Chair'; {
				$tmpField->codeName = 'isChair';
				$tmpField->sqlUpdateFieldName = 'isChair'; //The database field where data from this field will go on INSERT or UPDATE.
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isChair', 'gcc_yes_no', 'word'); //To input field names into this array, prefix them with **. The data from these fields will be looked up and "flattened" into the array before formatting.
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Board Liaison'; {
				$tmpField->codeName = 'isBoardLiaison';
				$tmpField->sqlUpdateFieldName = 'isBoardLiaison'; //The database field where data from this field will go on INSERT or UPDATE.
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isBoardLiaison', 'gcc_yes_no', 'word'); //To input field names into this array, prefix them with **. The data from these fields will be looked up and "flattened" into the array before formatting.
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
		}
	}
	class relationship extends dbRecord {
		function __construct() {
			$this->tableName = 'gcc_relationships';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->formFormattingArguments = array('**id');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Inferior Member'; {
				$tmpField->codeName = 'memberLower';
				$tmpField->sqlUpdateFieldName = 'memberLower';
				$tmpField->displayFormattingFunctionName = 'lookupFormattedForeignKey';
				$tmpField->displayFormattingArguments = array('**memberLower', 'gcc_people', 'formatName', array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full'));
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_SUGGESTION);
				$tmpField->foreignKeyTableName = 'gcc_people';
				$tmpField->formFormattingFunctionName = 'formatName';
				$tmpField->formFormattingArguments = array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full');
				$tmpField->linkToEditForm = EDIT_FOREIGN;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Superior Member'; {
				$tmpField->codeName = 'memberUpper';
				$tmpField->sqlUpdateFieldName = 'memberUpper';
				$tmpField->displayFormattingFunctionName = 'lookupFormattedForeignKey';
				$tmpField->displayFormattingArguments = array('**memberUpper', 'gcc_people', 'formatName', array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full'));
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_SUGGESTION);
				$tmpField->foreignKeyTableName = 'gcc_people';
				$tmpField->formFormattingFunctionName = 'formatName';
				$tmpField->formFormattingArguments = array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full');
				$tmpField->linkToEditForm = EDIT_FOREIGN;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Type'; {
				$tmpField->codeName = 'type';
				$tmpField->sqlUpdateFieldName = 'type';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**type', 'gcc_relationship_types', 'nameUpperNa');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_relationship_types';
				$tmpField->foreignTableDisplayFieldName = 'nameUpperNa';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Guardian'; {
				$tmpField->codeName = 'guardian';
				$tmpField->sqlUpdateFieldName = 'guardian';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**guardian', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Emergency Contact'; {
				$tmpField->codeName = 'emergency';
				$tmpField->sqlUpdateFieldName = 'emergency';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**emergency', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
		}
	}
	class school extends dbRecord {
		function __construct() {
			$this->tableName = 'gcc_schools';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->formFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Sort Order'; {
				$tmpField->codeName = 'order';
				$tmpField->sqlUpdateFieldName = 'order';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**order');
				$tmpField->formFormattingArguments = array('**order');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Name'; {
				$tmpField->codeName = 'name';
				$tmpField->sqlUpdateFieldName = 'name';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**name');
				$tmpField->formFormattingArguments = array('**name');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Full Address'; {
				$tmpField->codeName = 'address';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingFunctionName = 'formatAddress';
				$tmpField->displayFormattingArguments = array('**street', '**city', '**state', '**zip', 2);
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Street'; {
				$tmpField->codeName = 'street';
				$tmpField->sqlUpdateFieldName = 'street';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**street');
				$tmpField->formFormattingArguments = array('**street');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'City'; {
				$tmpField->codeName = 'city';
				$tmpField->sqlUpdateFieldName = 'city';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**city');
				$tmpField->formFormattingArguments = array('**city');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'State'; {
				$tmpField->codeName = 'state';
				$tmpField->sqlUpdateFieldName = 'state';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**state');
				$tmpField->formFormattingArguments = array('**state');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Zip'; {
				$tmpField->codeName = 'zip';
				$tmpField->sqlUpdateFieldName = 'zip';
				$tmpField->displayFormattingFunctionName = 'formatZip';
				$tmpField->displayFormattingArguments = array('**zip');
				$tmpField->setFormFieldType(FIELD_TYPE_ZIP);
				$tmpField->formFormattingArguments = array('**zip');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Grade Range'; {
				$tmpField->codeName = 'grades';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingFunctionName = 'formatGradeRange';
				$tmpField->displayFormattingArguments = array('**gradeLo', '**gradeHi');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Lowest Grade'; {
				$tmpField->codeName = 'gradeLo';
				$tmpField->sqlUpdateFieldName = 'gradeLo';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**gradeLo');
				$tmpField->formFormattingArguments = array('**gradeLo');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Highest Grade'; {
				$tmpField->codeName = 'gradeHi';
				$tmpField->sqlUpdateFieldName = 'gradeHi';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**gradeHi');
				$tmpField->formFormattingArguments = array('**gradeHi');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Recruit?'; {
				$tmpField->codeName = 'recruitFrom';
				$tmpField->sqlUpdateFieldName = 'recruitFrom';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**recruitFrom', 'gcc_yes_no', 'word');
				$tmpField->formFormattingArguments = array('**recruitFrom');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
				$tmpField->foreignTableOrderFieldName = 'order';
			}
			$this->addField($tmpField);
		}
	}
	class person extends dbRecord {
		//This constructor sets up the fields that it is possible to load from a MySQL row. All fields listed here will be loaded upon $this->loadSingleRecord();
		function __construct() {
			$this->tableName = 'gcc_people';
			$tmpField = new dbField;
				$tmpField->displayName = 'ID'; {
				$tmpField->codeName = 'id';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingArguments = array('**id');
				$tmpField->formFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Current GCC Singer'; {
				$tmpField->codeName = 'isCurrentSinger';
				$tmpField->sqlUpdateFieldName = 'isCurrentSinger';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isCurrentSinger', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Current GCC Parent'; {
				$tmpField->codeName = 'isCurrentParent';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'isCurrentParent';
				$tmpField->displayFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Past GCC Singer'; {
				$tmpField->codeName = 'isAlumnus';
				$tmpField->sqlUpdateFieldName = 'isAlumnus';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isAlumnus', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'GCC Graduate'; {
				$tmpField->codeName = 'isGraduate';
				$tmpField->sqlUpdateFieldName = 'isGraduate';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isGraduate', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'GCC Staff'; {
				$tmpField->codeName = 'isStaff';
				$tmpField->sqlUpdateFieldName = 'isStaff';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**isStaff', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Status'; {
				$tmpField->codeName = 'status';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getCurrentStatus';
				$tmpField->displayFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Full Name'; {
				$tmpField->codeName = 'nameFull';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'formatName';
				$tmpField->displayFormattingArguments = array('**honorific', '**nameFirst', '**nameLast', getViewAttribute('nameDisplay'), 'full');
				$tmpField->linkToEditForm = EDIT_THIS;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Honorific'; {
				$tmpField->codeName = 'honorific';
				$tmpField->sqlUpdateFieldName = 'honorific';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**honorific');
				$tmpField->formFormattingArguments = array('**honorific');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'First Name'; {
				$tmpField->codeName = 'nameFirst';
				$tmpField->sqlUpdateFieldName = 'nameFirst';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**nameFirst');
				$tmpField->formFormattingArguments = array('**nameFirst');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Last Name'; {
				$tmpField->codeName = 'nameLast';
				$tmpField->sqlUpdateFieldName = 'nameLast';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**nameLast');
				$tmpField->formFormattingArguments = array('**nameLast');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Gender'; {
				$tmpField->codeName = 'gender';
				$tmpField->sqlUpdateFieldName = 'gender';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**gender', 'gcc_genders', 'nameLong');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_genders';
				$tmpField->foreignTableDisplayFieldName = 'nameLong';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Birthdate'; {
				$tmpField->codeName = 'birthdate';
				$tmpField->sqlUpdateFieldName = 'birthdate';
				$tmpField->displayFormattingFunctionName = 'formatSqlDate';
				$tmpField->displayFormattingArguments = array('**birthdate', 'dateOnly');
				$tmpField->setFormFieldType(FIELD_TYPE_DATE_SELECTOR);
				$tmpField->formFormattingArguments = array('**birthdate');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Age'; {
				$tmpField->codeName = 'age';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getAgeFromSqlBirthdate';
				$tmpField->displayFormattingArguments = array('**birthdate', time());
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Grade'; {
				$tmpField->codeName = 'grade';
				$tmpField->sqlUpdateFieldName = 'yearK';
				$tmpField->displayFormattingFunctionName = 'getGradeOrdinalFromYearK';
				$tmpField->displayFormattingArguments = array('**yearK', time());
				$tmpField->setFormFieldType(FIELD_TYPE_GRADE);
				$tmpField->formFormattingFunctionName = 'getGradeCardinalFromYearK';
				$tmpField->formFormattingArguments = array('**yearK', time());
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Cell Phone'; {
				$tmpField->codeName = 'phoneCell';
				$tmpField->sqlUpdateFieldName = 'phoneCell';
				$tmpField->displayFormattingFunctionName = 'formatPhone';
				$tmpField->displayFormattingArguments = array('**phoneCell', 'parentheses', false);
				$tmpField->setFormFieldType(FIELD_TYPE_PHONE);
				$tmpField->formFormattingArguments = array('**phoneCell');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Home Phone'; {
				$tmpField->codeName = 'phoneHome';
				$tmpField->sqlUpdateFieldName = 'phoneHome';
				$tmpField->displayFormattingFunctionName = 'formatPhone';
				$tmpField->displayFormattingArguments = array('**phoneHome', 'parentheses', false);
				$tmpField->setFormFieldType(FIELD_TYPE_PHONE);
				$tmpField->formFormattingArguments = array('**phoneHome');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Office Phone'; {
				$tmpField->codeName = 'phoneOffice';
				$tmpField->sqlUpdateFieldName = 'phoneOffice';
				$tmpField->displayFormattingFunctionName = 'formatPhone';
				$tmpField->displayFormattingArguments = array('**phoneOffice', 'parentheses', false);
				$tmpField->setFormFieldType(FIELD_TYPE_PHONE);
				$tmpField->formFormattingArguments = array('**phoneOffice');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Full Address'; {
				$tmpField->codeName = 'address';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'formatAddress';
				$tmpField->displayFormattingArguments = array('**street', '**city', 'CO', '**zip', 1);
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Street'; {
				$tmpField->codeName = 'street';
				$tmpField->sqlUpdateFieldName = 'street';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**street');
				$tmpField->formFormattingArguments = array('**street');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'City'; {
				$tmpField->codeName = 'city';
				$tmpField->sqlUpdateFieldName = 'city';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**city');
				$tmpField->formFormattingArguments = array('**city');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Zip'; {
				$tmpField->codeName = 'zip';
				$tmpField->sqlUpdateFieldName = 'zip';
				$tmpField->displayFormattingFunctionName = 'formatZip';
				$tmpField->displayFormattingArguments = array('**zip');
				$tmpField->setFormFieldType(FIELD_TYPE_ZIP);
				$tmpField->formFormattingArguments = array('**zip');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Email 1'; {
				$tmpField->codeName = 'email1';
				$tmpField->sqlUpdateFieldName = 'email1';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingFunctionName = 'formatClickableEmailAddress';
				$tmpField->displayFormattingArguments = array('**email1');
				$tmpField->formFormattingArguments = array('**email1');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Email 2'; {
				$tmpField->codeName = 'email2';
				$tmpField->sqlUpdateFieldName = 'email2';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingFunctionName = 'formatClickableEmailAddress';
				$tmpField->displayFormattingArguments = array('**email2');
				$tmpField->formFormattingArguments = array('**email2');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'School'; {
				$tmpField->codeName = 'school';
				$tmpField->sqlUpdateFieldName = 'school';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**school', 'gcc_schools', 'name');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_schools';
				$tmpField->allowNullValue = true;
				$tmpField->linkToEditForm = EDIT_FOREIGN;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Ensemble'; {
				$tmpField->codeName = 'ensemble';
				$tmpField->sqlUpdateFieldName = 'ensemble';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**ensemble', 'gcc_ensembles', 'name');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_ensembles';
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Employer'; {
				$tmpField->codeName = 'employer';
				$tmpField->sqlUpdateFieldName = 'employer';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**employer');
				$tmpField->formFormattingArguments = array('**employer');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Related People'; {
				$tmpField->codeName = 'relatedPeople';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getRelatedPeople';
				$tmpField->displayFormattingArguments = array('**id', null);
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Parents'; {
				$tmpField->codeName = 'parents';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getRelatedPeople';
				$tmpField->displayFormattingArguments = array('**id', array(array('field'=>'type', 'value'=>'1')));
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Committees'; {
				$tmpField->codeName = 'committees';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'getCommitteeMemberships';
				$tmpField->displayFormattingArguments = array('**id');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Medical Concerns'; {
				$tmpField->codeName = 'medical';
				$tmpField->sqlUpdateFieldName = 'medical';
				$tmpField->displayFormattingArguments = array('**medical');
				$tmpField->setFormFieldType(FIELD_TYPE_TEXTAREA);
				$tmpField->formFormattingArguments = array('**medical');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Allergies'; {
				$tmpField->codeName = 'allergies';
				$tmpField->sqlUpdateFieldName = 'allergies';
				$tmpField->displayFormattingArguments = array('**allergies');
				$tmpField->setFormFieldType(FIELD_TYPE_TEXTAREA);
				$tmpField->formFormattingArguments = array('**allergies');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Tylenol Allowed'; {
				$tmpField->codeName = 'tylenol';
				$tmpField->sqlUpdateFieldName = 'tylenol';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**tylenol', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'GCC Membership Dates'; {
				$tmpField->codeName = 'membershipRange';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'formatNumberRange';
				$tmpField->displayFormattingArguments = array('**membershipStart', '**membershipEnd');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Number of Years in GCC'; {
				$tmpField->codeName = 'membershipYears';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcYearsInGcc';
				$tmpField->displayFormattingArguments = array('**isSinger', '**membershipStart', '**membershipEnd');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Start of Membership'; {
				$tmpField->codeName = 'membershipStart';
				$tmpField->sqlUpdateFieldName = 'membershipStart';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**membershipStart');
				$tmpField->formFormattingArguments = array('**membershipStart');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'End of Membership'; {
				$tmpField->codeName = 'membershipEnd';
				$tmpField->sqlUpdateFieldName = 'membershipEnd';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**membershipEnd');
				$tmpField->formFormattingArguments = array('**membershipEnd');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Permission to Photograph'; {
				$tmpField->codeName = 'photoPermission';
				$tmpField->sqlUpdateFieldName = 'photoPermission';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**photoPermission', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'General Notes'; {
				$tmpField->codeName = 'notesGeneral';
				$tmpField->sqlUpdateFieldName = 'notesGeneral';
				$tmpField->displayFormattingArguments = array('**notesGeneral');
				$tmpField->setFormFieldType(FIELD_TYPE_TEXTAREA);
				$tmpField->formFormattingArguments = array('**notesGeneral');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Tuition Grant'; {
				$tmpField->codeName = 'grant';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'formatGrant';
				$tmpField->displayFormattingArguments = array('**grantAmount', '**grantCalculation');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Grant Amount'; {
				$tmpField->codeName = 'grantAmount';
				$tmpField->canBeDisplayed = false;
				$tmpField->sqlUpdateFieldName = 'grantAmount';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**grantAmount');
				$tmpField->formFormattingArguments = array('**grantAmount');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Grant Calculation'; {
				$tmpField->codeName = 'grantCalculation';
				$tmpField->canBeDisplayed = false;
				$tmpField->sqlUpdateFieldName = 'grantCalculation';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**grantCalculation', 'gcc_grant_calculation_types', 'name');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_grant_calculation_types';
				$tmpField->foreignTableOrderFieldName = 'id';
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Composite Tuition for Year'; {
				$tmpField->codeName = 'compositeTuition';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcCompositeTuitionString';
				$tmpField->displayFormattingArguments = array('**isSinger', '**grantAmount', '**grantCalculation', '**siblingsInGroup');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Composite Tuition (Unformatted)'; {
				$tmpField->codeName = 'compositeTuitionRaw';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcCompositeTuition';
				$tmpField->displayFormattingArguments = array('**isSinger', '**grantAmount', '**grantCalculation', '**siblingsInGroup');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Simple Tuition (Unformatted)'; {
				$tmpField->codeName = 'simpleTuitionRaw';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcSimpleTuition';
				$tmpField->displayFormattingArguments = array('**isSinger', '**grantAmount', '**grantCalculation');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Tickets (Unformatted)'; {
				$tmpField->codeName = 'ticketsRaw';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcTickets';
				$tmpField->displayFormattingArguments = array('**isSinger', '**siblingsInGroup');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'First Tuition Payment'; {
				$tmpField->codeName = 'tuitionPaymentFirst';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcFirstPaymentString';
				$tmpField->displayFormattingArguments = array('**isSinger', '**grantAmount', '**grantCalculation', '**siblingsInGroup');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Normal Tuition Payment'; {
				$tmpField->codeName = 'tuitionPaymentNormal';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->showOnForm = false;
				$tmpField->displayFormattingFunctionName = 'calcAveragePaymentString';
				$tmpField->displayFormattingArguments = array('**isSinger', '**grantAmount', '**grantCalculation', '**siblingsInGroup');
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Going on Tour'; {
				$tmpField->codeName = 'goingOnTour';
				$tmpField->sqlUpdateFieldName = 'goingOnTour';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**goingOnTour', 'gcc_yes_no', 'word');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_yes_no';
				$tmpField->foreignTableDisplayFieldName = 'word';
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Voice Part'; {
				$tmpField->codeName = 'voicePart';
				$tmpField->sqlUpdateFieldName = 'voicePart';
				$tmpField->displayFormattingFunctionName = 'lookupForeignKey';
				$tmpField->displayFormattingArguments = array('**voicePart', 'gcc_voice_parts', 'nameLong');
				$tmpField->setFormFieldType(FIELD_TYPE_FOREIGN_KEY_DROPDOWN);
				$tmpField->foreignKeyTableName = 'gcc_voice_parts';
				$tmpField->foreignTableDisplayFieldName = 'nameLong';
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Siblings in GCC'; {
				$tmpField->codeName = 'siblingsInGroup';
				$tmpField->sqlUpdateFieldName = 'siblingsInGroup';
				$tmpField->setFormFieldType(FIELD_TYPE_TEXT);
				$tmpField->displayFormattingArguments = array('**siblingsInGroup');
				$tmpField->formFormattingArguments = array('**siblingsInGroup');
				$tmpField->allowNullValue = true;
			}
			$this->addField($tmpField);
			$tmpField = new dbField;
				$tmpField->displayName = 'Record Updated'; {
				$tmpField->codeName = 'dateUpdated';
				$tmpField->includeInSqlUpdate = false;
				$tmpField->setFormFieldType(FIELD_TYPE_READOUT);
				$tmpField->displayFormattingFunctionName = 'formatSqlDate';
				$tmpField->displayFormattingArguments = array('**dateUpdated', 'dateTime');
			}
			$this->addField($tmpField);
		}
	}
?>